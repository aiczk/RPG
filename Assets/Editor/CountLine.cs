﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Linq;

public class CountLine : EditorWindow
{
    private static CountLine count;

    [MenuItem("Window/LineCount")]
    public static void Init()
    {
        if (count == null)
            count = CreateInstance<CountLine>();
        
        count.ShowUtility();
    }

    private string path = "";

    private void OnGUI()
    { 
        path = GUILayout.TextField(path);
        if (GUILayout.Button("Count!"))
        {
            var dir = new DirectoryInfo("Assets/" + path);
            var info = dir.GetFiles("*.cs", SearchOption.AllDirectories);
            if(info.Length == 0)
            {
                return;
            }

            var allLineCount = 0;
            info.Where(x => x.Name != "CountLine.cs")
                .ToList()
                .ForEach(x =>
                {

                    using (var sr = x.OpenText())
                    {
                        while (sr.ReadLine() != null)
                        {
                            allLineCount++;
                        }
                    }
                });

            Debug.Log($"{allLineCount}行");

        }
    }
}