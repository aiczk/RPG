﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Domain.UseCase
{
    public interface IUIOpenUseCase
    {
        IObservable<bool> OnUIStateAsObservable();
    }

    public interface IUIOpenPresenter
    {
        IObservable<Unit> OnUIOpenAsObservable();
        IObservable<Unit> OnUICloseAsObservable();
        
    }
    
    public class UIOpenUseCase : MonoBehaviour,IUIOpenUseCase
    {
        private ReactiveProperty<bool> state = new ReactiveProperty<bool>(false);
        private IUIOpenPresenter presenter;

        private void Start()
        {
            presenter = GetComponent<IUIOpenPresenter>();

            presenter
                .OnUIOpenAsObservable()
                .Where(x => !state.Value)
                .Subscribe(x => state.Value = true);

            presenter
                .OnUICloseAsObservable()
                .Where(x => state.Value)
                .Subscribe(x => state.Value = false);
        }

        IObservable<bool> IUIOpenUseCase.OnUIStateAsObservable() => state.AsObservable();
    }
}
