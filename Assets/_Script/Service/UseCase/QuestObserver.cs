﻿using System;
using System.Collections;
using System.Collections.Generic;
using App.Utility;
using Domain.Entity;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.UI;

namespace Domain.UseCase
{
    public interface IQuestObserver
    {
        int GetProgress();
    }
    
    //クエスト発注するやつ
    public class QuestObserver : MonoBehaviour,IQuestObserver
    {
        [SerializeField, Range(0, 10)] private int loadMissionProgress = default;
        [SerializeField] private GameObject observer = default;
        
        private QuestProgress playerQuestProgress = QuestProgress.None;
        private IQuestUseCase questUseCase = default;
        private IQuestRepository questRepository = default;
        
        
        private void Awake()
        {
            questUseCase = GetComponent<IQuestUseCase>();
            questRepository = GetComponent<IQuestRepository>();
        }

        private void Start()
        {            
            questUseCase
                .QuestProgress()
                .Subscribe(x =>
                {
                    playerQuestProgress = x;
                });
                        
        observer.OnTriggerEnterAsObservable()
                .Where(x => x.gameObject.CompareTag("Player"))
                .Subscribe(x =>
                {
                    Debug.Log("enter");
                    switch (playerQuestProgress)
                    {
                        case QuestProgress.None:
                            Debug.Log("可能");
                            questUseCase.SetQuestData(questRepository.GetQuestData().GetKillQuest()[loadMissionProgress]);
                            break;
                        
                        case QuestProgress.During:
                            Debug.Log("実行中 クエストは受けられない");
                            break;
                        
                        case QuestProgress.Completed:
                            Debug.Log("完了");
                            loadMissionProgress += 1;
                            //questUseCase.SetQuestData(questRepository.GetQuestData().GetKillQuest()[loadMissionProgress]);
                            break;
                        
                        default:
                            throw new ArgumentOutOfRangeException(nameof(x), x, null);
                    }
                    
                });

        observer.OnTriggerExitAsObservable()
            .Where(x => x.gameObject.CompareTag("Player"))
            .Subscribe(x =>
            {
                
            });
        }

        int IQuestObserver.GetProgress() => loadMissionProgress;
    }
}