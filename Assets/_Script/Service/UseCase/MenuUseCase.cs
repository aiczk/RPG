﻿using System;
using System.Collections;
using System.Collections.Generic;
using App.Utility;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Domain.UseCase
{
    public interface IMenuUseCase
    {
        IObservable<MenuIndex> OnMenuChoiceAsObservable();
    }

    public interface IMenuPresenter
    {
        IObservable<Unit> OnSettingButtonClickAsObservable();
        IObservable<Unit> OnWareHouseButtonClickAsObservable();
        IObservable<Unit> OnGachaButtonClickAsObservable();
        IObservable<Unit> OnStoreButtonClickAsObservable();
        IObservable<Unit> OnNotificationButtonClickAsObservable();
    }
    
    public class MenuUseCase : MonoBehaviour,IMenuUseCase
    {
        private IMenuPresenter presenter;
        private ReactiveProperty<MenuIndex> currentIndex = new ReactiveProperty<MenuIndex>();
        
        private void Start()
        {
            presenter = GetComponent<IMenuPresenter>();
            
            presenter
                .OnSettingButtonClickAsObservable()
                .Subscribe(x => currentIndex.Value = MenuIndex.Setting);

            presenter
                .OnWareHouseButtonClickAsObservable()
                .Subscribe(x => currentIndex.Value = MenuIndex.WareHouse);
            
            presenter
                .OnGachaButtonClickAsObservable()
                .Subscribe(x => currentIndex.Value = MenuIndex.Gacha);
                
            presenter
                .OnStoreButtonClickAsObservable()
                .Subscribe(x => currentIndex.Value = MenuIndex.Store);

            presenter
                .OnNotificationButtonClickAsObservable()
                .Subscribe(x => currentIndex.Value = MenuIndex.Notification);
        }

        IObservable<MenuIndex> IMenuUseCase.OnMenuChoiceAsObservable() => currentIndex.SkipLatestValue().AsObservable();
    }
}