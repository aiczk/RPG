﻿using System;
using System.Collections;
using System.Collections.Generic;
using App.Utility;
using UniRx;
using UniRx.Diagnostics;
using UnityEngine;

namespace Domain.UseCase
{
    public interface IStoryProgressUseCase
    {
        IObservable<bool> OnOpenButtonInteractiveAsObservable();
    }

    public interface IStoryProgressRepository
    {
        int GetCurrentStoryProgress();
        void SaveCurrentStoryProgress(int scene);
    }

    public interface IStoryProgressPresenter
    {
    }
    
    public class StoryProgressUseCase : MonoBehaviour,IStoryProgressUseCase
    {
        private Subject<bool> interactive = new Subject<bool>();
        private IStoryProgressRepository repository;
        private IStageSelectUseCase stageSelect;

        private void Start()
        {
            repository = GetComponent<IStoryProgressRepository>();
            stageSelect = GetComponentInParent<IStageSelectUseCase>();

            stageSelect
                .ObserveEveryValueChanged(x => x.GetCurrentStageID())
                .Cast<StageID, int>()
                .Subscribe(x => interactive.OnNext(x <= repository.GetCurrentStoryProgress()));
        }

        IObservable<bool> IStoryProgressUseCase.OnOpenButtonInteractiveAsObservable() => interactive.AsObservable();
    }
}
