﻿using System;
using System.Collections;
using System.Collections.Generic;
using App.Utility;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Domain.UseCase
{
    public interface IMenuThemeUseCase
    {
        IObservable<MenuTheme[]> OnPreviewThemeAsObservable();
        IObservable<int> OnCurrentPageAsObservable();
        IObservable<MenuTheme> OnApplyThemeAsObservable();
        MenuTheme InitializeMenu();
    }

    public interface IMenuThemeRepository
    {
        MenuTheme[] GetPreviewMenuTheme();
        //TODO: 色は分離させる
        void SaveColor(Color color);
        Color LoadColor();

        void SaveTheme(int index);
        int LoadTheme();
    }

    public interface IMenuThemePresenter
    {
        IObservable<Unit> OnThemeDecisionAsObservable();
        IObservable<Unit> OnLeftArrowAsObservable();
        IObservable<Unit> OnRightArrowAsObservable();
    }
    
    public class MenuThemeUseCase : MonoBehaviour,IMenuThemeUseCase
    {                
        private IMenuThemeRepository menuRepository;
        private IMenuThemePresenter menuPresenter;
        private IMenuUseCase menuUseCase;
        private Color temp,currentColor;
        private int tempIndex,currentIndex;
        private MenuTheme[] tempThemes;
        private Subject<MenuTheme[]> previewThemes = new Subject<MenuTheme[]>();
        private Subject<MenuTheme> applyTheme = new Subject<MenuTheme>();
        private ReactiveProperty<int> currentPage = new ReactiveProperty<int>();

        private void Awake()
        {
            menuPresenter = GetComponent<IMenuThemePresenter>();
            menuRepository = GetComponent<IMenuThemeRepository>();
            menuUseCase = GameObject.FindWithTag("Menu").GetComponent<IMenuUseCase>();
        }
        
        private void Start()
        {            
            temp = currentColor = menuRepository.LoadColor();            
            tempThemes = menuRepository.GetPreviewMenuTheme();
            
            menuPresenter
                .OnThemeDecisionAsObservable()
                .Subscribe(x =>
                {
                    if (!currentIndex.Equals(tempIndex))
                    {
                        var value = currentPage.Value.Repeat(0, tempThemes.Length);
                        
                        applyTheme.OnNext(tempThemes[value]);
                        menuRepository.SaveTheme(value);
                        tempIndex = value;
                    }
                    
                    if (currentColor.Equals(temp))
                        return;
                    
                    menuRepository.SaveColor(currentColor);
                    temp = currentColor;
                });

            menuPresenter
                .OnLeftArrowAsObservable()
                .Subscribe(x => currentPage.Value--);

            menuPresenter
                .OnRightArrowAsObservable()
                .Subscribe(x => currentPage.Value++);

            currentPage
                .Subscribe(x => currentIndex = x);

            menuUseCase
                .OnMenuChoiceAsObservable()
                .Where(x => x == MenuIndex.Store)
                .Subscribe(x => previewThemes.OnNext(tempThemes));
        }
        
        IObservable<MenuTheme[]> IMenuThemeUseCase.OnPreviewThemeAsObservable() => previewThemes.AsObservable();

        IObservable<int> IMenuThemeUseCase.OnCurrentPageAsObservable() => currentPage.SkipLatestValue().AsObservable();

        IObservable<MenuTheme> IMenuThemeUseCase.OnApplyThemeAsObservable() => applyTheme.AsObservable();

        MenuTheme IMenuThemeUseCase.InitializeMenu() =>
            menuRepository.GetPreviewMenuTheme()[menuRepository.LoadTheme()];
    }
}
