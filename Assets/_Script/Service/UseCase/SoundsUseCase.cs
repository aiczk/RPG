﻿using System;
using System.Collections;
using System.Collections.Generic;
using Domain.Entity;
using Sirenix.OdinInspector;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.UI;
#pragma warning disable 0649

namespace Domain.UseCase
{
    public interface ISoundsPresenter
    {
        
    }
    
    public interface ISoundsRepository
    {
        float GetBGMVolume();
        float GetSEVolume();
    }

    public interface ISoundsUseCase
    {
        //変更した値をプッシュ
        IObservable<float> OnBGMVolumeChangedAsObservable();
        IObservable<float> OnSEVolumeChangedAsObservable();
        IObservable<bool> OnDisplayAsObservable();
    }
    
    public class SoundsUseCase : MonoBehaviour,ISoundsUseCase
    {
        [SerializeField, BoxGroup("Volume", centerLabel: true)] private Slider BGMVolumeSlider,SEVolumeSlider;
        
        private FloatReactiveProperty BGMVolume = new FloatReactiveProperty();
        private FloatReactiveProperty SEVolume = new FloatReactiveProperty();
        private BoolReactiveProperty display = new BoolReactiveProperty(false);
        private Subject<(float,float)> volumes = new Subject<(float, float)>();

        private ISoundsRepository soundsRepository;
        
        private void Start()
        {
            soundsRepository = GetComponent<ISoundsRepository>();

            BGMVolumeSlider.value = soundsRepository.GetBGMVolume();
            SEVolumeSlider.value = soundsRepository.GetSEVolume();
            
            BGMVolumeSlider
                .ObserveEveryValueChanged(x => x.value)
                .Subscribe(x => BGMVolume.Value = x);

            SEVolumeSlider
                .ObserveEveryValueChanged(x => x.value)
                .Subscribe(x => SEVolume.Value = x);
            
            volumes.OnNext((BGMVolume.Value,SEVolume.Value));
        }
        

        IObservable<float> ISoundsUseCase.OnBGMVolumeChangedAsObservable() =>
            BGMVolume
                .SkipLatestValue()
                .AsObservable();

        IObservable<float> ISoundsUseCase.OnSEVolumeChangedAsObservable() =>
            SEVolume
                .SkipLatestValue()
                .AsObservable();

        IObservable<bool> ISoundsUseCase.OnDisplayAsObservable() =>
            display
                .AsObservable();
    }

}
