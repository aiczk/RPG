﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace Domain.UseCase
{
    public interface IDamagePositionUseCase
    {
        IObservable<(Vector3 pos,int damage)> OnDamagePositionAsObservable();
    }

    public interface IDamagePositionRepository
    {
        IObservable<(Vector3 pos,int damage)> OnDamagePositionAsObservable();
    }

    public interface IDamagePositionPresenter
    {
        
    }
    
    public class DamagePositionUseCase : MonoBehaviour,IDamagePositionUseCase
    {
        private IDamagePositionRepository damagePositionRepository;

        private void Start()
        {
            //TODO: ここでUIの位置調整をする。
            damagePositionRepository = GetComponent<IDamagePositionRepository>();
        }

        IObservable<(Vector3 pos,int damage)> IDamagePositionUseCase.OnDamagePositionAsObservable() =>
            damagePositionRepository.OnDamagePositionAsObservable();
    }
}
