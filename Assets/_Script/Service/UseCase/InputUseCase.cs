﻿using System;
using System.Linq;
using App.Utility;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using Manager.Update;
using Input = UnityEngine.Input;

namespace Domain.UseCase
{
    public interface IInputPresenter
    {
    }

    public interface IInputable
    {
        IObservable<Unit> OnEventStartAsObservable();
        IObservable<Unit> OnNextPageAsObservable();
    }
    
    public class InputUseCase : SingletonMonoBehaviour<InputUseCase>,IInputable,IUpdatable
    {        
        private Subject<Unit> onEventStart = new Subject<Unit>(),nextPage = new Subject<Unit>();
        
        private void Start() => UpdateManager.Instance.AddUpdateList(this);

        void IUpdatable.Update()
        {
            InputRelated();
        }

        private void InputRelated()
        {
#if UNITY_STANDALONE_WIN
            
            if(Input.GetKeyDown(KeyCode.Space)) 
                onEventStart.OnNext(default);

            if(Input.GetMouseButtonDown(0)) 
                nextPage.OnNext(default);
            
#elif UNITY_STANDALONE_OSX
            //OSX
#elif UNITY_IOS
            //IOS
#elif UNITY_ANDROID
            //ANDROID
#endif
        }

        IObservable<Unit> IInputable.OnEventStartAsObservable() => onEventStart.AsObservable().Share();
        IObservable<Unit> IInputable.OnNextPageAsObservable() => nextPage.AsObservable().Share();
    }

}
