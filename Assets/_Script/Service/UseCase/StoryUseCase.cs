﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.OleDb;
using Save;
using UniRx;
using UniRx.Diagnostics;
using UnityEngine;

namespace Domain.UseCase
{
    public interface IStoryUseCase
    {
        IObservable<StoryDataTable> OnSendStoryAsObservable();
        IObservable<Unit> OnInitializeTextAsObservable();
    }

    public interface IStoryProgress
    {
        void SetStoryProgress(int scene,int story,int stage);
        string GetStoryProgress();
    }

    public interface IStoryPresenter
    {
        
    }

    public interface IStoryRepository
    {
        StoryDataTable[] GetStoryData(string key);
    }
    
    public class StoryUseCase : MonoBehaviour,IStoryUseCase,IStoryProgress
    {
        private const string fileName = "STORY INDEX";
        
        private string sceneData;
        private int count;
        private IInputable inputable;
        private IStoryProgress storyProgress;
        private IStoryRepository storyRepository;
        private IConversationControlUseCase conversation;
        private Vector3Int tempProgress = Vector3Int.zero,currentProgress = Vector3Int.zero;

        private Subject<Unit> initialize = new Subject<Unit>();
        private Subject<StoryDataTable> textSender = new Subject<StoryDataTable>();
        
        private void Start()
        {
            inputable = InputUseCase.Instance;
            storyProgress = GetComponent<IStoryProgress>();
            storyRepository = GetComponent<IStoryRepository>();
            conversation = GameObject.FindGameObjectWithTag("AnimateUI").GetComponent<IConversationControlUseCase>();
            
            storyProgress.SetStoryProgress(1,1,1);

           var mouseObservable = inputable
                .OnNextPageAsObservable()
                .Where(x => conversation.GetCurrentControlState())
                .Select(x => ++count)
                .ToReactiveProperty()
                .Share();
            
            mouseObservable
                .Where(x => storyRepository.GetStoryData(sceneData).IsArrayWithInRange(x))
                .Subscribe(x => textSender.OnNext(storyRepository.GetStoryData(sceneData)[x]));

            mouseObservable
                .Where(x => !storyRepository.GetStoryData(sceneData).IsArrayWithInRange(x))
                .Subscribe(x =>
                {
                    count = 0;
                    conversation.SetControlState(false);
                    initialize.OnNext(default);
                });

            conversation
                .ObserveEveryValueChanged(x => x.GetCurrentControlState())
                .Where(x => x)
                .Where(x => count == 0)
                .Throttle(TimeSpan.FromMilliseconds(600))
                .Subscribe(x => textSender.OnNext(storyRepository.GetStoryData(sceneData)[0]));
        }

        private void OnDestroy()
        {
            ReserveSaveData<Vector3Int>.Save(currentProgress, fileName);
        }

        IObservable<StoryDataTable> IStoryUseCase.OnSendStoryAsObservable() => textSender.AsObservable();
        IObservable<Unit> IStoryUseCase.OnInitializeTextAsObservable() => initialize.AsObservable();
        
        void IStoryProgress.SetStoryProgress(int scene, int story, int stage)
        {
            currentProgress = new Vector3Int(scene, story, stage);
            
            if(tempProgress.Equals(currentProgress))
                return;

            count = 0;
            initialize.OnNext(default);
            tempProgress = currentProgress;
            sceneData = $"{string.Intern(scene.ToString())}-{string.Intern(story.ToString())}-{string.Intern(stage.ToString())}";
        }
        
        string IStoryProgress.GetStoryProgress() => sceneData;
    }
}
