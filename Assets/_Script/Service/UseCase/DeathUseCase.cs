﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Domain.UseCase
{
    public interface IDeathUseCase
    {
        IObservable<Unit> OnDeathEventAsObservable();
    }

    public interface IDeathPresenter
    {
        
    }
    
    public class DeathUseCase : MonoBehaviour,IDeathUseCase
    {        
        private Subject<Unit> Death = new Subject<Unit>();

//        private void Start() => 
//            GameObject
//                .FindGameObjectWithTag("Data")
//                .GetComponent<IDataUseCase>()
//                .OnGetHealth()
//                .Where(x => x <= 0)
//                .Take(1)
//                .Subscribe(x => Death.OnNext(default));

        public IObservable<Unit> OnDeathEventAsObservable() => Death.AsObservable();
    }
}
