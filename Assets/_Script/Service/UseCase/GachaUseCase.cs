﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Domain.UseCase
{
    public interface IGachaUseCase
    {
        IObservable<GachaItemData> OnSingleGachaAsObservable();
        IObservable<GachaItemData[]> OnMultipleGachaAsObservable();
    }

    public interface IGachaRepository
    {
        GachaItemData GetSingleItem();
        GachaItemData[] GetMultipleItems(int count);
    }

    public interface IBelongCharacterRepository
    {
        bool IsPossibleToGacha();
        int GetNumber();
    }

    public interface IGachaPresenter
    {
        IObservable<Unit> OnSingleGachaClicked();
        IObservable<Unit> OnMultiGachaClicked();
    }
    
    public class GachaUseCase : MonoBehaviour,IGachaUseCase
    {
        private IGachaPresenter presenter;
        private IGachaRepository repository = default;
        private IBelongCharacterRepository belongCharacterRepository;
        private Subject<GachaItemData> single = new Subject<GachaItemData>();
        private Subject<GachaItemData[]> multiple = new Subject<GachaItemData[]>();

        private void Start()
        {
            presenter = GetComponent<IGachaPresenter>();
            repository = GetComponent<IGachaRepository>();
            belongCharacterRepository = GetComponent<IBelongCharacterRepository>();
            
            presenter
                .OnSingleGachaClicked()
                .Where(x => belongCharacterRepository.IsPossibleToGacha())
                .Subscribe(x => single.OnNext(repository.GetSingleItem()));

            presenter
                .OnMultiGachaClicked()
                .Where(x => belongCharacterRepository.IsPossibleToGacha())
                .Subscribe(x => multiple.OnNext(repository.GetMultipleItems(10)));
        }

        IObservable<GachaItemData> IGachaUseCase.OnSingleGachaAsObservable() => single.AsObservable();
        IObservable<GachaItemData[]> IGachaUseCase.OnMultipleGachaAsObservable() => multiple.AsObservable();
    }
}
