﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.Utilities;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace Domain.UseCase
{
    public interface IIndicatorUseCase
    {
        IObservable<GameObject> ClosestObjectAsObservable();
        IObservable<bool> IsAcquirableDistance();
        bool GetIsAcquirableDistance();
        bool DrawingRangeInExists();
        GameObject ClosestObject();
    }
    
    public class IndicatorUseCase : MonoBehaviour,IIndicatorUseCase
    {
        [SerializeField] private Camera MainCamera = new Camera(),UICamera = new Camera();
        [SerializeField] private IndicatorData data = default;
        private IIndicatorData indicator;
        private ReactiveProperty<GameObject> closestObject = new ReactiveProperty<GameObject>();
        private BoolReactiveProperty isAcquirableDistance = new BoolReactiveProperty();
        private GameObject[] targets;
        private Transform player;
        //private const int frameCount = 2;

        private Plane[] planes;
        
        private void Awake() => targets = GameObject.FindGameObjectsWithTag("Target");
                
        private void Start()
        {
            indicator = CastTo<IIndicatorData>.From(data);
            
            indicator.Initialize();
            player = SingletonUtility.GetPlayerTransform();
            
                
             MainCamera
                .transform
                .ObserveEveryValueChanged(x => x.position)
                .Where(x => !indicator.GetRelativeDistanceValue())
                .Subscribe(x => NotAcquirable(targets));
                        
            MainCamera
                .transform
                .ObserveEveryValueChanged(x => x.position)
                .Where(_ => indicator.GetRelativeDistanceValue())
                .Subscribe(_ => Calculate(MainCamera,targets));

            closestObject
                .SkipLatestValue()
                .Subscribe(x => indicator.GetCaption().SetText($"[{x}]"));
        }

        IObservable<GameObject> IIndicatorUseCase.ClosestObjectAsObservable() => closestObject.SkipLatestValue().AsObservable();
        IObservable<bool> IIndicatorUseCase.IsAcquirableDistance() => isAcquirableDistance.AsObservable();
        bool IIndicatorUseCase.GetIsAcquirableDistance() => isAcquirableDistance.Value;
        bool IIndicatorUseCase.DrawingRangeInExists() => indicator.GetDrawingRangeInExists();
        GameObject IIndicatorUseCase.ClosestObject() => closestObject.Value;

        private void NotAcquirable(GameObject[] targets)
        {                
            if (closestObject.Value != Utility.ClosestObject(player, targets))
            {
                closestObject.Value = Utility.ClosestObject(player, targets);
                indicator.SetGameObject(closestObject.Value);
            }
            
            indicator.CheckRelativeDistance(player.position);

            if (indicator.GetRelativeDistanceValue() != !indicator.GetRelativeDistanceValue())
                return;
            
            indicator.SetRelativeDistanceValue(indicator.GetRelativeDistanceValue());
        }
        

        private void Calculate(Camera mainCam,GameObject[] targets)
        {
            planes = GeometryUtility.CalculateFrustumPlanes(mainCam);

            if (closestObject.Value != Utility.ClosestObject(player, targets))
            {
                closestObject.Value = Utility.ClosestObject(player, targets);
                indicator.SetGameObject(closestObject.Value);
            }
            
            indicator.CheckRelativeDistance(player.position);

            if (indicator.GetRelativeDistanceValue() == !indicator.GetRelativeDistanceValue())
                indicator.SetRelativeDistanceValue(indicator.GetRelativeDistanceValue());

            if (isAcquirableDistance.Value == !indicator.GetRelativeDistanceValue())
                isAcquirableDistance.Value = indicator.GetRelativeDistanceValue();
            
            if (!indicator.GetRelativeDistanceValue())
            {
                indicator.ImageEnable(false);
                return;
            }

            indicator.SetDrawingRangeInExists(GeometryUtility.TestPlanesAABB(planes, indicator.GetTargetBound()));
            indicator.ImageEnable(indicator.GetDrawingRangeInExists());        

            if(!indicator.GetDrawingRangeInExists())
                return;
                        
            indicator.SetImagePosition(mainCam, UICamera);
        }
    }

}
