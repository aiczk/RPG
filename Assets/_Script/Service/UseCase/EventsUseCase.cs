﻿using System;
using UniRx;
using UniRx.Diagnostics;
using UnityEngine;

namespace Domain.UseCase
{
    public interface IEventUseCase
    {
        IObservable<Vector2> OnEventImagePositionAsObservable();
        IObservable<Unit> OnStartEventAsObservable();
        IObservable<bool> OnEventImageVisibleAsObservable();
        IObservable<string> OnEventStoryIDAsObservable();
    }

    public interface IEventPresenter
    {
        
    }
    
    public class EventsUseCase : MonoBehaviour,IEventUseCase
    {
        private Camera main;
        private Bridge bridge;
        private readonly string tagName = "EventNPC";
        private Subject<Unit> eventStart = new Subject<Unit>();
        private Subject<string> storyID = new Subject<string>();
        private BoolReactiveProperty state = new BoolReactiveProperty(false);
        private IStoryProgress storyProgress;
        private IInputable input;

        private void Awake()
        {
            bridge = Bridge.Instance;
            main = SingletonUtility.GetMainCamera();
            input = InputUseCase.Instance;
        }
        
        private void Start()
        {
            storyProgress = GameObject.FindGameObjectWithTag("Story").GetComponent<IStoryProgress>();

            bridge
                .OnTriggerExit()
                .Where(x => state.Value)
                .Where(x => x.CompareTag(tagName))
                .Subscribe(x => state.Value = false);
            
            bridge
                .OnTriggerStay()
                .Where(x => !state.Value)
                .Where(x => x.CompareTag(tagName))
                .Subscribe(x => state.Value = true);

            bridge
                .OnTriggerEnter()
                .Where(x => x.CompareTag("EventNPC"))
                .Select(x => x.GetComponent<NPC>().GetStoryID())
                .Subscribe(id => storyProgress.SetStoryProgress(id.x,id.y,id.z));
            
            input
                .OnEventStartAsObservable()
                .Where(x => state.Value)
                .Subscribe(x => eventStart.OnNext(default));
        }

        IObservable<Vector2> IEventUseCase.OnEventImagePositionAsObservable() =>
            bridge
                .OnTriggerStay()
                .Select(x => RectTransformUtility.WorldToScreenPoint(main,x.gameObject.transform.position));

        IObservable<Unit> IEventUseCase.OnStartEventAsObservable() => eventStart.AsObservable();
        IObservable<bool> IEventUseCase.OnEventImageVisibleAsObservable() => state.AsObservable();
        IObservable<string> IEventUseCase.OnEventStoryIDAsObservable() => storyID.AsObservable();
    }
}
