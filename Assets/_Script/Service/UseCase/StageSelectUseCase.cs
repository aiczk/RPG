﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using App.Utility;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Domain.UseCase
{
    public interface IStageSelectUseCase
    {
        IObservable<SceneObject> OnStageSceneAsObservable();
        IObservable<(string title, string description)> OnSendStageDataAsObservable();
        IObservable<Sprite> OnBackGroundImageAsObservable();
        IObservable<Sprite> OnThumbNailImageAsObservable();
        StageID GetCurrentStageID();
    }

    public interface IStageSelectPresenter
    {
        
    }

    public interface IStageSelectRepository
    {
        (string, string) GetStageData(StageID id);
        SceneObject GetStageScene(StageID id);
        Sprite GetBackGroundTexture(StageID id);
        Sprite GetThumbNail(StageID id);
    }
    
    public class StageSelectUseCase : MonoBehaviour,IStageSelectUseCase
    {
        [SerializeField] private Button lButton = default, rButton = default, decision = default;

        private IStageSelectRepository repository = default;
        private StageID currentStageId = default;
        private int stageMaxIndex = 0;
        
        private Subject<SceneObject> stageId = new Subject<SceneObject>();
        private Subject<Sprite> backGround = new Subject<Sprite>(),thumbNail = new Subject<Sprite>();
        private Subject<(string,string)> description = new Subject<(string, string)>();

        private void Awake()
        {            
            repository = GetComponent<IStageSelectRepository>();
            stageMaxIndex = Enum.GetValues(typeof(StageID)).Length;
            
            //BUG: stage1が2回呼ばれる
            this.ObserveEveryValueChanged(x => x.currentStageId)
                .Cast<StageID,int>()
                .Select(x => x.Repeat(0,stageMaxIndex))
                .Cast<int,StageID>()
                .Do(x => Debug.Log(x))
                .Subscribe(x => currentStageId = x);
            
            lButton
                .OnClickAsObservable()
                .Subscribe(x => SendData(currentStageId -= 1));
            
            rButton
                .OnClickAsObservable()
                .Subscribe(x => SendData(currentStageId += 1));

            decision
                .OnClickAsObservable()
                .Subscribe(x => stageId.OnNext(repository.GetStageScene(currentStageId)));
        }

        private void Start() => SendData(currentStageId);

        private void SendData(StageID id)
        {
            backGround.OnNext(repository.GetBackGroundTexture(id));
            thumbNail.OnNext(repository.GetThumbNail(id));
            description.OnNext(repository.GetStageData(id));
        }

        IObservable<SceneObject> IStageSelectUseCase.OnStageSceneAsObservable() => stageId.AsObservable();
        IObservable<(string, string)> IStageSelectUseCase.OnSendStageDataAsObservable() => description.AsObservable();
        IObservable<Sprite> IStageSelectUseCase.OnBackGroundImageAsObservable() => backGround.AsObservable();
        IObservable<Sprite> IStageSelectUseCase.OnThumbNailImageAsObservable() => thumbNail.AsObservable();
        StageID IStageSelectUseCase.GetCurrentStageID() => currentStageId;
    }
}
