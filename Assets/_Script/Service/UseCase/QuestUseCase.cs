﻿using System;
using App.Utility;
using Domain.Entity;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Domain.UseCase
{
    public interface IAttackable
    {
        int GetHP();
        void ReduceHP(int minus);
    }

    public interface IQuestUseCase
    {
        void SetQuestData(QuestEntity.KillQuest entity);
        QuestEntity.KillQuest GetQuestData();
        Subject<EnemyAttribute> KillCallBack { get; }
        IObservable<int> KillCounter();
        IObservable<Unit> MissionComplete();
        IObservable<QuestEntity.KillQuest> QuestData();
        IObservable<QuestProgress> QuestProgress();
    }

    public interface IQuestRepository
    {
        QuestEntity GetQuestData();
    }

    public interface IQuestPresenter
    {
        
    }
    
    public class QuestUseCase : MonoBehaviour,IQuestUseCase
    {
        private ReactiveProperty<int> questKillCounter = new ReactiveProperty<int>(0);
        private ReactiveProperty<QuestProgress> progress = new ReactiveProperty<QuestProgress>(App.Utility.QuestProgress.None);
        
        private Subject<QuestEntity.KillQuest> questData = new Subject<QuestEntity.KillQuest>();
        private Subject<Unit> mission_complete = new Subject<Unit>();
        public Subject<EnemyAttribute> KillCallBack { get; } = new Subject<EnemyAttribute>();

        private IQuestRepository questRepository = default;
        private IQuestUseCase questUseCaseImplementation;
        private IQuestObserver observer;

        private void Awake()
        {
            questRepository = GetComponent<IQuestRepository>();
            observer = GetComponent<IQuestObserver>();
                       
            KillCallBack
                .Do(x => Debug.Log(progress.Value))
                .Where(_ => progress.Value == QuestProgress.During)
                .Where(attribute => attribute == ((IQuestUseCase) this).GetQuestData().attribute)
                .Subscribe(x =>
                {
                    questKillCounter.Value -= 1;

                    if (questKillCounter.Value != 0) 
                        return;
                    
                    mission_complete.OnNext(default);
                    progress.Value = App.Utility.QuestProgress.Completed;
                });

            questData
                .Subscribe(x =>
                {
                    switch (progress.Value)
                    {
                        case QuestProgress.None:
                            
                            Debug.Log("受注しました。");
                            questKillCounter.Value = x.KillCount;
                            progress.Value = App.Utility.QuestProgress.During;
                            break;
                        
                        case QuestProgress.During:
                            break;
                        
                        case QuestProgress.Completed:
                            progress.Value = QuestProgress.During;
                            questKillCounter.Value = x.KillCount;
                            //Expとかを与える
                            break;
                        
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                });
        }
        

        QuestEntity.KillQuest IQuestUseCase.GetQuestData()
        {
            return questRepository.GetQuestData().GetKillQuest()[observer.GetProgress()];
        }

        void IQuestUseCase.SetQuestData(QuestEntity.KillQuest entity)
        {
            questData.OnNext(entity);
        }


        IObservable<int> IQuestUseCase.KillCounter()
        {
            return questKillCounter.SkipLatestValue().AsObservable();
        }

        IObservable<Unit> IQuestUseCase.MissionComplete()
        {
            return mission_complete.AsObservable();
        }

        IObservable<QuestEntity.KillQuest> IQuestUseCase.QuestData()
        {
            return questData.AsObservable();
        }

        IObservable<QuestProgress> IQuestUseCase.QuestProgress()
        {
            return progress.AsObservable();
        }
    }
}
