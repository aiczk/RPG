﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace Domain.UseCase
{
    public interface IConversationControlUseCase
    {
        IObservable<bool> OnConversationEventAsObservable();
        bool GetCurrentControlState();
        void SetControlState(bool state);
    }

    public interface IConversationControlPresenter
    {
        
    }
        
    public class ConversationControlUseCase : MonoBehaviour,IConversationControlUseCase
    {
        private BoolReactiveProperty conversationToggle = new BoolReactiveProperty(false);

        private IEventUseCase eventUseCase;

        private void Start()
        {
            eventUseCase = GameObject.FindGameObjectWithTag("Events").GetComponent<IEventUseCase>();

            eventUseCase
                .OnStartEventAsObservable()
                .Subscribe(x => conversationToggle.Value = true);
        }

        IObservable<bool> IConversationControlUseCase.OnConversationEventAsObservable() => conversationToggle.AsObservable();

        bool IConversationControlUseCase.GetCurrentControlState() => conversationToggle.Value;

        void IConversationControlUseCase.SetControlState(bool state) => conversationToggle.Value = state;
    }
}
