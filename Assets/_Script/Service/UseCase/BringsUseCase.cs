﻿using System;
using System.Collections;
using System.Collections.Generic;
using App.Utility;
using UniRx;
using UnityEngine;

namespace Domain.UseCase
{
    public interface IBringsUseCase
    {
        IObservable<GachaItemData[]> OnPlayerBringsDataAsObservable();
        IObservable<bool> OnCurrentWareHousePageStateAsObservable();
    }

    public interface IBringsRepository
    {
        GachaItemData[] GetItemData();
    }

    public interface IBringsPresenter
    {
        
    }
    
    public class BringsUseCase : MonoBehaviour,IBringsUseCase
    {
        private IBringsRepository repository;
        private IMenuUseCase menuUseCase;
        private Subject<GachaItemData[]> data = new Subject<GachaItemData[]>();
        private ReactiveProperty<bool> wareHouseState = new BoolReactiveProperty();

        private void Start()
        {
            var menu = GameObject.FindWithTag("Menu");
            
            repository = GetComponent<IBringsRepository>();
            menuUseCase = menu.GetComponent<IMenuUseCase>();

            menuUseCase
                .OnMenuChoiceAsObservable()
                .Subscribe(x =>
                {
                    switch (x)
                    {
                        case MenuIndex.WareHouse:
                            data.OnNext(repository.GetItemData());
                            wareHouseState.Value = true;
                            break;
                        
                        case MenuIndex.Notification:                        
                        case MenuIndex.Gacha:
                        case MenuIndex.Setting:
                        case MenuIndex.Store:
                            wareHouseState.Value = false;
                            break;
                        
                        default:
                            throw new ArgumentOutOfRangeException(nameof(x), x, null);
                    }
                });
        }

        IObservable<GachaItemData[]> IBringsUseCase.OnPlayerBringsDataAsObservable() => data.AsObservable();
        IObservable<bool> IBringsUseCase.OnCurrentWareHousePageStateAsObservable() => wareHouseState.AsObservable();
    }
}
