﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace Domain.UseCase
{
    public interface IMapUseCase
    {
        IObservable<Vector3> XYAxisMoveAsObservable();
    }

    public interface IMapPresenter
    {
        
    }
    
    //TODO: MapleStory's Map
    public class MapUseCase : MonoBehaviour,IMapUseCase
    {
        private ReactiveProperty<Vector3> Pos = new ReactiveProperty<Vector3>();
        private ReactiveProperty<float> Rot = new ReactiveProperty<float>();

        private void Start()
        {
            SingletonUtility
                .GetPlayerTransform()
                .ObserveEveryValueChanged(x => x.localPosition)
                .ThrottleFirstFrame(3)
                .Subscribe(go => Pos.Value = go);

            SingletonUtility
                .GetPlayerTransform()
                .ObserveEveryValueChanged(x => x.localEulerAngles.y)
                .ThrottleFirstFrame(3)
                .Subscribe(x => Rot.Value = -x /*+ 90*/);
        }


        IObservable<Vector3> IMapUseCase.XYAxisMoveAsObservable() => Pos.SkipLatestValue().AsObservable();
    }
}
