﻿using System.Collections;
using System.Collections.Generic;
using App.Utility;
using Domain.Entity;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace Domain.UseCase
{
    public class EnemyUseCase : MonoBehaviour,IAttackable
    {
        [SerializeField] private bool isBoss = false;
        private ReactiveProperty<int> health = new ReactiveProperty<int>(100);
        private IQuestUseCase questUseCase = default;

        private void Awake()
        {            
            questUseCase = GameObject.FindGameObjectWithTag("Quest").GetComponent<IQuestUseCase>();
            
            if (isBoss)
                health.Value = 500;
        }
        
        private void Start()
        {
            health.Where(x => x <= 0)
                .Take(1)
                .Subscribe(x =>
                {
                    questUseCase.KillCallBack.OnNext(gameObject.name.Contains("tank") ? EnemyAttribute.Ground : EnemyAttribute.Flying);
                    Destroy(this.gameObject);
                });
        }

        int IAttackable.GetHP()
        {
            return health.Value;
        }

        void IAttackable.ReduceHP(int minus)
        {
            Debug.Log($"minus :{minus}");
            health.Value -= minus;
        }

    }


}