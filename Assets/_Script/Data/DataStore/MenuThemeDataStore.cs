﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Data.Repository;
using Save;
using UnityEngine;

namespace Data.DataStore
{
    public class MenuThemeDataStore : MonoBehaviour,IMenuThemeDataStore
    {
        [SerializeField]
        private MenuTheme[] menuThemes = default;

        private string ColorFileName = "color";
        private string ThemeFileName = "THEME";
        
        MenuTheme[] IMenuThemeDataStore.GetPreviewMenuTheme() => menuThemes;

        //TODO: 保存したデータをプレビューできるようにする。
        void IMenuThemeDataStore.SaveColor(Color color) => ReserveSaveData<Color>.Save(color,ColorFileName);
        Color IMenuThemeDataStore.LoadColor() => ReserveSaveData<Load>.LoadColor(ColorFileName);

        void IMenuThemeDataStore.SaveTheme(int index) =>
            ReserveSaveData<Vector2>.Save(new Vector2(index, 0), ThemeFileName);

        int IMenuThemeDataStore.LoadTheme() => (int)ReserveSaveData<Load>.LoadVector2(ThemeFileName).x;
    }
}