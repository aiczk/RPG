﻿using System.Collections;
using System.Collections.Generic;
using Data.Repository;
using Save;
using UnityEngine;

namespace Data.DataStore
{
    public class StoryProgressDataStore : MonoBehaviour,IStoryProgressDataStore
    {
        private string currentProgressSavedFileName = "CurrentStoryProgress";
        private int temp = 0;

        private void Awake()
        {
            if (!ReserveSaveData<Load>.IsFileExist(currentProgressSavedFileName))
            {
                ReserveSaveData<Load>.FileCreate(currentProgressSavedFileName);
                ReserveSaveData<Vector2>.Save(new Vector2(0, 0), currentProgressSavedFileName);
            }

            temp = (int)ReserveSaveData<Load>.LoadVector2(currentProgressSavedFileName).x;
        }
        
        int IStoryProgressDataStore.GetStoryCurrentProgress() => temp;

        void IStoryProgressDataStore.SaveCurrentStoryProgress(int scene)
        {
            ReserveSaveData<Vector2>.Save(new Vector2(scene, 0), currentProgressSavedFileName);
            
            if (temp > scene)
                temp = scene;
        }
    }
}
