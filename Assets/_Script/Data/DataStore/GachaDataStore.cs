﻿using System.Collections;
using System.Collections.Generic;
using Data.Repository;
using UnityEngine;

namespace Data.DataStore
{
    public class GachaDataStore : MonoBehaviour,IGachaDataStore
    {
        //データ一覧 Dictionary
        [SerializeField] private GachaItemData[] items = default;
        private GachaItemData[] multipleItems = new GachaItemData[10];

        GachaItemData IGachaDataStore.GetSingleItem() => items[Random.Range(0, items.Length)];

        GachaItemData[] IGachaDataStore.GetMultipleItems(int count)
        {
            for (var i = 0; i < count; i++)
                multipleItems[i] = items[Random.Range(0, items.Length)];

            return multipleItems;
        }
    }
}