﻿using System.Collections;
using System.Collections.Generic;
using App.Utility;
using Data.Repository;
using Save;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Data.DataStore
{
    public class StoryDataStore : SerializedMonoBehaviour,IStoryDataStore
    {
        private string tempKey = "";
        private StoryDataTable[] tempArray;
        
        [SerializeField,ListDrawerSettings(Expanded = true)] private Dictionary<string,StoryData> storyData = new Dictionary<string, StoryData>();

        StoryDataTable[] IStoryDataStore.GetStoryData(string key)
        {            
            if (tempKey.Contains(key))
                return tempArray;
            
            tempKey = key;

            var story = storyData[key].GetStory();
            tempArray = new StoryDataTable[story.Length];
            tempArray = story;
            
            return tempArray;
        }
    }
}
