﻿using Data.Repository;
using Save;
using UniRx;
using UnityEngine;

namespace Data.DataStore
{
    public class PlayerBringDataStore : MonoBehaviour,IBelongCharacterDataStore
    {
        private static readonly string fileName = "Characters";
        private static readonly int max = 30;
        private int playersBelongCount = 0,cheatValue = 0;
        
        private void Start()
        {
            if (ReserveSaveData<Load>.IsFileExist(FileName.GACHA_RESULT) && ReserveSaveData<Load>.IsFileExist(fileName))
            {
                playersBelongCount = (int)ReserveSaveData<Load>.LoadVector2(fileName).x;
                cheatValue = ReserveSaveData<Load>.LoadCharacterID(FileName.GACHA_RESULT).Length;
            }

            if (!cheatValue.Equals(playersBelongCount)) 
                Debug.Log("CHEAT");

            this.GetRepository()
                .OnSingleBelongCountAsObservable()
                .Merge(this.GetRepository().OnMultipleBelongCountAsObservable())
                .Subscribe(x => ReserveSaveData<Vector2>.Save(new Vector2(playersBelongCount += x, 0), fileName));

            Debug.Log(playersBelongCount.ToString());
        }
        
        bool IBelongCharacterDataStore.IsPossibleToGacha() => playersBelongCount < max;
        int IBelongCharacterDataStore.GetNumber() => playersBelongCount;
    }
}
