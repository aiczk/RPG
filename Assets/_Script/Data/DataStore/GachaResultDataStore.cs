﻿using System.Linq;
using Data.Repository;
using Save;
using UniRx;
using UnityEngine;

namespace Data.DataStore
{
    public class GachaResultDataStore : MonoBehaviour,IGachaResultDataStore
    {        
        private void Start()
        {
            this.GetRepository()
                .OnSingleGachaAsObservable()
                .Subscribe(x => ReserveSaveData<GachaItem>.Save(x.GetItem(), FileName.GACHA_RESULT,true));

            this.GetRepository()
                .OnMultipleGachaAsObservable()
                .Select(x => x.Select(y => y.GetItem()))
                .Subscribe(async x => await ReserveSaveData<GachaItem>.SaveArray(x, FileName.GACHA_RESULT, true));
        }

        //private void Awake() => ReserveSaveData<Load>.Simplify(FileName.GACHA_RESULT);
        //private void OnApplicationQuit() => ReserveSaveData<Load>.Obfuscation(FileName.GACHA_RESULT);
    }
}
