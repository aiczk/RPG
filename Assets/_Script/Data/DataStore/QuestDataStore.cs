﻿using System.Collections;
using System.Collections.Generic;
using Data.Repository;
using Domain.Entity;
using UnityEngine;

namespace Data.DataStore
{
    public class QuestDataStore : MonoBehaviour , IQuestDataStore
    {
        [SerializeField] private QuestEntity KillQuest = default;

        QuestEntity IQuestDataStore.GetQuestEntity()
        {
            return KillQuest;
        }
    }

}