﻿using System;
using System.Collections;
using System.Collections.Generic;
using App.Utility;
using Data.Repository;
using Domain.Entity;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Data.DataStore
{
    public class StageSelectDataStore : SerializedMonoBehaviour,IStageSelectDataStore
    {
        [SerializeField] 
        private NullData nullData = default;
        
        [SerializeField]
        private Dictionary<StageID, StageDataEntity> sceneData = new Dictionary<StageID, StageDataEntity>();

        (string, string) IStageSelectDataStore.GetStageData(StageID id) =>
            sceneData.ContainsKey(id) ? sceneData[id].GetStageData() : nullData.GetStageData();

        SceneObject IStageSelectDataStore.GetStageScene(StageID id) =>
            sceneData.ContainsKey(id) ? sceneData[id].GetScene() : nullData.GetScene();

        Sprite IStageSelectDataStore.GetBackGroundTexture(StageID id) =>
            sceneData.ContainsKey(id) ? sceneData[id].GetBackGround() : nullData.GetBackGround();

        Sprite IStageSelectDataStore.GetThumbNail(StageID id) =>
            sceneData.ContainsKey(id) ? sceneData[id].GetThumbNail() : nullData.GetThumbNail();
        
        [Serializable]
        private struct NullData
        {
            [SerializeField] private SceneObject scene;
            [SerializeField] private Sprite thumbNail,backGround;
            [SerializeField] private string title, description;

            public NullData(SceneObject scene, Sprite thumbNail, Sprite backGround, string title, string description)
            {
                this.scene = scene;
                this.thumbNail = thumbNail;
                this.backGround = backGround;
                this.title = title;
                this.description = description;
            }

            public Sprite GetThumbNail() => thumbNail;
            public Sprite GetBackGround() => backGround;
            public SceneObject GetScene() => scene;

            public (string,string) GetStageData() => (title,description);
        }
    }
}
