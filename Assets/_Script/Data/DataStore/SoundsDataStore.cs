﻿using System.Collections;
using System.Collections.Generic;
using Data.Repository;
using UniRx;
using UnityEngine;

namespace Data.DataStore
{
    public class SoundsDataStore : MonoBehaviour,ISoundsDataStore
    {
        private static readonly string BGM_VOLUME = "BGM_VOLUME";
        private static readonly string SE_VOLUME = "SE_VOLUME";
        
        private void Start()
        {
            //値の保存＆引き出す処理
            this.GetRepository()
                .OnBGMVolumeChangedAsObservable()
                .Subscribe(x =>
                {
                    //保存
                    PlayerPrefs.SetFloat(BGM_VOLUME, x);
                    PlayerPrefs.Save();
                });

            this.GetRepository()
                .OnSEVolumeChangedAsObservable()
                .Subscribe(x =>
                {
                    PlayerPrefs.SetFloat(SE_VOLUME,x);
                    PlayerPrefs.Save();
                });
        }
        
        float ISoundsDataStore.GetBGMVolume() => PlayerPrefs.GetFloat(BGM_VOLUME,50);
        float ISoundsDataStore.GetSEVolume() => PlayerPrefs.GetFloat(SE_VOLUME,50);
    }

}
