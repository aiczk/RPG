﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Data.Repository;
using Save;
using Sirenix.Utilities;
using UniRx;
using UniRx.Async;
using UnityEngine;

namespace Data.DataStore
{
    public class BringsDataStore : MonoBehaviour,IBringsDataStore,IGachaResultDataStore
    {
        [SerializeField] private GachaItemDataTable table = default;
        private Indexer<GachaItemData, string> indexer;
        private List<GachaItemData> items = new List<GachaItemData>();

        private void Start()
        {
            indexer = table;

            if (ReserveSaveData<Load>.IsFileExist(FileName.GACHA_RESULT))
                Reload();

            this.GetRepository()
                .OnSingleGachaAsObservable()
                .Subscribe(x => items.Add(x));

            this.GetRepository()
                .OnMultipleGachaAsObservable()
                .Subscribe(x => x.ForEach(y => items.Add(y)));
        }

        private void Reload()
        {
            var characterId = ReserveSaveData<Load>.LoadCharacterID(FileName.GACHA_RESULT);
            
            foreach (var id in characterId)
            {
                items.Add(indexer[id]);
            }
        }

        GachaItemData[] IBringsDataStore.GetItemData() => items.ToArray();
    }
}
