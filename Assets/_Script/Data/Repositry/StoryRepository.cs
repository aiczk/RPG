﻿using Domain.UseCase;
using UnityEngine;

namespace Data.Repository
{
    public interface IStoryDataStore
    {
        StoryDataTable[] GetStoryData(string indexData);
    }
    
    public class StoryRepository : MonoBehaviour,IStoryRepository
    {
        private IStoryDataStore data = default;

        private void Start() => data = GetComponent<IStoryDataStore>();
        
        StoryDataTable[] IStoryRepository.GetStoryData(string key) => data.GetStoryData(key);
    }
}
