﻿using System.Collections;
using System.Collections.Generic;
using Domain.UseCase;
using UnityEngine;

namespace Data.Repository
{
    public interface IGachaDataStore
    {
        GachaItemData GetSingleItem();
        GachaItemData[] GetMultipleItems(int count);
    }
    
    public class GachaRepository : MonoBehaviour,IGachaRepository
    {
        private IGachaDataStore dataStore = default;

        private void Start() => dataStore = GetComponent<IGachaDataStore>();

        GachaItemData IGachaRepository.GetSingleItem() => dataStore.GetSingleItem();

        GachaItemData[] IGachaRepository.GetMultipleItems(int count) => dataStore.GetMultipleItems(count);
    }
}
