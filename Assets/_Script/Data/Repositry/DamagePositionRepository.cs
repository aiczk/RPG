﻿using System;
using System.Collections;
using System.Collections.Generic;
using Domain.UseCase;
using UniRx;
using UnityEngine;

namespace Data.Repository
{
    public interface IDamagePositionDataStore
    {
    }
    
    public class DamagePositionRepository : SingletonMonoBehaviour<DamagePositionRepository>,IDamagePositionRepository
    {
        private Subject<(Vector3 pos,int dam)> damageSubject = new Subject<(Vector3 pos, int dam)>();

        IObservable<(Vector3 pos, int damage)> IDamagePositionRepository.OnDamagePositionAsObservable() =>
            damageSubject.AsObservable();

        public void SetValue(Vector3 position, int damage) => damageSubject.OnNext((position,damage));
    }

    public static class Ns
    {
        public static DamagePositionRepository GetRepository(this IDamagePositionDataStore ds) =>
            DamagePositionRepository.Instance;
    }
}
