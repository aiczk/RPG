﻿using System.Collections;
using System.Collections.Generic;
using Domain.Entity;
using Domain.UseCase;
using UnityEngine;

namespace Data.Repository
{
    public interface IQuestDataStore
    {
        //クエストのデータ
        QuestEntity GetQuestEntity();
    }
    
    public class QuestRepository : MonoBehaviour,IQuestRepository
    {
        private IQuestDataStore store = default;
        
        private void Awake()
        {
            store = GetComponent<IQuestDataStore>();
        }

        QuestEntity IQuestRepository.GetQuestData()
        {
            return store.GetQuestEntity();
        }
    }

}