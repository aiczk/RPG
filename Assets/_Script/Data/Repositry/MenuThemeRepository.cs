﻿using System.Collections;
using System.Collections.Generic;
using Domain.UseCase;
using UnityEngine;

namespace Data.Repository
{
    public interface IMenuThemeDataStore
    {
        MenuTheme[] GetPreviewMenuTheme();
        void SaveColor(Color color);
        Color LoadColor();
        void SaveTheme(int index);
        int LoadTheme();
    }
    
    public class MenuThemeRepository : MonoBehaviour,IMenuThemeRepository
    {
        private IMenuThemeDataStore dataStore;

        private void Awake()
        {
            dataStore = GetComponent<IMenuThemeDataStore>();
        }

        MenuTheme[] IMenuThemeRepository.GetPreviewMenuTheme() => dataStore.GetPreviewMenuTheme();

        void IMenuThemeRepository.SaveColor(Color color) => dataStore.SaveColor(color);
        Color IMenuThemeRepository.LoadColor() => dataStore.LoadColor();
        
        void IMenuThemeRepository.SaveTheme(int index) => dataStore.SaveTheme(index);
        int IMenuThemeRepository.LoadTheme() => dataStore.LoadTheme();
    }
}