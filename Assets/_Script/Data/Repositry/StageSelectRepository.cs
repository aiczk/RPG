﻿using System.Collections;
using System.Collections.Generic;
using App.Utility;
using Domain.UseCase;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Data.Repository
{
    public interface IStageSelectDataStore
    {
        (string, string) GetStageData(StageID id);
        SceneObject GetStageScene(StageID id);
        Sprite GetBackGroundTexture(StageID id);
        Sprite GetThumbNail(StageID id);
    }
    
    public class StageSelectRepository : SingletonMonoBehaviour<StageSelectRepository>,IStageSelectRepository
    {
        private IStageSelectDataStore dataStore = default;

        private void Awake() => dataStore = GetComponent<IStageSelectDataStore>();

        (string, string) IStageSelectRepository.GetStageData(StageID id) => dataStore.GetStageData(id);

        SceneObject IStageSelectRepository.GetStageScene(StageID id) => dataStore.GetStageScene(id);

        Sprite IStageSelectRepository.GetBackGroundTexture(StageID id) => dataStore.GetBackGroundTexture(id);

        Sprite IStageSelectRepository.GetThumbNail(StageID id) => dataStore.GetThumbNail(id);
    }

    public static class SSr
    {
        public static StageSelectRepository GetRepository(this IStageSelectDataStore ds) =>
            StageSelectRepository.Instance;
    }
}
