﻿using System;
using System.Collections;
using System.Collections.Generic;
using Domain.UseCase;
using UniRx;
using UnityEngine;

namespace Data.Repository
{
    public interface ISoundsDataStore
    {
        float GetBGMVolume();
        float GetSEVolume();
    }
    
    public class SoundsRepository : SingletonMonoBehaviour<SoundsRepository>,ISoundsRepository
    {
        private ISoundsUseCase soundsUseCase;
        private ISoundsDataStore soundsDataStore;

        private void Awake()
        {
            soundsDataStore = GetComponent<ISoundsDataStore>();
            soundsUseCase = GetComponent<ISoundsUseCase>();
        }

        float ISoundsRepository.GetBGMVolume() => soundsDataStore.GetBGMVolume();
        float ISoundsRepository.GetSEVolume() => soundsDataStore.GetSEVolume();

        public IObservable<float> OnBGMVolumeChangedAsObservable() => soundsUseCase.OnBGMVolumeChangedAsObservable();
        public IObservable<float> OnSEVolumeChangedAsObservable() => soundsUseCase.OnSEVolumeChangedAsObservable();
    }

    public static class SoundsRep
    {
        public static SoundsRepository GetRepository(this ISoundsDataStore soundsDataStore) => SoundsRepository.Instance;
    }
}
