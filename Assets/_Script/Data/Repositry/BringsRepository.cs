﻿using System;
using System.Collections;
using System.Collections.Generic;
using Domain.UseCase;
using UniRx;
using UnityEngine;

namespace Data.Repository
{
    public interface IBringsDataStore
    {
        GachaItemData[] GetItemData();
    }
    
    public class BringsRepository : SingletonMonoBehaviour<BringsRepository>,IBringsRepository
    {
        private IBringsUseCase useCase;
        private IBringsDataStore dataStore;
        private IBringsRepository bringsRepositoryImplementation;

        private void Awake()
        {
            useCase = GetComponent<IBringsUseCase>();
            dataStore = GetComponent<IBringsDataStore>();
        }

        GachaItemData[] IBringsRepository.GetItemData() => dataStore.GetItemData();
    }
}
