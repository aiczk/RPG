﻿using System;
using System.Collections;
using System.Collections.Generic;
using Domain.UseCase;
using UniRx;
using UnityEngine;

namespace Data.Repository
{
    public interface IBelongCharacterDataStore
    {
        bool IsPossibleToGacha();
        int GetNumber();
    }
    
    public class PlayerBelongRepository : SingletonMonoBehaviour<PlayerBelongRepository>,IBelongCharacterRepository
    {
        private IBelongCharacterDataStore belongCharacterDataStore;
        private IGachaUseCase useCase;

        private void Awake()
        {
            belongCharacterDataStore = GetComponent<IBelongCharacterDataStore>();
            useCase = GetComponent<IGachaUseCase>();
        }

        public IObservable<int> OnSingleBelongCountAsObservable() =>
            useCase.OnSingleGachaAsObservable().Select(x => 1);

        public IObservable<int> OnMultipleBelongCountAsObservable() =>
            useCase.OnMultipleGachaAsObservable().Select(x => x.Length);

        bool IBelongCharacterRepository.IsPossibleToGacha() => belongCharacterDataStore.IsPossibleToGacha();
        int IBelongCharacterRepository.GetNumber() => belongCharacterDataStore.GetNumber();
    }

    public static class PB
    {
        public static PlayerBelongRepository GetRepository(this IBelongCharacterDataStore ds) =>
            PlayerBelongRepository.Instance;
    }
}
