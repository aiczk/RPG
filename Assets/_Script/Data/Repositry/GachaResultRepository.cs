﻿using System;
using System.Collections;
using System.Collections.Generic;
using Domain.UseCase;
using UnityEngine;

namespace Data.Repository
{
    public interface IGachaResultDataStore
    {
        
    }
    
    public class GachaResultRepository : SingletonMonoBehaviour<GachaResultRepository>
    {
        private IGachaUseCase useCase;

        private void Awake() => useCase = GetComponent<IGachaUseCase>();
        
        public IObservable<GachaItemData> OnSingleGachaAsObservable() => useCase.OnSingleGachaAsObservable();
        public IObservable<GachaItemData[]> OnMultipleGachaAsObservable() => useCase.OnMultipleGachaAsObservable();
    }

    public static class GTP
    {
        public static GachaResultRepository GetRepository(this IGachaResultDataStore ds) =>
            GachaResultRepository.Instance;
    }
}
