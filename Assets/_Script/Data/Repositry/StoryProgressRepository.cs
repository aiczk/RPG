﻿using System.Collections;
using System.Collections.Generic;
using Domain.UseCase;
using UnityEngine;

namespace Data.Repository
{
    public interface IStoryProgressDataStore
    {
        int GetStoryCurrentProgress();
        void SaveCurrentStoryProgress(int scene);
    }
    
    public class StoryProgressRepository : MonoBehaviour,IStoryProgressRepository
    {
        private IStoryProgressDataStore dataStore;

        private void Awake() => dataStore = GetComponent<IStoryProgressDataStore>();

        int IStoryProgressRepository.GetCurrentStoryProgress() => dataStore.GetStoryCurrentProgress();
        void IStoryProgressRepository.SaveCurrentStoryProgress(int scene) => dataStore.SaveCurrentStoryProgress(scene);
    }
}
