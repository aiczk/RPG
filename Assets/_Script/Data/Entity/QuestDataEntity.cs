﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Domain.Entity
{
    public class QuestDataEntity : ScriptableObject
    {
        [SerializeField] public List<QuestEntity> entities = new List<QuestEntity>();
    }


}
