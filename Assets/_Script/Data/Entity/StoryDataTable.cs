using System;
using Sirenix.OdinInspector;
using UnityEngine;

[Serializable]
public struct StoryDataTable
{
    [SerializeField] private CharacterData character;
    [SerializeField, EnumToggleButtons] private CharacterExpressionType faceType;
    [SerializeField,TextArea(2,4)] private string text;

    public StoryDataTable(CharacterData character, CharacterExpressionType faceType,string text)
    {
        this.character = character;
        this.faceType = faceType;
        this.text = text;
    }

    public CharacterData GetCharacterData() => character;
    public CharacterExpressionType GetExpressionType() => faceType;
    public string GetText() => text;
}