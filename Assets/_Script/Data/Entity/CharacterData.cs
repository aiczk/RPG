﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu( menuName = "Param/Create Character", fileName = "Chara" )]
public class CharacterData : SerializedScriptableObject
{
    [SerializeField] private string characterName = default;
    [SerializeField,EnumToggleButtons] private Speaker speakerPosition = default;
    [SerializeField] private Dictionary<CharacterExpressionType,Sprite> characterImages = new Dictionary<CharacterExpressionType, Sprite>();

    public Sprite GetExpressions(CharacterExpressionType chara) => characterImages[chara];
    public string GetCharacterName() => characterName;
    public Speaker GetSpeaker() => speakerPosition;
}

public enum CharacterExpressionType
{
    //ふつう
    Normal,
    //心配
    Worry,
    //恥ずかしい
    Embarrassed,
    //困惑
    Shocked,
    //怒る
    Anger,
    //泣く
    Cry,
    //笑う
    Smile,
    //驚く
    Amazed,
}

public enum Speaker
{
    LEFT,
    RIGHT
}