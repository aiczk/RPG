﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu( menuName = "Param/Create Theme", fileName = "Theme" )]
public class MenuTheme : ScriptableObject
{
    [SerializeField] private Sprite backGround = default,button = default,thumbNail = default;

    public Sprite GetBackGroundTheme() => backGround;
    public Sprite GetButtonTheme() => button;
    public Sprite GetThumbNailTheme() => thumbNail;
}