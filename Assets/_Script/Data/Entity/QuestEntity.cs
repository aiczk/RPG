﻿using System;
using System.Collections;
using System.Collections.Generic;
using App.Utility;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Domain.Entity
{
    
    [CreateAssetMenu( menuName = "Param/Create Quest", fileName = "Parameter" )]
    public class QuestEntity : ScriptableObject
    {
        [SerializeField] private List<KillQuest> Quests = new List<KillQuest>();
        //FromTo
        //Kill data x 0
        //Send data / item
        
        [Serializable]
        public struct KillQuest
        {
            [SerializeField] public string Name;
            [SerializeField,EnumToggleButtons] public EnemyAttribute attribute;
            [SerializeField, Range(1, 10)] public int KillCount;
        }

        public List<KillQuest> GetKillQuest()
        {
            return Quests;
        }
    }


}