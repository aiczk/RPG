﻿using System.Collections;
using System.Collections.Generic;
using App.Utility;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu( menuName = "Param/Create Story", fileName = "Parameter" )]
public class StoryData : SerializedScriptableObject
{
    [SerializeField,ListDrawerSettings(Expanded = true)] private StoryDataTable[] story = default;

    public StoryDataTable[] GetStory() => story;
}