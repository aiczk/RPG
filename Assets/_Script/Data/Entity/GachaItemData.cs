﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu( menuName = "Param/Create GachaItem", fileName = "GachaItem" )]
public class GachaItemData : ScriptableObject
{
    [SerializeField] private GachaItem itemName = default;
    [SerializeField] private Sprite sprite = default;
    [SerializeField, EnumToggleButtons] private Rare rare = default;
    [SerializeField,TextArea(3,5)] private string description = default;

    public Sprite GetItemSprite() => sprite;
    public Color GetRareColor() => ExportRareColor(rare);
    public GachaItem GetItem() => itemName;
    public string GetDescription() => description;

    private Color ExportRareColor(Rare rareLevel)
    {
        switch (rareLevel)
        {
            case Rare.Normal:
                return Color.gray;
            
            case Rare.Rare:
                return Color.blue;
            
            case Rare.SuperRare:
                return Color.red;
            
            default:
                throw new ArgumentOutOfRangeException(nameof(rareLevel), rareLevel, null);
        }
    }
    
    private enum Rare
    {
        Normal,
        Rare,
        SuperRare,
    }
}

[Serializable,InlineProperty(LabelWidth = 200)]
public struct GachaItem
{
    //publicじゃないとjsonに出なくなる
    //ReSharper disable once MemberCanBePrivate.Global
    public string ID;
}
