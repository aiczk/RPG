﻿using UnityEngine;

public interface IParamAtaccher
{
    int GetDamage();
    int GetEndurance();
    GameObject GetObject();
    string GetName();
}

public struct WEAPON_PARAMS : IParamAtaccher
{
    private int damage, endurance;
    private GameObject prefab;
    private string name;

    public WEAPON_PARAMS(int damage, int endurance,GameObject prefab,string name)
    {
        this.damage = damage;
        this.endurance = endurance;
        this.prefab = prefab;
        this.name = name;
    }

    int IParamAtaccher.GetEndurance() => endurance;
    int IParamAtaccher.GetDamage() => damage;
    GameObject IParamAtaccher.GetObject() => prefab;
    string IParamAtaccher.GetName() => name;
}