﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu( menuName = "Param/Create GachaTable", fileName = "Table" )]
public class GachaItemDataTable : SerializedScriptableObject,Indexer<GachaItemData,string>
{
    [SerializeField] private Dictionary<string, GachaItemData> dataTable = new Dictionary<string, GachaItemData>();

    GachaItemData Indexer<GachaItemData, string>.this[string index] => dataTable[index];
}
