﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Domain.Entity
{
    [CreateAssetMenu( menuName = "Param/Create StageParams", fileName = "Parameter" )]
    public class StageDataEntity : ScriptableObject
    {
        [SerializeField] private SceneObject sceneData = default;
        [SerializeField] private string stageName = default;
        [SerializeField,TextArea(3,5)] private string stageDescription = default;
        [SerializeField] private Sprite backGroundImage = default;
        [SerializeField] private Sprite ThumbNail = default;

        public (string, string) GetStageData() => (stageName, stageDescription);
        public SceneObject GetScene() => sceneData;
        public Sprite GetBackGround() => backGroundImage;
        public Sprite GetThumbNail() => ThumbNail;
    }
}
