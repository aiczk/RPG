﻿//インクルードファイル内部では、ファイルが複数回インクルードされるのを防ぐために、インクルードガードから始めます。
#ifndef MYRP_UNLIT_INCLUDED
#define MYRP_UNLIT_INCLUDED

    //これで共通ライブラリ機能を含めることができます。
    //定数バッファマクロとともに、複数の便利な関数とマクロを定義しているので、それらを使用する前にインクルードしてください。
#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"
    
    //現時点ではコンパイルシェーダーがありますが、それでも賢明な結果は得られません。
    //次のステップは、頂点位置を正しい空間に変換することです。
    //モデル-ビュー-プロジェクション行列があれば、オブジェクト空間からクリップ空間に直接変換できますが、Unityはそのような行列を作成しません。
    //それはモデル行列を利用可能にし、それをオブジェクト空間からワールド空間へ変換するために使います。
    //Unityは行列を格納するために、シェーダーがfloat4x4 unity_ObjToWorld変数を持つことを期待しています。
    //HLSLを使用しているので、その変数を自分で定義する必要があります。
    //それを使い、頂点関数でワールド空間に変換し、それを出力に使用します。
    //
    //定数バッファはすべてのプラットフォームに役立つわけではないので、Unityのシェーダーは必要な時にだけそれらを使用するためにマクロに依存します。
    //CBUFFER_STARTを直接書き込む代わりに、nameパラメータをもつCBUFFER_ENDマクロがバッファの末尾を置き換えます。
    //これら2つのマクロは定義されていないため、コンパイルエラーになります。
    //定数バッファを使用してマクロを自分で定義するのが適切かどうかを判断するのではなく、レンダリングパイプラインにUnityのコアライブラリを使用します。
    //それはパッケージマネージャを通して追加することができます。
        
    CBUFFER_START (UnityPerDraw)
    float4x4 unity_ObjToWorld;
    CBUFFER_END
    
    CBUFFER_START (UnityPerFrame)
    float4x4 unity_MatrixVP;
    CBUFFER_END
    
    //インスタンス化が有効になると、GPUは同じ定数データで同じメッシュを複数回描画するように指示します。
    //しかし、M行列はそのデータの一部です。
    //つまり、同じメッシュが全く同じ方法で複数回レンダリングされることになります。
    //この問題を回避するには、すべてのオブジェクトのM行列を含む配列を定数バッファに入れる必要があります。
    //各インスタンスはそれ自身のインデックスで描画され、それは配列から正しいM行列を取得するために使用することができます。
    //
    //インスタンス化しない場合はunity_ObjectToWorld配列を使用し、インスタンス化する場合は行列配列を使用する必要があります。
    //どちらの場合も、UnlitPassVertexのコードを同じに保つために、行列のマクロ、特にUNITY_MATRIX_Mを定義します。
    //コアライブラリには、インスタンス化をサポートするマクロを定義するインクルードファイルがあり、必要に応じて行列配列を使用するようにUNITY_MATRIX_Mを再定義しているので、このマクロ名を使用します。
    #define UNITY_MATRIX_M unity_ObjToWorld
    
    //インクルードファイルはUnityInstancing.hlsl、そしてそれはUNITY_MATRIX_Mを再定義するかもしれないので、そのマクロを自分で定義したあとにそれをインクルードしなくてはなりません。
    #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/UnityInstancing.hlsl"
    
    //カラーデータを配列に配置することで 、インスタンス化が再び機能するようにしました。
    //_Colorプロパティには、M行列と同じ処理を指定する必要があります。
    //この場合、コアライブラリは任意のプロパティのマクロを再定義しないため、明示的に指定する必要があります。
    //代わりに、UNITY_INSTANCING_BUFFER_STARTと、それに伴う終了マクロを使用して、インスタンス化のために定数バッファを手動で作成し、
    //名前付けスキームの一貫性を保つためにPerInstanceという名前を付けます。
    //バッファ内では、色をUNITY_DEFINE_INSTANCED_PROP(flaot4,"_Color")として定義します。
    //インスタンス化を使用しない場合は、float4 _Colorと等しくなりますが、使用しない場合はインスタンスデータの配列になります。
    UNITY_INSTANCING_BUFFER_START (PerInstance)
    UNITY_DEFINE_INSTANCED_PROP(float4, _Color)
    UNITY_INSTANCING_BUFFER_END (PerInstance)

    //頂点プログラムの入出力をfloat4で定義する。
    struct VertexInput{
        float4 pos : POSITION;
        
        //インスタンス化をする場合、現在描画されているオブジェクトのインデックスはGPUによって、その頂点データに追加されます。
        //UNITY_MATRIX_Mはインデックスに依存しているので、それをVertexInput構造体に追加する必要があります。
        //そのためにUNITY_VERTEX_INPUT_INSTANCE_IDを使用することができます。
        UNITY_VERTEX_INPUT_INSTANCE_ID
    };
    
    struct VertexOutput{
        float4 clipPos : SV_POSITION;
        
        //インスタンスインデックスもUnlitPassFragmentで利用可能にする必要があります。
        //そこで、VertexOutputにUNITY_VERTEX_INPUT_INSTANCE_IDを追加し、UnlitPassVertexと同様にUnlitPassFragmentでUNITY_VERTEX_INPUT_INSTANCE_IDを使用します。
        //これを機能させるためには、インデックスを頂点入力から頂点出力にコピーする必要があります。
        //頂点出力にはUNITY_TRANSFER_INSTANCE_IDマクロを使用できます。
        UNITY_VERTEX_INPUT_INSTANCE_ID
    };
    
    //次に、頂点プログラム関数UnlitPassVertexを定義します。
    //今のところ、オブジェクト空間の頂点位置をクリップ空間の位置として直接使用します。
    //これは正しくないですが、コンパイルシェーダーを取得する最も簡単な方法です。
    VertexOutput UnlitPassVertex(VertexInput input){
        VertexOutput output;
        
        //引数を入力として受け取る。
        UNITY_SETUP_INSTANCE_ID(input);
        UNITY_TRANSFER_INSTANCE_ID(input,output);
        float4 worldPos = mul(UNITY_MATRIX_M,float4(input.pos.xyz,1.0));
        output.clipPos = mul(unity_MatrixVP,worldPos);
        
        return output;
    }
    
    //色を定義できる2つの可能な方法に対処するには、UNITY_ACCESS_INSTANCED_PROPマクロを介してアクセスし、バッファとプロパティの名前を渡す必要があります。
    float4 UnlitPassFragment(VertexOutput input) : SV_TARGET{
        UNITY_SETUP_INSTANCE_ID(input);
        return UNITY_ACCESS_INSTANCED_PROP(PerInstance,_Color);
    }

#endif // MYRP_UNLIT_INCLUDED