﻿//インクルードファイル内部では、ファイルが複数回インクルードされるのを防ぐために、インクルードガードから始めます。
#ifndef MYRP_SHADOWCASTER_INCLUDED
#define MYRP_SHADOWCASTER_INCLUDED

#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"
            
    CBUFFER_START (UnityPerDraw)
        float4x4 unity_ObjToWorld;
    CBUFFER_END
    
    CBUFFER_START (UnityPerFrame)
        float4x4 unity_MatrixVP;
    CBUFFER_END
    
    #define UNITY_MATRIX_M unity_ObjToWorld
        
    #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/UnityInstancing.hlsl"
    
    struct VertexInput{
        float4 pos : POSITION;
        UNITY_VERTEX_INPUT_INSTANCE_ID
    };
    
    struct VertexOutput{
        float4 clipPos : SV_POSITION;
    };
    
    VertexOutput ShadowCasterPassVertex(VertexInput input){
        VertexOutput output;
        UNITY_SETUP_INSTANCE_ID(input);
        float4 worldPos = mul(UNITY_MATRIX_M,float4(input.pos.xyz,1.0));
        output.clipPos = mul(unity_MatrixVP,worldPos);
        
        //影をレンダリングするには上記のものだけで十分ですが、シャドウキャスターが近くの場所と交差する可能性があります。
        //これにより、シャドウに穴が現れる可能性があります。これを防ぐには、頂点プログラムで頂点を近い場所に固定する必要があります。
        //これは、クリップスペースとB
        //
        //ただし、これはクリップ空間上の仕様上複雑です。
        //ニアクリップレーンの深度値をゼロとみなし、遠くのクリップ面の奥行きの値を大きくすることです。
        //しかし、OpenGL API以外のすべてのAPIは逆で、値は近接平面で1です。
        //また、OpenGLでは近接平面の値は-1です。
        //Commonに含まれている、UNITY_RESERVED_Zおよび、UNITY_NEAR_CLIP_VALUEマクロを使用することで、すべてのケースをカバーできます。
        #if UNITY_REVERSED_Z
            output.clipPos.z = min(output.clipPos.z,output.clipPos.w * UNITY_NEAR_CLIP_VALUE);
        #else
            output.clipPos.z = max(output.clipPos.z,output.clipPos.w * UNITY_NEAR_CLIP_VALUE);
        #endif
                
        return output;
    }
    
    float4 ShadowCasterPassFragment(VertexOutput input) : SV_TARGET{
        return 0;
    }
    
#endif // MYRP_SHADOWCASTER_INCLUDED