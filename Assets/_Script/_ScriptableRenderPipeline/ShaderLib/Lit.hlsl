﻿//インクルードファイル内部では、ファイルが複数回インクルードされるのを防ぐために、インクルードガードから始めます。
#ifndef MYRP_LIT_INCLUDED
#define MYRP_LIT_INCLUDED

    //これで共通ライブラリ機能を含めることができます。
    //定数バッファマクロとともに、複数の便利な関数とマクロを定義しているので、それらを使用する前にインクルードしてください。
#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"
    
    //シーンで定義されているライトを使用するには、パイプラインからGPUにライトデータを送信する必要があります。
    //シーン内に複数のライトが存在する可能性があるため、複数のライトもサポートする必要があります。
    //それは複数の方法があります。
    //Unityの規定のパイプラインでは、オブジェクトごとに個別パスで各ライトがレンダリングされます。
    //LWRPでは、オブジェクトごとに1回のパスですべてのライトがレンダリングされます。
    //また、HDRPでは遅延レンダリングが使用され、すべてのオブジェクトのサーフェスデータがレンダリングされた後、ライトごとに1回のパスが実行されます。
    //
    //LWRPと同様のアプローチを使用して、すべてのライトを考慮して各オブジェクトを1回レンダリングします。
    //そのために、現在GPUで表示されているすべてのライトのデータを送信します。
    //シーン内にあり、レンダリングされるものに影響しないライトは無視されます。
        
    CBUFFER_START (UnityPerDraw)
        float4x4 unity_ObjToWorld;
        
        //インデックスは、UnityPerDrawバッファの一部であるunity_4LightIndices0,1ベクトルを介して利用可能になります。
        //それ以外に、unity_LightIndicesOffsetAndCount、別のfloat4ベクトルもあります。
        //そのY成分は、オブジェクトに影響を与えるライトの数を含みます。
        //X成分は、無視できます。
        float4 unity_LightIndicesOffsetAndCount;
        float4 unity_4LightIndices0, unity_4LightIndices1;
    CBUFFER_END
    
    CBUFFER_START (UnityPerFrame)
        float4x4 unity_MatrixVP;
    CBUFFER_END
    
    #define MAX_VISIBLE_LIGHTS 16
    
    //すべての照明を1回のパスでレンダリングするということは、すべての照明データを同時に使用できる必要があるということです。
    //現時点では指向性ライトに限定しています。
    //つまり、各ライトの色と方向の両方を知る必要があります。
    //任意の数の光源をサポートするために、このデータを格納するために配列を使用します。
    //
    //しかし、任意のサイズの配列を定義することはできません。
    //配列の定義は、ただちにそのサイズを宣言する必要があります。
    //配列の長さを4にします。つまり、同時に4つのライトをサポートするということです。
    CBUFFER_START (_LightBuffer)
        float4 _VisibleLightColors[MAX_VISIBLE_LIGHTS];
        float4 _VisibleLightDirectionsOrPositions[MAX_VISIBLE_LIGHTS];
        float4 _VisibleLightAttenuations[MAX_VISIBLE_LIGHTS];
        float4 _VisibleLightSpotDirections[MAX_VISIBLE_LIGHTS];
    CBUFFER_END
    
    CBUFFER_START(_ShadowBuffer)
        float4x4 _WorldToShadowMatrix;
    CBUFFER_END
    
    //テクスチャリソースはバッファの一部ではありません。
    //代わりにそれらは別々に定義されています。
    TEXTURE2D_SHADOW(_ShadowMap);
    
    //次に、テクスチャのサンプリングに使用されるサンプラーの状態も定義する必要があります。
    //通常、これはSAMPLERマクロで行われますが、特別な比較サンプラーを使用する代わりにSAMPLER_CMPを使用します。
    SAMPLER_CMP(sampler_ShadowMap);
    
    //ワールド空間での位置をパラメータとして、ShadowAttenuation関数を作成します。
    //ライトの影の減衰係数が返されます。
    float ShadowAttenuation(float3 worldPos){
        float4 shadowPos = mul(_WorldToShadowMatrix,float4(worldPos,1.0));
        
        //位置はクリップ空間に変換するときと同じように、同次座標で表されます。
        //しかし、正規座標が必要なので、XYZ成分をW成分で割ります。
        shadowPos.xyz /= shadowPos.w;
        
        //SAMPLE_TEXTURE2Dマクロを使用し、シャドウマップをサンプリングできます。
        //それは引数として、テクスチャ、サンプラー状態、そして影の位置を必要とします。
        //位置のZ値がシャドウマップに格納されている値よりも小さい場合、結果は1になります。
        //つまり、影を投影しているものよりもライトに近くなります。
        //そうでなければ、それはシャドウキャスターの後ろにあり、結果はゼロです。
        //サンプラーは双一次補完の前に比較を実行するので、影のエッジはシャドウマップテクセル全体に溶け込みます。
        return SAMPLE_TEXTURE2D_SHADOW(_ShadowMap,sampler_ShadowMap,shadowPos.xyz);
    }
    
    //インスタンス化が有効になると、GPUは同じ定数データで同じメッシュを複数回描画するように指示します。
    //しかし、M行列はそのデータの一部です。
    //つまり、同じメッシュが全く同じ方法で複数回レンダリングされることになります。
    //この問題を回避するには、すべてのオブジェクトのM行列を含む配列を定数バッファに入れる必要があります。
    //各インスタンスはそれ自身のインデックスで描画され、それは配列から正しいM行列を取得するために使用することができます。
    //
    //インスタンス化しない場合はunity_ObjectToWorld配列を使用し、インスタンス化する場合は行列配列を使用する必要があります。
    //どちらの場合も、UnlitPassVertexのコードを同じに保つために、行列のマクロ、特にUNITY_MATRIX_Mを定義します。
    //コアライブラリには、インスタンス化をサポートするマクロを定義するインクルードファイルがあり、必要に応じて行列配列を使用するようにUNITY_MATRIX_Mを再定義しているので、このマクロ名を使用します。
    #define UNITY_MATRIX_M unity_ObjToWorld
    
        //ライトデータを使用し、ライティングをする。
        //パラメータとしてライトインデックスと、法線ベクトルを必要とし、配列から関連データを抽出し、
        //次に拡散ライティング計算を実行し、それをライトの色で変調して返します。
        //
        //点光源を扱う場合は、自分自身で光源の方向を計算しなければなりません。
        //まず、ライトの位置からサーフェスの位置を引くので、関数にパラメータを加えます。
        //それがワールド空間の光のベクトルを与え、それを正規化することで方向に変えます。
        float3 DiffuseLight(int index,float3 normal,float3 worldPos,float shadowAttenuation){
        float3 lightColor = _VisibleLightColors[index].rgb;
        float4 lightPositionOrDirection = _VisibleLightDirectionsOrPositions[index];
        float4 lightAttenuation = _VisibleLightAttenuations[index];
        float3 spotDirection = _VisibleLightSpotDirections[index].xyz;
            
        //これはポイントライトに有効ですが、指向性ライトには無意味です。
        //ワールド空間の位置に光の方向のW成分または位置ベクトルをかけることで、同じ計算で両方をサポートできます。
        //それが位置ベクトルならば、Wは1で計算は変わりません。
        //しかし、それが方向ベクトルならば、Wは0であり、減算は除去されます。
        //そのため、元の方向ベクトルを正規化しても違いはありません。
        //指向性ライトに不要な正規化が導入されていますが、それを避けるための分岐は避けます。
        float3 lightVector = lightPositionOrDirection.xyz - worldPos * lightPositionOrDirection.w;
        float3 lightDirection = normalize(lightVector);
        float diffuse = saturate(dot(normal,lightDirection));
            
        //範囲によって生じるフェーディングを計算する。
        float rangeFade = dot(lightVector,lightVector) * lightAttenuation.x;
        rangeFade = saturate(1.0 - rangeFade * rangeFade);
        rangeFade *= rangeFade;
            
        //拡散光。
        float spotFade = dot(spotDirection,lightDirection);
        spotFade = saturate(spotFade * lightAttenuation.z + lightAttenuation.w);
        spotFade *= spotFade;
            
        //ライトの強度が距離とともに減少します。
        //関係は「i / d ^ 2」です。
        //ここでは「i」は光の表示強度、「d」は光源と表面の間の距離です。
        //そのため、最終的な拡散の影響をライトベクトルの二乗で割る必要があります。
        //ゼロによる除算を回避するために、使用される兵法距離の最小値を強制します。
        float distanceSqr = max(dot(lightVector,lightVector),0.00001);
        diffuse *= shadowAttenuation * spotFade * rangeFade / distanceSqr;
                    
        return diffuse * lightColor;
    }
    
    //インクルードファイルはUnityInstancing.hlsl、そしてそれはUNITY_MATRIX_Mを再定義するかもしれないので、そのマクロを自分で定義したあとにそれをインクルードしなくてはなりません。
    #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/UnityInstancing.hlsl"
    
    //カラーデータを配列に配置することで 、インスタンス化が再び機能するようにしました。
    //_Colorプロパティには、M行列と同じ処理を指定する必要があります。
    //この場合、コアライブラリは任意のプロパティのマクロを再定義しないため、明示的に指定する必要があります。
    //代わりに、UNITY_INSTANCING_BUFFER_STARTと、それに伴う終了マクロを使用して、インスタンス化のために定数バッファを手動で作成し、
    //名前付けスキームの一貫性を保つためにPerInstanceという名前を付けます。
    //バッファ内では、色をUNITY_DEFINE_INSTANCED_PROP(flaot4,"_Color")として定義します。
    //インスタンス化を使用しない場合は、float4 _Colorと等しくなりますが、使用しない場合はインスタンスデータの配列になります。
    UNITY_INSTANCING_BUFFER_START (PerInstance)
        UNITY_DEFINE_INSTANCED_PROP(float4, _Color)
    UNITY_INSTANCING_BUFFER_END (PerInstance)

    //頂点プログラムの入出力をfloat4で定義する。
    //
    //ディレクショナルライトの影響を計算するには、サーフェス法線を知る必要があります。
    //したがって、頂点の入力構造と出力構造の両方に法線ベクトルを追加する必要があります。
    struct VertexInput{
        float4 pos : POSITION;
        float3 normal : NORMAL;
        
        //インスタンス化をする場合、現在描画されているオブジェクトのインデックスはGPUによって、その頂点データに追加されます。
        //UNITY_MATRIX_Mはインデックスに依存しているので、それをVertexInput構造体に追加する必要があります。
        //そのためにUNITY_VERTEX_INPUT_INSTANCE_IDを使用することができます。
        UNITY_VERTEX_INPUT_INSTANCE_ID
    };
    
    struct VertexOutput{
        float4 clipPos : SV_POSITION;
        float3 normal : TEXCOORD0;
        float3 worldPos : TEXCOORD1;
        float3 vertexLighting : TEXCOORD2;
        
        //インスタンスインデックスもUnlitPassFragmentで利用可能にする必要があります。
        //そこで、VertexOutputにUNITY_VERTEX_INPUT_INSTANCE_IDを追加し、UnlitPassVertexと同様にUnlitPassFragmentでUNITY_VERTEX_INPUT_INSTANCE_IDを使用します。
        //これを機能させるためには、インデックスを頂点入力から頂点出力にコピーする必要があります。
        //頂点出力にはUNITY_TRANSFER_INSTANCE_IDマクロを使用できます。
        UNITY_VERTEX_INPUT_INSTANCE_ID
    };
    
    //次に、頂点プログラム関数UnlitPassVertexを定義します。
    //今のところ、オブジェクト空間の頂点位置をクリップ空間の位置として直接使用します。
    //これは正しくないですが、コンパイルシェーダーを取得する最も簡単な方法です。
    VertexOutput LitPassVertex(VertexInput input){
        VertexOutput output;
        
        UNITY_SETUP_INSTANCE_ID(input);
        UNITY_TRANSFER_INSTANCE_ID(input,output);
        float4 worldPos = mul(UNITY_MATRIX_M,float4(input.pos.xyz,1.0));
        output.clipPos = mul(unity_MatrixVP,worldPos);
        
        //法線をオブジェクト空間からワールド空間に変換します。
        //均一な尺度のみを使用していると仮定すると、モデル行列の3x3部分を使用します。
        output.normal = mul((float3x3)UNITY_MATRIX_M,input.normal);
        
        output.worldPos = worldPos.xyz;
        
        //
        output.vertexLighting = 0;
        for(int i = 4; i < min(unity_LightIndicesOffsetAndCount.y, 8); i++)
        {
            int lightIndex = unity_4LightIndices1[i - 4];
            
            output.vertexLighting += DiffuseLight(lightIndex,output.normal,output.worldPos,1);
        }
        
        return output;
    }
    
    //色を定義できる2つの可能な方法に対処するには、UNITY_ACCESS_INSTANCED_PROPマクロを介してアクセスし、バッファとプロパティの名前を渡す必要があります。
    float4 LitPassFragment(VertexOutput input) : SV_TARGET{
        UNITY_SETUP_INSTANCE_ID(input);
        
        //フラグメントごとに正規化します。
        //不均等なスケールをサポートするには、代わりに転置されたワールドからオブジェクトへの行列を使用する必要があります。
        input.normal = normalize(input.normal);
        
        //正しい法線ベクトルが得られることを確認するために、それらを色として使用します。
        float3 albedo = UNITY_ACCESS_INSTANCED_PROP(PerInstance,_Color).rgb;
        
        //拡散ライトの影響は、ライトが表面に当たる角度によって異なります。
        //この角度は、サーフェス法線とライトの方向の内積を計算することで求められ、負の結果は無視されます。
        //ディレクショナルライトの場合、ライトベクトルは一定です。
        //ここでは、まっすぐ上を指して、ハードコードされた方向を使用します。
        //拡散光とアルベドを乗算し、最終的な色を求めます。
        float3 diffuseLight = input.vertexLighting;
        
        //forごとに1回関数を呼び出し、フラグメントに与える全拡散ライトを累積します。
        //
        //これで、必要なDiffuseライトだけを呼び出すように制限することができます。
        //しかし、正しい光指数を取得する必要があります。
        //現在最大4つの可視光をサポートしているunity_LightIndices0で、これは配列で成分を得られます。
        //
        //オブジェクトが4つ以上のライトの影響を受ける可能性があるとしても、unity_4LightIndices0は4つまでです。
        //誤った結果を防ぐために4つまでに制限します。
        for(int i = 0; i < min(unity_LightIndicesOffsetAndCount.y,4); i++)
        {
            int lightIndex = unity_4LightIndices0[i];
            float shadowAttenuation = ShadowAttenuation(input.worldPos);
            
            diffuseLight += DiffuseLight(lightIndex,input.normal,input.worldPos,shadowAttenuation);
        }
        
        float3 color = albedo * diffuseLight;
        
        return float4(color,1);
    }

#endif // MYRP_LIT_INCLUDED