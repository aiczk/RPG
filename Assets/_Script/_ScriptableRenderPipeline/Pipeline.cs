﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Rendering;

namespace Render.Pipeline
{
/*
 * RenderPipelineは、IRenderPipelineインターフェースで定義されたRenderメソッドの実装が含まれています。
 * その最初の引数はレンダリングコンテキストです。
 * これはScriptableRenderContect構造体で、ネイティブコードのファサードとして機能します。
 * 2つめの引数は、レンダリングが必要なすべてのカメラを含む配列です。
 *
 * RenderPipeline.Renderは何も描画しませんが、パイプラインオブジェクトがレンダリングに使用できるかどうかを確認します。
 * そうでない場合は、例外が発生します。
 */
    public class Pipeline : RenderPipeline
    {
        private const int maxVisibleLights = 16;
        
        //現時点ではまだ黒い立方体ですが、同じ配列をパイプラインに同じサイズで追加する必要があります。
        private static int visibleLightColorID = Shader.PropertyToID("_VisibleLightColors");
        
        //指向性ライトとは異なり、ポイントライトの位置は重要です。
        //位置に別々の配列を追加するのではなく、方向と位置の両方のデータを同じ配列に格納します。
        //各要素には方向または位置が含まれます。
        private static int visibleLightDirectionOrPositionID = Shader.PropertyToID("_VisibleLightDirectionsOrPositions");
        
        //ポイントライトにも設定範囲があり、その影響範囲が制限されます。
        //オブジェクトを照らすことはできますが、この範囲外のライトは影響を受けません。
        //これは現実的ではありませんが、ライトの制御が向上し、ライトの影響を受けるオブジェクトの数が制限されます。
        //この範囲制限がないと、すべてのライトが常に可視とみなされます。
        //
        //ライト範囲はシーンデータの一部なので、ライトごとにGPUに送信する必要があります。
        //この減衰データには別の配列を使用します。
        //float配列で充分である可能性がありますが、後でさらにデータを含める必要があるので、もう一度Vector配列を使用します。
        private static int visibleLightAttenuationID = Shader.PropertyToID("_VisibleLightAttenuations");
        
        //SpotLight
        private static int visibleLightSpotDirectionID = Shader.PropertyToID("_VisibleLightSpotDirections");

        //Unityにライトデータを設定させないことによる副作用は、最後のオブジェクトに設定された値が維持されるということです。
        //そのため、すべてのオブジェクトに対して0のライトカウントが発生する可能性があります。
        //それを避けるために、手動でunity_LightIndicesOffsetAndCountをゼロにします。
        private static int lightIndicesOffsetAndCountID = Shader.PropertyToID("unity_LightIndicesOffsetAndCount");

        //シャドウマップに格納されている深度情報は、マップをレンダリングするときに使用したクリップスペースに対して有効です。
        //これをシャドウスペースと呼びます。
        //オブジェクトを通常のレンダリングするときに使用するスペースとは一致しません。
        //フラグメントが格納したシャドウの深さに対する相対的な位置を知るためには、フラグメントの位置をシャドウスペースに変換する必要があります。
        private static int shadowMapID = Shader.PropertyToID("_ShadowMap");

        //ワールド空間からシャドウ空間に変換するためのシェーダー行列変数を追加します。
        private static int worldToShadowMatrixID = Shader.PropertyToID("_WorldToShadowMatrix");
        
        //Color配列よりもこっちの方がよい
        private Vector4[] visibleLightColors = new Vector4[maxVisibleLights];
        private Vector4[] visibleLightDirectionsOrPositions = new Vector4[maxVisibleLights];
        private Vector4[] visibleLightAttenuation = new Vector4[maxVisibleLights];
        private Vector4[] visibleLightSpotDirections =  new Vector4[maxVisibleLights];
        
        //動的バッチ処理を処理/制御するためのboolパラメータを使用し、コンストラクタで設定します。
        private DrawRendererFlags drawFlags;

        //影を処理する方法はいくつかありますが、シャドウマップを使用するというデフォルトのアプローチを使います。
        //つまり、光の観点からシーンをレンダリングします。
        //このレンダリングの深さ情報にのみ関心があります。
        //これは、ライトがサーフェスに当たるまでに、到達する距離を示すためです。
        //遠くにあるものはすべて陰になります。
        //
        //シャドウマップを使用するには、通常のカメラでレンダリングする前に作成する必要があります。
        //後でシャドウマップをサンプリングできるようにするには、通常のフレームバッファの代わりに別のレンダーテクスチャにレンダリングする必要があります。
        private RenderTexture shadowMap;
        
        public Pipeline(bool dynamicBatching,bool instancing)
        {
            //デフォルトでは、Unityはライトの強度がガンマ空間で定義されているとみなします。
            //これは、線形空間で作業している場合でも同様です。これはUnityのデフォルトのパイプラインの問題です。
            //新しいパイプラインはそれを線形値としてみなします。
            //この動作はGraphicsSetting.lightsUseLinearIntensityによって制御されます。
            //
            //この設定を変更しても、グラフィック設定が再適用されるときのみエディタに影響します。
            //これは自動的には起こりません。
            //プレイモードに入ったりするとそれが適用されます。
            
            GraphicsSettings.lightsUseLinearIntensity = true;
            
            if (dynamicBatching)
                drawFlags = DrawRendererFlags.EnableDynamicBatching;

            if (instancing)
                drawFlags |= DrawRendererFlags.EnableInstancing;
        }
        
        /*
         * --カメラ--
         * シーン内にはすべてレンダリングする必要がある複数のカメラが存在する可能性があるため、配列を使用していましたが、
         * 現時点ではマルチカメラによるパイプラインのサポートについて心配する必要はありません。
         * 代替のRenderメソッドを作成します。
         */
        
        /*
         * --コマンドバッファ--
         * コンテキストは、送信するまで実際のレンダリングを遅らせます。
         * その前に、それを設定し、後で実行できるようにコマンドを追加します。
         * スカイボックスの描画など、一部のタスクは専用の方法で発行できますが、
         * 他のコマンドは別のコマンドバッファを介して間接的に発行する必要があります。
         *
         * UnityEngine.Rendering名前空間で定義されている新しいCommandBufferオブジェクトをインスタンス化することで、コマンドバッファを作成できます。
         * スクリプト可能なレンダリングパイプラインが追加される前にコマンドバッファが既に存在していたので、これらはExperimentalではありません。
         */
        
        /*
         * --カリング--
         * スカイボックスをレンダリングすることはできますが、シーンに配置したオブジェクトはまだレンダリングができません。
         * すべてのオブジェクトをレンダリングするのではなく、カメラから見えるオブジェクトだけをレンダリングします。
         * これは、シーン内のすべてのレンダラーから始めて、カメラの視錐台の外側にあるレンダラーを選択することによって行います。
         *
         * 何を選択したら良いかを判断するには、ScriptableCullingParameters構造体を使用できる複数のカメラ設定と行列を追跡する必要があります。
         * 自分で入力する代わりに、その作業を静的なCullResults.GetCullingParametersメソッドに委任することができます。
         * 入力としてカメラを取り、出力としてカリングパラメータを作成します。
         */
        
        /*
         * --ドローイング--
         * 何が見えるのかがわかれば、それらの図形のレンダリングをすることができます。
         * これは、cull.visibleRenderersを引数としてDrawRenderersしているコンテキストに対してDrawRenderersを呼び出し、どのレンダラーを使用するかを指示することによって行われます。
         * それ以外に、描画設定とフィルタ設定を提供する必要があります。
         * どちらも構造体(DrawRendererSettingおよびFilterRendererSettings)です。
         * このため、最初はデフォルト値を使用します。
         * 
         * オーバードローを防ぐため、不透明なレンダラーをスカイボックス前に描画します。
         * 図形は常にスカイボックスの正面にあるため、最初に描画することでオーバードローを避けます。
         * これは、不透明なシェーダーが深度バッファに書き込みを渡すためです。
         * 深度バッファは、後で描画されて遠くのものを描画させないために使用されます。
         *
         * 空の一部を覆うことに加え、不透明なレンダラーは互いを隠すこともあります。
         * 理想的には、カメラに最も近いものだけがフレームバッファの各フラグメントに対して描画されます。
         * そのため、オーバードローをできるだけ避けるために、最も近い図形を最初に描画する必要があります。
         * これは、描画前にレンダラーを並べ替えることにとって実行できます。
         *
         * 描画設定には、並び替えフラグを含むDrawRendererSortSetting型のsorting構造体が含まれています。
         * 不透明な図形を描画する前に、SortFlag.CommonOpaqueに設定します。
         * これはUnityにレンダラーを前面から背面への距離とその他いくつかの基準で並び替えるように指示します。
         */
        
        /*
         * --磨く--
         * 正しくレンダリングできるということは、機能的なパイプラインを持つことの一部にすぎません。
         * 十分に早いか、不要な一時オブジェクトを割り当てていないか、UnityEditorとうまく統合されているかなど、考慮すべきこともあります。
         */
        
        /*
         * --メモリ割り当て--
         * カリングが最も多くのメモリが割り当てられているのがわかります。 そのうち、いくつは制御外ですが、かなりの数バイトが割り当てられています。
         * その理由は、CullResults構造体がオブジェクトである3つのリストを含んでいるからです。
         * 新しいカリング結果を要求するたびに、新しいリストにメモリを割り当てます。
         * そのため、CullResultsが構造体であることに大きなメリットはありません。
         * 幸い、CullResultsは新しいパラメータを返す代わりに、構造体を参照パラメータとして受け取る代替のCullResultsメソッドがあります。
         */
        
        /*
         *  --デフォルトのパイプラインをレンダリングする--
         * このパイプラインは現在Unlitシェーダーしかサポートしていないため、異なるシェーダーを使用するとそれはレンダリングされず、見えなくなります。
         * 必要なデータは設定されていないので、照明に依存するものはすべて黒になります。
         * 代わりに、エラーシェーダーを使用してそれらをレンダリングする必要があります。
         * これを行うには、エラー素材が必要です。
         */
        
        /*
         * --GPUのインスタンス化--
         * 動的バッチ処理は、フレームあたりの呼び出しの数を減らすことができる唯一の方法ではありません。
         * 別のアプローチは、GPUインスタンス化を使用することです。
         * インスタンス化の場合、CPUは単一の描画呼び出しを介して特定のメッシュ-マテリアルの組み合わせを複数回描画するようにGPUに指示します。
         * これにより、新しいメッシュを作成しなくても、同じメッシュとマテリアルを使用するオブジェクトをグループ化することができます。
         */

        private CullResults cull;
        private Material errorMaterial;
        
        //コマンドバッファのインスタンスを作る。
        private CommandBuffer cameraBuffer = new CommandBuffer()
        {
            name = "Render Camera"
        };
        
        //影のためのコマンドバッファ。
        private CommandBuffer shadowBuffer = new CommandBuffer()
        {
            name = "Render Shadows"
        };

        private void Render(ScriptableRenderContext renderContext, Camera camera)
        {
            ScriptableCullingParameters cullParam;

            //出力パラメータの他に、GetCullingParametersには有効なパラメータを作成できたかどうかも返します。
            //すべてのカメラ設定が有効ではないため、カリングに使用できない結果が返された場合、Renderから出ることができます。
            if(!CullResults.GetCullingParameters(camera, out cullParam))
                return;
            
            //カリングパラメータを取得したら、それらを使用してカリングすることができます。
            //これは、カリングパラメータとコンテキストの両方を引数として、静的CullResults.Cullメソッドを呼び出すことによって行われます。
            //結果はCullResults構造体です。
            //この場合、参照として受け取る必要があります。
            CullResults.Cull(ref cullParam, renderContext,ref cull);
            
            //シャドウマップは通常のシーンの前にレンダリングされるので、通常のカメラをセットアップする前、つまりカリングしたあとにRenderShadowを呼び出します。
            RenderShadows(renderContext);
            
            //スカイボックスとシーン全体を正しくレンダリングするには、ビュー投影行列を設定する必要があります。
            //この変換行列は、カメラの位置と方向(ビュー行列)とカメラの遠近法、または正射影(投影行列)を組み合わせたものです。
            //SetupCameraPropertiesメソッドを介して、カメラのプロパティをコンテキストに適用する必要があります。
            renderContext.SetupCameraProperties(camera);
            
            var clearFlags = camera.clearFlags;
            cameraBuffer.ClearRenderTarget(
                (clearFlags & CameraClearFlags.Depth) != 0,
                (clearFlags & CameraClearFlags.Color) != 0,
                camera.backgroundColor
            );
            
            //フレームごとにライトデータをGPUに送信していますが、それでも規定のままなので、オブジェクトは黒いままです。
            //ベクトルをコピーする前にライトの設定をする必要があります。
            //ライトが0以上のときにのみ動く。
            if (cull.visibleLights.Count > 0)
                ConfigureLights();
            else
                cameraBuffer.SetGlobalVector(lightIndicesOffsetAndCountID,Vector4.zero);
            
            //空のコマンドバッファを実行しても何も起こりません。
            //以前に描画されたものによってレンダリングが影響を受けないように、レンダリングターゲットをクリアできるために追加しました。
            //これはコマンドバッファ経由で可能ですが、コンテキスト経由ではできません。
            //
            //ClearRenderTargetを呼び出すことで、バッファにクリアコマンドを追加できます。
            //第一引数は、深度情報をクリアするか。
            //第二引数は、色をクリアするか。
            //第三引数は、クリアカラーを制御します。
            //cameraBuffer.ClearRenderTarget(true,false,Color.clear);
            
            //コマンドバッファでSetGrobalVectorArrayを使用すると、配列をGPUにコピーできます。
            cameraBuffer.SetGlobalVectorArray(visibleLightColorID,visibleLightColors);
            cameraBuffer.SetGlobalVectorArray(visibleLightDirectionOrPositionID,visibleLightDirectionsOrPositions);
            cameraBuffer.SetGlobalVectorArray(visibleLightAttenuationID,visibleLightAttenuation);
            cameraBuffer.SetGlobalVectorArray(visibleLightSpotDirectionID,visibleLightSpotDirections);
            
            
            //ExecuteCommandBufferメソッドを介してバッファを実行できるようにコンテキストに指示できます。
            //これはすぐにコマンドを実行しませんが、それらをコンテキストの内部バッファにコピーします。
            renderContext.ExecuteCommandBuffer(cameraBuffer);
            
            cameraBuffer.Clear();
                        
            //また、コンストラクタにカメラとシェーダーパスを引数として渡し、描画設定を構成する必要があります。
            //カメラはレイヤのソートとかリングの設定に使用され、パスはレンダリングに使用されるシェーダーパスを制御します。
            //
            //シェーダーパスは、ShaderPassName構造体でラップする必要がある文字列によって識別されます。
            //このパイプラインでは唯一Untilをサポートしているので、SRPDefaultUnlitで識別されるUnityのデフォルトのUnlitのパスを設定します。
            //
            //カリング中に、Unityは表示されるライトを決定します。
            //これには、どのライトが各オブジェクトに影響を与えるかを把握することも含まれます。
            //Unityにライトインデックスのリストの形でこの情報を送るようにすることができます。
            //
            //Unityは現在、ライトインデックスに対して2つのフォーマットをサポートしています。
            //最初の方法は、オブジェクごとに設定される2つのfloat4変数に最大8つのインデックスを格納することです。
            //2番目の方法は、GPUインスタンシングデータの格納方法と同様に、すべてのオブジェクトのライトインデックスのリストを単一のバッファーに配置することです。
            //ただし、2番目のアプローチはUnity2018.3では無効になっており、最初のオプションのみがサポートされています。
            //理想的ではありませんが、最初の選択肢で実装します。
            var drawSetting = new DrawRendererSettings(camera, new ShaderPassName("SRPDefaultUnlit"))
            {
                flags = drawFlags,
                //float4フィールドを介してライトインデックスを設定するようにする。
                //rendererConfiguration = RendererConfiguration.PerObjectLightIndices8,
                sorting = {flags = SortFlags.CommonOpaque}
            };

            //可視光がゼロの場合、Unityがクラッシュします。
            //少なくとも一つの可視光がある場合、オブジェクトごとの光インデックスを使用するだけでクラッシュを回避できます。
            if (cull.visibleLights.Count > 0)
            {
                drawSetting.rendererConfiguration = RendererConfiguration.PerObjectLightIndices8;
            }


            //動的バッチ処理を有効にする。

            //デフォルトのフィルタ設定にはなにも含まれていないため、オブジェクトはまだ表示されていません。
            //代わりに、FilterRendererSettingsコンストラクタの引数としてtrueを設定することで、すべてを初期化できます。
            var filterSetting = new FilterRenderersSettings(true)
            {
                //透明なシェーダーパスは震度バッファに書き込まれないため、スカイボックスによって描画されます。
                //解決策は、スカイボックスの描画の後までレンダラの描画を遅らせることです。
                //まず、スカイボックスの前の描画を不透明なレンダラーのみに制限します。
                //これは、フィルタ設定のrenderQueueRangeに設定することによって行われます。
                renderQueueRange = RenderQueueRange.opaque
            };
            
            renderContext.DrawRenderers(cull.visibleRenderers,ref drawSetting,filterSetting);
            
            //DrawSkyBoxは引数としてカメラを必要とします。カメラの最初の要素のみを使用します。
            renderContext.DrawSkybox(camera);

            //ただし、透明レンダリングは描かれているものと以前に描かれたものの色を組み合わせているので、結果は透明に見えます。
            //それは後ろから前へ、逆の順序を必要とします。
            //そのためには、SortFlags.CommonTransparentを使用できます。
            drawSetting.sorting.flags = SortFlags.CommonTransparent;
            
            //次に、スカイボックスをレンダリングしたあと、キューの範囲を(RenderQueueRange.transparent2501からRenderQueueRange.transparent5000まで)変更し、もう一度レンダリングします。
            filterSetting.renderQueueRange = RenderQueueRange.transparent;
            renderContext.DrawRenderers(cull.visibleRenderers,ref drawSetting,filterSetting);
            
            //デフォルトのパイプラインを使う
            DrawDefaultPipeline(renderContext,camera);
                                    
            //SkyBoxはまだウィンドウに表示されません。
            //これは、コンテキストに対して発行したコマンドがバッファリングされているからです。
            //実際の作業は、Submitメソッドによって実行されます。
            renderContext.Submit();

            //コンテキストを送信した後は、必ずレンダリングテクスチャを開放します。
            if (shadowMap)
            {
                RenderTexture.ReleaseTemporary(shadowMap);
                shadowMap = null;
            }
        }

        //Unityデフォルトのサーフェスシェーダーには、最初のフォワードレンダリングパスとして使用されるForwardBaseパスがあります。
        //これを使用して、デフォルトのパイプラインで機能するマテリアルをもつオブジェクトを識別できます。
        //新しい描画設定を介してそのパスを選択し、新しいデフォルトのフィルタ設定とともにレンダリングに使用できます。
        //不透明なレンダラーの並び替えや分離については、どちらも無効であるため気にする必要はありません。
        [Conditional("UNITY_EDITOR"),Conditional("DEVELOPMENT_BUILD")]
        private void DrawDefaultPipeline(ScriptableRenderContext renderContext, Camera camera)
        {
            if (errorMaterial == null)
            {
                //Shader.Findを通してHidden/InternalErrorShaderを取得する。
                var errorShader = Shader.Find("Hidden/InternalErrorShader");
                
                //エラーマテリアルがなければ作る。
                errorMaterial = new Material(errorShader)
                {
                    hideFlags = HideFlags.HideAndDontSave
                };
            }
            
            var drawSetting = new DrawRendererSettings(camera,new ShaderPassName("ForwardBase"));
            
            //描画設定の1つのオプションは、SetOverrideMaterialを呼び出し、レンダリング時に使用されるマテリアルをオーバーライドすることです。
            //第一引数はマテリアルです。
            //第二引数は、レンダリングに使用されるマテリアルのシェーダーパスのインデックスです。
            drawSetting.SetOverrideMaterial(errorMaterial,0);
            //上記の設定で、サポートされていない素材を使用しているオブジェクトは、明らかに正しくないと表示されます。
            //しかし、これはUnityのデフォルトパイプラインのマテリアルにのみ当てはまり、そのシェーダーはForwardBaseパスを持っています。
            //PrepassBase,Always,Vertex,VertexLMRGBM,VertexLMなど、様々なパスで識別できる組み込みのシェーダーがほかにもあります。
            //
            //幸い、SetShaderPassNameを呼び出すことによって、描画設定に複数のパスを追加することが可能です。
            
            drawSetting.SetShaderPassName(1,new ShaderPassName("PrepassBase"));
            drawSetting.SetShaderPassName(2,new ShaderPassName("Always"));
            drawSetting.SetShaderPassName(3,new ShaderPassName("Vertex"));
            drawSetting.SetShaderPassName(4,new ShaderPassName("VertexLMRGBM"));
            drawSetting.SetShaderPassName(5,new ShaderPassName("VertexLM"));

            
            var filterSetting = new FilterRenderersSettings(true);
            
            renderContext.DrawRenderers(cull.visibleRenderers,ref drawSetting,filterSetting);
        }
        
        //レンダリング状態を制御するにはレンダリングコンテキストを通すことで描画できます。
        public override void Render(ScriptableRenderContext renderContext, Camera[] cameras)
        {
            base.Render(renderContext,cameras);

            //Unityのパイプラインもこのようなアプローチを用いてカメラをループさせる。
            foreach (var camera in cameras)
            {
                Render(renderContext,camera);
            }
        }

        //カリング中に、Unityはどのライトが表示されているかも把握します。
        //この情報は、visibleLightsの結果の一部であるvisibleListを通して可能になります。
        //リストの要素は、必要なすべてのデータを含むVisibleLight構造体です。
        //必要なConfigureメソッドを作成し、ループさせる。
        //
        //指向性ライトの方向はその回転によって決定されます。
        //光はそのローカルZ軸に沿って光ります。
        //VisibleLight.localToWorld行列フィールドを介して、このベクトルをワールド空間で見つけることができます。
        //その行列の3列目は、変換されたローカルZ方向ベクトルをを定義します。
        //これは、インデックス2を引数として、Matrix4x4.GetColumnメソッドを介して取得できます。
        //
        //これにより、光が当たっている方向がわかりますが、シェーダーではサーフェスから光源への方向を使用します。
        //そのため、visibleLightDirectionを割り当てる前に、ベクトルを無効にする必要があります。
        //方向ベクトルの4番目のコンポーネントは常に0であるため、X,Y,Zを無効にするだけでよいです。
        private void ConfigureLights()
        {            
            for (var i = 0; i < cull.visibleLights.Count; i++)
            {
                //サポートするライトの数以上forを回さない。
                if(maxVisibleLights == i)
                    break;
                
                var light = cull.visibleLights[i];
                //ライトの色を保持する。
                visibleLightColors[i] = light.finalColor;
                var attenuation = Vector4.zero;
                attenuation.w = 1f;

                //ConfigureLightsはVisibleLight.lightTypeを使用して各ライトの種類を確認できます。
                //ディレクショナルライトの場合、方向を記憶するのは正しい。
                //それ以外であれば、代わりにライトのワールド位置を格納します。
                //これは、ローカルからワールドへの行列の4列目から抽出できます。
                switch (light.lightType)
                {
                    case LightType.Directional:
                    {
                        var v = light.localToWorld.GetColumn(2);

                        v.x = -v.x;
                        v.y = -v.y;
                        v.z = -v.z;

                        visibleLightDirectionsOrPositions[i] = v;
                        break;
                    }
                    
                    case LightType.Point:
                    case LightType.Area:
                    case LightType.Disc:
                    case LightType.Spot:
                    {
                        visibleLightDirectionsOrPositions[i] = light.localToWorld.GetColumn(3);
                        attenuation.x = 1f / Mathf.Max(light.range * light.range, 0.00001f);
                        
                        //指向性ライトと同じように方向ベクトルを設定する。                    
                        if (light.lightType == LightType.Spot)
                        {
                            var v = light.localToWorld.GetColumn(2);
                        
                            v.x = -v.x;
                            v.y = -v.y;
                            v.z = -v.z;

                            visibleLightSpotDirections[i] = v;

                            //スポットライトの円錐は、180°未満の正の角度で指定されます。
                            //スポットライトの方向と光の方向の内積をとることで、サーフェスポイントが円錐内にあるかどうかを判断できます。
                            //結果が最大でも設定されたスポット角度の半分の余弦である場合、フラグメントは影響を受けます。
                            //
                            //円錐の端には瞬間的なカットオフはありません。
                            //代わりに、光が消える推移範囲があります。
                            //この範囲は、退色が始まる内側のスポット範囲と光強度がゼロに達する外側の角度を設定することしかできません。
                            //Unityのデフォルトパイプラインは、フォールオフを決定するためにライトクッキーを使用しますが、
                            //LWRPは内角と外角の間の固定関係を仮定する滑らかな関数でフォールオフを計算します。
                            //
                            //減衰を決定するには、スポットの角度の半分を度からRadに変換して、その余弦を計算します。
                            var outerRad = Mathf.Deg2Rad * 0.5f * light.spotAngle;
                            var outerCos = Mathf.Cos(outerRad);
                            var outerTan = Mathf.Tan(outerRad);
                            var innerCos = Mathf.Cos(Mathf.Atan((46f / 64f) * outerTan));
                            var angleRange = Mathf.Max(innerCos - outerCos, 0.001f);
                            attenuation.z = 1f / angleRange;
                            attenuation.w = -outerCos * attenuation.z;
                        }
                    }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                visibleLightAttenuation[i] = attenuation;
            }

            //現在は最大16個の光をサポートしていますが、シーン内に十分な数の光があれば、範囲を超えてしまう可能性があります。
            //その場合、レンダリング時に、全体的にもっとも重要度の低いライトは省略されます。
            //ただし、それはシェーダーにデータをコピーしないためです。
            //Unityはこれを認識せず、オブジェクトごとのライトインデックスリストからこれらのライトを削除しません。
            //したがって、境界外のライトインデックスになる可能性があります。
            //これを防ぐには、いくつかのライトが削除されたことをUnityに伝えるひ津用があります。
            //
            //すべての可視ライトのインデックスのリストを取得するには、cullの結果に対してGetLightIndexMapを呼び出します。
            //Unityではこのマッピングを変更し、SetLightIndexMapを介して結果に割り当てることができます。
            //つまり、Unityではインデックスが-1に変更されたすべてのライトがスキップされます。
            //これは、最大値を超えるすべてのライトについて、ConfigureLight終了時に行います。

            if (cull.visibleLights.Count > maxVisibleLights)
                return;
            
            var lightIndices = cull.GetLightIndexMap();
            for (var i = 0; i < cull.visibleLights.Count; i++)
            {
                lightIndices[i] = -1;
            }
            cull.SetLightIndexMap(lightIndices);
        }

        //コンテキストを引数として、影をレンダリングするための別のメソッドを作成します。
        //最初にやることは、RenderTextureを取得することです。
        //静的RenderTexture.GetTemporaryメソッド使用し、これを行います。
        //それは新しいRenderTextureを作成するか、まだクリーンアップされていない古いものを再利用します。
        //ほとんどの場合、影は毎フレーム必要になります。
        //
        //マップの幅、そして広さ、深度チャンネルに使用されるビット数、最後にテクスチャフォーマットを指定します。
        //深度チャンネルには16ビットを使用するので、高精度です。
        private void RenderShadows(ScriptableRenderContext context)
        {
            shadowMap = RenderTexture.GetTemporary(512, 512, 16, RenderTextureFormat.Shadowmap);

            //フィルターをバイリニアにする。
            shadowMap.filterMode = FilterMode.Bilinear;
            //ラップモードをクランプにする。
            shadowMap.wrapMode = TextureWrapMode.Clamp;
            
            //レンダリングターゲットを設定する。
            //影をレンダリングするまえに、まずGPUにシャドウマップをレンダリングするように指示します。
            //これを行う便利な方法は、引数にコマンドバッファとシャドウマップを指定してCoreUtils.SetRenderTargetを呼び出すことで可能です。
            //
            //深さチャンネルだけをクリアする必要があるので、設定する。
            //DontCareにすることで、GPUを少し効率的に使用することができます。
            CoreUtils.SetRenderTarget(shadowBuffer,shadowMap,RenderBufferLoadAction.DontCare,RenderBufferStoreAction.Store,ClearFlag.Depth);
            
            shadowBuffer.BeginSample("Render Shadows");
            context.ExecuteCommandBuffer(shadowBuffer);
            shadowBuffer.Clear();
            
            //この考え方は、光源の視点から影をレンダリングするということです。
            //つまり、スポットライトをカメラのように使用しているということです。
            //したがって、適切なビューと射影行列を提供する必要があります。
            //ComputeSpotShadowMatricesAndCullingPrimitivesを使用して、これらの行列を取得できます。
            //シーン内に１つしかライトが存在しないので、単にゼロをセットします。
            Matrix4x4 viewMatrix, projectionMatrix;
            ShadowSplitData splitData;
            cull.ComputeSpotShadowMatricesAndCullingPrimitives(0, out viewMatrix, out projectionMatrix, out splitData);
            
            //行列をセットし、クリアする。
            shadowBuffer.SetViewProjectionMatrices(viewMatrix,projectionMatrix);
            context.ExecuteCommandBuffer(shadowBuffer);
            shadowBuffer.Clear();
            
            //適切な行列を配置したら、すべてのシャドウキャスティングオブジェクトのレンダリングをすることができます。
            //コンテキストで、DrawShadowsを呼び出すことにより、影の描画をすることができます。
            //このメソッドには、DrawShadowSetting引数があり、これはライトインデックスをパラメータとしてDrawShadowsSettingコンストラクタメソッドを介して作成できます。
            var shadowSetting = new DrawShadowsSettings(cull,0);
            context.DrawShadows(ref shadowSetting);

            //しかし、ここでもまた、クリップスペースのZの寸法が逆になっているかどうかによって違いがあります。
            //これは、SystemInfo.usesReservedZBufferで確認できます。
            //そうであれば、乗算するまえに投影行列のZ成分の行を無効にする必要があります。
            if (SystemInfo.usesReversedZBuffer)
            {
                projectionMatrix.m20 = -projectionMatrix.m20;
                projectionMatrix.m21 = -projectionMatrix.m21;
                projectionMatrix.m22 = -projectionMatrix.m22;
                projectionMatrix.m23 = -projectionMatrix.m23;
            }

            //これでワールド空間からシャドウ空間への変換行列が完成しました。
            //しかし、クリップ空間は-1から1になり、テクスチャ座標と深度は0から1になります。
            //この範囲変換は、すべての次元で1/2単位でスケールとオフセットを行うマトリックスとの追加の乗算によって、この行列に上書きすることができます。
            var scaleOffset = Matrix4x4.TRS(Vector3.one * 0.5f, Quaternion.identity, Vector3.one * 0.5f);
            
            //この行列は、影をレンダリングするときに使用したビュー行列と投影行列を乗算し、それをSetGlobalMatrixを介してGPUに送信します。
            var worldToShadowMatrix = scaleOffset * (projectionMatrix * viewMatrix);
            shadowBuffer.SetGlobalMatrix(worldToShadowMatrixID,worldToShadowMatrix);
            
            //実行される前に、シャドウマップをバインドする。
            shadowBuffer.SetGlobalTexture(shadowMapID,shadowMap);
            shadowBuffer.EndSample("Render Shadows");
            context.ExecuteCommandBuffer(shadowBuffer);
            shadowBuffer.Clear();
        }
    }
}
