﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering;

namespace Render.Pipeline
{
    [CreateAssetMenu(menuName = "Rendering/Pipeline")]
    public class PipelineAsset : RenderPipelineAsset
    {
        //動的バッチ処理は利点になることがありますが、重くなったりすることもあります。
        //シーンに同じマテリアルを共有する小さなメッシュがたくさん含まれていない場合、動的バッチ処理を無効にするのが良いです。
        //そこで、動的バッチ処理を有効にするためのオプションをパイプラインに追加します。
        [SerializeField] private bool dynamicBatching = false;
        
        //GPUのインスタンス化はデフォルトで有効になっていますが、カスタム描画フラグを使用してオーバーライドしています。
        //なので、GPUのインスタンス化もオプションにします。
        //これにより、結果の有無を比較することが容易になります。
        [SerializeField] private bool instancing = true;
                
        protected override IRenderPipeline InternalCreatePipeline()
        {
            return new Pipeline(dynamicBatching,instancing);
        }
    }
}
