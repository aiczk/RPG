﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstancedColor : MonoBehaviour
{
    //より多くの色をシーンに含めたい場合、より多くのマテリアルを作成する必要があり、最終的には多くのバッチ処理が走ることになります。
    //しかし、行列を配列に入れることができれば、色に対しても同じことができるはずです。
    //次に、異なる色のオブジェクトを1つのバッチで結合できます。
    //
    //オブジェクトごとに固有の色をサポートするための最初の手順は、それぞれの色を個別に設定できるようにすることです。
    //これはすべてのオブジェクトが共有するアセットであるため、マテリアルを使用して行うことはできません。
    //InstancedColorという名前のコンポーネントを作成し、単一の設定可能のコンポーネントを作成します。
    //パイプラインに固有ではないので、フォルダ外に置いておきます。
    [SerializeField] private Color color = Color.white;
    private static MaterialPropertyBlock property;
    
    //また、Shader.PropertyToIDメソッドを介してプロパティIDをプリフェッチすることで、Colorプロパティを若干高速化することができます。
    //各シェーダープロパティ名は、グローバル識別子の整数を取得します。
    //これらの識別子は変更される可能性がありますが、再生している間などは常に一定です。
    //そのため、静的フィールドのデフォルト値として実行できるように、1度フェッチします。
    private static int colorID = Shader.PropertyToID("_Color");
    
    private void Awake()
    {
        OnValidate();
    }
    
    //マテリアルの色を上書きするには、オブジェクトのレンダラーコンポーネントにmaterialブロックを提供する必要があります。
    //そのためには、新しいマテリアルプロパティブロックオブジェクトインスタンスを作成し、SetColorメソッドで_Colorプロパティを指定してから、
    //SetPropertyBlockを呼び出してオブジェクトのMeshRendererコンポーネントに渡します。
    private void OnValidate()
    {
        if (property == null)
            property = new MaterialPropertyBlock();
        property.SetColor(colorID,color);
        GetComponent<MeshRenderer>().SetPropertyBlock(property);
    }
}
