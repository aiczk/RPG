﻿Shader "Pipeline/Lit"
{

    Properties
    {
        //これでマテリアルの色を調整できるようになりましたが、描画されるものに対しては影響しません。
        //Includeファイルに _Color変数を追加し、float4 _Colorの固定値の代わりにそれを返します。
        //色はマテリアルごとに定義されているので、マテリアルが切り替わった時にのみ変更する必要がある定数バッファに入れることができます。
        _Color ("Color",Color) = (1,1,1,1)
    }
    
    SubShader
    {
    
        Pass
        {
            //少なくとも、ほとんどのプラットフォームでシェーダーが再び機能します。
            //ライブラリを含めた後、このシェーダーはOpenGL ES 2用にコンパイルできません。
            //これは、デフォルトでUnityがコアライブラリでは動作しないOpenGL ES2用のシェーダーコンパイラを使用するために起こります。
            //#pragma prefer_hlslcc glesをシェーダーに追加することによってこれを修正できます。(UnityがLWRPのシェーダーに対して行っている。)
            //ただし、それを行う代わりに、古いデバイスには全く機能しません。
            //これを行うには、#pragma targetディレクティブを使用して、デフォルトレベルを2.5 -> 3.5にする必要があります。
            HLSLPROGRAM
            
            #pragma target 3.5
            
            //GPUインスタンス化がパイプラインで有効になっているからといって、オブジェクトが自動的にインスタンス化されるわけではありません。
            //マテリアルによってサポートされなければなりません。
            //インスタンス化オプションは常に必要なわけではありません。
            //インスタンス化をサポートするものと、そうでないものの2つのシェーダーバリアントが必要です。
            //#pragma multi_compile_instancingを追加することで、必要なバリアントすべてを作成できます。
            //この例では、INSTANCING_ONキーワードが定義されているものと、そうでないものの2つシェーダーバリアントが生成されます。
            #pragma multi_compile_instancing
            
            //オブジェクトから行列のほかに、デフォルトでワールドからオブジェクトへの行列もインスタンス化バッファに置かれます。
            //これらはM行列の逆行列です。
            //これは、非一様スケールを使用するときに法線ベクトルに必要です。
            //しかし、このパイプラインは均一な尺度しか使っていないので、これらを追加する行列は必要ありません。
            //このことは、#pragma instancing_options assumeuniformscalingディレクティブを追加することでUnityに通知できます。
            #pragma instancing_options assumeuniformscaling
            
            #pragma vertex LitPassVertex
            #pragma fragment LitPassFragment
            
            #include "../ShaderLib/Lit.hlsl"
            
            ENDHLSL
        }
        
        //これですべてのオブジェクトはシャドウマップにレンダリングされます。
        //この時点では、オブジェクトは複数のライトの影響を受けないため、GPUインスタンシングは有効的です。
        Pass
        {
            Tags
            {
                "LightMode" = "ShadowCaster"
            }
                    
            HLSLPROGRAM
            			
            #pragma target 3.5
            			
            #pragma multi_compile_instancing
            #pragma instancing_options assumeuniformscaling
            
            #pragma vertex ShadowCasterPassVertex
            #pragma fragment ShadowCasterPassFragment
            			
            #include "../ShaderLib/ShadowCaster.hlsl"
            			
            ENDHLSL
        }
    }
}
