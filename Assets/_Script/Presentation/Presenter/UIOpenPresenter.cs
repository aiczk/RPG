﻿using System;
using System.Collections;
using System.Collections.Generic;
using Domain.UseCase;
using UniRx;
using UnityEngine;

namespace Presentation.Presenter
{
    public interface IUIOpenView
    {
        IObservable<Unit> OnUIOpenAsObservable();
        IObservable<Unit> OnUICloseAsObservable();
    }

    public class UIOpenPresenter : SingletonMonoBehaviour<UIOpenPresenter>, IUIOpenPresenter
    {
        private IUIOpenUseCase useCase;
        private IUIOpenView view;

        private void Start()
        {
            useCase = GetComponent<IUIOpenUseCase>();
            view = GetComponent<IUIOpenView>();
        }

        public IObservable<bool> OnUIStateAsObservable() => useCase.OnUIStateAsObservable();

        IObservable<Unit> IUIOpenPresenter.OnUIOpenAsObservable() => view.OnUIOpenAsObservable();
        IObservable<Unit> IUIOpenPresenter.OnUICloseAsObservable() => view.OnUICloseAsObservable();
    }

    public static class UIS
    {
        public static UIOpenPresenter GetPresenter(this IUIOpenView view) => UIOpenPresenter.Instance;
    }
}
