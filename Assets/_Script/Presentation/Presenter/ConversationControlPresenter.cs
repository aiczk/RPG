﻿using System;
using System.Collections;
using System.Collections.Generic;
using Domain.UseCase;
using UnityEngine;

namespace Presentation.Presenter
{
    public interface IConversationControlView
    {
        
    }
    
    public class ConversationControlPresenter : SingletonMonoBehaviour<ConversationControlPresenter>,IConversationControlPresenter
    {
        private IConversationControlUseCase controlUseCase;

        private void Awake() => controlUseCase = GetComponent<IConversationControlUseCase>();

        public IObservable<bool> OnConversationEventAsObservable() => controlUseCase.OnConversationEventAsObservable();
    }

    public static class CC
    {
        public static ConversationControlPresenter GetPresenter(this IConversationControlView view) =>
            ConversationControlPresenter.Instance;
    }
}