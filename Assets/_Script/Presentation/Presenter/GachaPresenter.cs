﻿using System;
using System.Collections;
using System.Collections.Generic;
using Domain.UseCase;
using UniRx;
using UnityEngine;

namespace Presentation.Presenter
{
    public interface IGachaView
    {
        IObservable<Unit> OnSingleGachaClicked();
        IObservable<Unit> OnMultiGachaClicked();
    }
    
    public class GachaPresenter : SingletonMonoBehaviour<GachaPresenter>,IGachaPresenter
    {
        private IGachaView view;
        private IGachaUseCase useCase;

        private void Awake()
        {
            useCase = GetComponent<IGachaUseCase>();
            view = GetComponent<IGachaView>();
        }

        public IObservable<GachaItemData> OnSingleGachaAsObservable() => useCase.OnSingleGachaAsObservable();
        public IObservable<GachaItemData[]> OnMultipleGachaAsObservable() => useCase.OnMultipleGachaAsObservable();

        IObservable<Unit> IGachaPresenter.OnSingleGachaClicked() => view.OnSingleGachaClicked();
        IObservable<Unit> IGachaPresenter.OnMultiGachaClicked() => view.OnMultiGachaClicked();
    }

    public static class GS
    {
        public static GachaPresenter GetPresenter(this IGachaView view) => GachaPresenter.Instance;
    }
}
