﻿using System;
using System.Collections;
using System.Collections.Generic;
using Domain.UseCase;
using UnityEngine;

namespace Presentation.Presenter
{
    public interface IStageSelectView
    {
        
    }
    
    public class StageSelectPresenter : SingletonMonoBehaviour<StageSelectPresenter>,IStageSelectPresenter
    {
        private IStageSelectUseCase useCase = default;

        private void Awake() => useCase = GetComponent<IStageSelectUseCase>();

        public IObservable<(string title, string descript)> OnSendStageDataAsObservable() =>
            useCase.OnSendStageDataAsObservable();

        public IObservable<SceneObject> OnStageSceneAsObservable() => useCase.OnStageSceneAsObservable();

        public IObservable<Sprite> OnBackGroundImageAsObservable() => useCase.OnBackGroundImageAsObservable();

        public IObservable<Sprite> OnThumbNailImageAsObservable() => useCase.OnThumbNailImageAsObservable();
    }

    public static class ssv
    {
        public static StageSelectPresenter GetPresenter(this IStageSelectView view) => StageSelectPresenter.Instance;
    }
}
