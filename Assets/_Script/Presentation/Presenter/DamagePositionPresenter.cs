﻿using System;
using System.Collections;
using System.Collections.Generic;
using Domain.UseCase;
using UnityEngine;

namespace Presentation.Presenter
{
    public interface IDamagePositionView
    {
        
    }
    
    public class DamagePositionPresenter : SingletonMonoBehaviour<DamagePositionPresenter>,IDamagePositionPresenter
    {
        private IDamagePositionUseCase positionUseCase;

        private void Awake() => positionUseCase = GetComponent<IDamagePositionUseCase>();

        public IObservable<(Vector3 pos, int damage)> OnDamagePositionAsObservable() =>
            positionUseCase.OnDamagePositionAsObservable();
    }

    public static class Dam
    {
        public static DamagePositionPresenter GetPresenter(this IDamagePositionView view) =>
            DamagePositionPresenter.Instance;
    }
}
