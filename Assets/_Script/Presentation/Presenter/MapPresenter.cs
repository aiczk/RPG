﻿using System;
using System.Collections;
using System.Collections.Generic;
using Domain.UseCase;
using UnityEngine;

namespace Presentation.Presenter
{
    public interface IMapView
    {
        
    }
    
    public class MapPresenter : SingletonMonoBehaviour<MapPresenter>,IMapPresenter
    {
        private IMapUseCase mapUseCase;
        
        private void Awake()
        {
            mapUseCase = GetComponent<IMapUseCase>();
        }

        public IObservable<Vector3> XYAxisMoveAsObservable() => mapUseCase.XYAxisMoveAsObservable();
    }

    public static class MV
    {
        public static MapPresenter GetPresenter(this IMapView map) => MapPresenter.Instance;
    }
}
