﻿using System;
using System.Collections;
using System.Collections.Generic;
using Domain.UseCase;
using UniRx;
using UnityEngine;

namespace Presentation.Presenter
{
    public interface IDeathView
    {
        
    }
    
    public class DeathPresenter : SingletonMonoBehaviour<DeathPresenter>,IDeathPresenter
    {
        private IDeathUseCase deathUseCase = default;

        private void Awake() => deathUseCase = GetComponent<IDeathUseCase>();

        public IObservable<Unit> OnDeathAsObservable() => deathUseCase.OnDeathEventAsObservable();
    }

    public static class DiePre
    {
        public static DeathPresenter GetPresenter(this IDeathView deathView) => DeathPresenter.Instance;
    }
}
