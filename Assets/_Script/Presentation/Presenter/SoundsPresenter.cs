﻿using System;
using System.Collections;
using System.Collections.Generic;
using Domain.UseCase;
using UnityEngine;

namespace Presentation.Presenter
{
    public interface ISoundsView
    {
        
    }
    
    public class SoundsPresenter : SingletonMonoBehaviour<SoundsPresenter>,ISoundsPresenter
    {
        private ISoundsUseCase soundsUseCase = default;

        private void Awake()
        {
            soundsUseCase = GetComponent<ISoundsUseCase>();
        }

        public IObservable<float> OnBGMVolumeChangedAsObservable() => soundsUseCase.OnBGMVolumeChangedAsObservable();
        public IObservable<float> OnSEVolumeChangedAsObservable() => soundsUseCase.OnSEVolumeChangedAsObservable();
    }

    public static class SoundsPres
    {
        public static SoundsPresenter GetPresenter(this ISoundsView soundsView) => SoundsPresenter.Instance;
    }
}
