﻿using System;
using System.Collections;
using System.Collections.Generic;
using Domain.UseCase;
using UnityEngine;

namespace Presentation.Presenter
{
    public interface IBringsView
    {
        
    }
    
    public class BringsPresenter : SingletonMonoBehaviour<BringsPresenter>,IBringsPresenter
    {
        private IBringsUseCase bringsUseCase;

        private void Awake()
        {
            bringsUseCase = GetComponent<IBringsUseCase>();
        }

        public IObservable<GachaItemData[]> OnPlayerBringsDataAsObservable() =>
            bringsUseCase.OnPlayerBringsDataAsObservable();

        public IObservable<bool> OnCurrentWareHousePageStateAsObservable() =>
            bringsUseCase.OnCurrentWareHousePageStateAsObservable();
    }

    public static class BP
    {
        public static BringsPresenter GetPresenter(this IBringsView view) => BringsPresenter.Instance;
    }
}
