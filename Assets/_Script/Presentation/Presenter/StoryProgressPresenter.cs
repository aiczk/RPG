﻿using System;
using System.Collections;
using System.Collections.Generic;
using Domain.UseCase;
using UnityEngine;

namespace Presentation.Presenter
{
    public interface IStoryProgressView
    {
        
    }
    
    public class StoryProgressPresenter : SingletonMonoBehaviour<StoryProgressPresenter>,IStoryProgressPresenter
    {
        private IStoryProgressUseCase useCase;

        private void Awake() => useCase = GetComponent<IStoryProgressUseCase>();

        public IObservable<bool> OnInteractiveAsObservable() => useCase.OnOpenButtonInteractiveAsObservable();
    }

    public static class SPV
    {
        public static StoryProgressPresenter GetPresenter(this IStoryProgressView view) =>
            StoryProgressPresenter.Instance;
    }
}
