﻿using System;
using System.Collections;
using System.Collections.Generic;
using App.Utility;
using Domain.Entity;
using Domain.UseCase;
using UniRx;
using UnityEngine;

namespace Presentation.Presenter
{
    public interface IQuestView
    {
        
    }
    
    public class QuestPresenter : SingletonMonoBehaviour<QuestPresenter>,IQuestPresenter
    {
        private IQuestUseCase questUseCase = default;
        
        private void Awake()
        {
            questUseCase = GetComponent<IQuestUseCase>();
        }

        public QuestEntity.KillQuest GetQuestData()
        {
            return questUseCase.GetQuestData();
        }

        public IObservable<Unit> MissionCompleted()
        {
            return questUseCase.MissionComplete();
        }

        public IObservable<int> KillCounter()
        {
            return questUseCase.KillCounter();
        }

        public IObservable<QuestProgress> QuestProgress()
        {
            return questUseCase.QuestProgress();
        }

        public IObservable<QuestEntity.KillQuest> QuestData()
        {
            return questUseCase.QuestData();
        }
        
        public void SetQuestData(QuestEntity.KillQuest entity)
        {
            questUseCase.SetQuestData(entity);
        }

        
    }

    public static class Ext
    {
        public static QuestPresenter GetPresenter(this IQuestView view)
        {
            return QuestPresenter.Instance;
        }
    }


}