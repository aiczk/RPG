﻿using System;
using System.Collections;
using System.Collections.Generic;
using Domain.UseCase;
using UniRx;
using UnityEngine;

namespace Presentation.Presenter
{
    public interface IMenuThemeView
    {
        IObservable<Unit> OnThemeColorDecisionAsObservable();
        IObservable<Unit> OnRightArrowAsObservable();
        IObservable<Unit> OnLeftArrowAsObservable();
    }
    
    public class MenuThemePresenter : SingletonMonoBehaviour<MenuThemePresenter>,IMenuThemePresenter
    {
        private IMenuThemeUseCase useCase;
        private IMenuThemeView view;

        private void Awake()
        {
            useCase = GetComponent<IMenuThemeUseCase>();
            view = GetComponent<IMenuThemeView>();
        }

        IObservable<Unit> IMenuThemePresenter.OnThemeDecisionAsObservable() =>
            view.OnThemeColorDecisionAsObservable();

        IObservable<Unit> IMenuThemePresenter.OnLeftArrowAsObservable() => view.OnLeftArrowAsObservable();
        IObservable<Unit> IMenuThemePresenter.OnRightArrowAsObservable() => view.OnRightArrowAsObservable();

        public IObservable<MenuTheme[]> OnPreviewThemeAsObservable() => useCase.OnPreviewThemeAsObservable();
        public IObservable<MenuTheme> OnApplyThemeAsObservable() => useCase.OnApplyThemeAsObservable();
        public IObservable<int> OnCurrentPageAsObservable() => useCase.OnCurrentPageAsObservable();
        public MenuTheme InitializeMenu() => useCase.InitializeMenu();
    }

    public static class TP
    {
        public static MenuThemePresenter GetPresenter(this IMenuThemeView view) => MenuThemePresenter.Instance;
    }
}
