﻿using System;
using System.Collections;
using System.Collections.Generic;
using Domain.UseCase;
using UniRx;
using UnityEngine;

namespace Presentation.Presenter
{
    public interface IStoryView
    {
        
    }
    
    public class StoryPresenter : SingletonMonoBehaviour<StoryPresenter>,IStoryPresenter
    {
        private IStoryUseCase useCase;

        private void Awake() => useCase = GetComponent<IStoryUseCase>();
        
        public IObservable<StoryDataTable> OnSendStoryAsObservable() => useCase.OnSendStoryAsObservable();
        public IObservable<Unit> OnInitializeTextAsObservable() => useCase.OnInitializeTextAsObservable();
    }

    public static class SV
    {
        public static StoryPresenter GetPresenter(this IStoryView view) => StoryPresenter.Instance;
    }
}
