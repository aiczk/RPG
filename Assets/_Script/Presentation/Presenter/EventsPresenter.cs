﻿using System;
using Domain.UseCase;
using UnityEngine;

namespace Presentation.Presenter
{
    public interface IEventView
    {
        
    }
    
    public class EventsPresenter : SingletonMonoBehaviour<EventsPresenter>,IEventPresenter
    {
        private IEventUseCase useCase;
        
        private void Awake()
        {
            useCase = GetComponent<IEventUseCase>();
        }

        public IObservable<Vector2> OnEventImagePositionAsObservable() => useCase.OnEventImagePositionAsObservable();
        public IObservable<bool> OnEventImageVisibleAsObservable() => useCase.OnEventImageVisibleAsObservable();
    }

    public static class EPP
    {
        public static EventsPresenter GetPresenter(this IEventView view) => EventsPresenter.Instance;
    }
}
