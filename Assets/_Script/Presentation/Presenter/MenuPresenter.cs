﻿using System;
using System.Collections;
using System.Collections.Generic;
using App.Utility;
using Domain.UseCase;
using UniRx;
using UnityEngine;

namespace Presentation.Presenter
{
    public interface IMenuView
    {
        IObservable<Unit> OnSettingButtonClickAsObservable();
        IObservable<Unit> OnWareHouseButtonClickAsObservable();
        IObservable<Unit> OnGachaButtonClickAsObservable();
        IObservable<Unit> OnStoreButtonClickAsObservable();
        IObservable<Unit> OnNotificationButtonClickAsObservable();
    }
    
    public class MenuPresenter : SingletonMonoBehaviour<MenuPresenter>,IMenuPresenter
    {
        private IMenuUseCase useCase;
        private IMenuView view;

        private void Awake()
        {
            useCase = GetComponent<IMenuUseCase>();
            view = GetComponent<IMenuView>();
        }

        public IObservable<MenuIndex> OnMenuChoiceAsObservable() => useCase.OnMenuChoiceAsObservable();

        IObservable<Unit> IMenuPresenter.OnSettingButtonClickAsObservable() => view.OnSettingButtonClickAsObservable();
        IObservable<Unit> IMenuPresenter.OnWareHouseButtonClickAsObservable() =>
            view.OnWareHouseButtonClickAsObservable();
        IObservable<Unit> IMenuPresenter.OnGachaButtonClickAsObservable() => view.OnGachaButtonClickAsObservable();
        IObservable<Unit> IMenuPresenter.OnStoreButtonClickAsObservable() => view.OnStoreButtonClickAsObservable();
        IObservable<Unit> IMenuPresenter.OnNotificationButtonClickAsObservable() =>
            view.OnNotificationButtonClickAsObservable();
    }

    public static class Ms
    {
        public static MenuPresenter GetPresenter(this IMenuView view) => MenuPresenter.Instance;
    }
}
