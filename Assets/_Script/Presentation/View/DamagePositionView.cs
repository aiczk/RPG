﻿using System;
using System.Collections;
using Presentation.Presenter;
using TMPro;
using UniRx;
using UniRx.Async;
using ReMotion;
using ReMotion.Extensions;
using Save;
using UnityEngine;
using TweenExtensions = ReMotion.Extensions.Tweening.TweenExtensions;

namespace Presentation.View
{
    public class DamagePositionView : MonoBehaviour,IDamagePositionView
    {
        [SerializeField] private TextMeshProUGUI damageText = default;
        private readonly float transparency = 0;
        private Vector3 damageTempPosition = default;
        
        private void Start()
        {
            damageText.alpha = 0;

            this.GetPresenter()
                .OnDamagePositionAsObservable()
                .Subscribe(x =>
                {
                    var (position, damage) = x;
                    
                    damageText.SetText(string.Intern(damage.ToString()));
                    Initialize(position);
                    MovePosition(position.WorldToViewPort());
                    AnimatePosition();
                });

            SingletonUtility
                .GetPlayerTransform()
                .ObserveEveryValueChanged(x => (x.rotation, x.position))
                .Where(x => !damageText.alpha.Equals(transparency))
                .Subscribe(x => MovePosition(damageTempPosition.WorldToViewPort()));

        }

        private void Initialize(Vector3 position)
        {
            damageText.alpha = 1;
            damageTempPosition = position;
        }

        private void MovePosition(Vector3 position) => damageText.transform.position = position;

        private void AnimatePosition()
        {
            damageText.transform.TweenPositionY(0.7f, 1);
            damageText.TweenAlpha(transparency, 0.8f);
        }
    }
}
