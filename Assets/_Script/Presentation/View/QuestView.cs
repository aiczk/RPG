﻿using System.Collections;
using System.Collections.Generic;
using App.Utility;
using Domain.Entity;
using Presentation.Presenter;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Presentation.View
{
    public class QuestView : MonoBehaviour,IQuestView
    {
        private QuestProgress progress = default;
       
        private QuestEntity.KillQuest killQuest;
        [SerializeField] private Text missionText = default;
        
        //描画
        private void Start()
        {
            MissionTextOverwrite("なし");
            
            killQuest = this.GetPresenter().GetQuestData();
            
            this.GetPresenter()
                .KillCounter()
                .Subscribe(x =>
                {
                    Debug.Log($"value :{x}");
                    MissionTextOverwrite($"{killQuest.Name} ×{x}");
                });

            this.GetPresenter()
                .MissionCompleted()
                .Subscribe(x =>
                {
                    Debug.Log("完了");
                    MissionTextOverwrite("討伐完了！",false);
                });

            this.GetPresenter()
                .QuestProgress()
                .Subscribe(x =>
                {
                    Debug.Log($"Player :{x}");
                    progress = x;
                });

            this.GetPresenter()
                .QuestData()
                .Subscribe(x =>
                {
                    MissionTextOverwrite($"{x.Name} ×{x.KillCount}");
                });

        }

        private void MissionTextOverwrite(string text,bool option = true)
        {
            if (option)
                missionText.text = "討伐 :" + text;
            else
                missionText.text = text;
        }

    }

}
