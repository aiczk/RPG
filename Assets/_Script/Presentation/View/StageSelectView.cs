﻿using System;
using System.Collections;
using System.Collections.Generic;
using Presentation.Presenter;
using TMPro;
using UniRx;
using UniRx.Async;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Presentation.View
{
    public class StageSelectView : MonoBehaviour,IStageSelectView
    {
        [SerializeField] private TextMeshProUGUI title = default, description = default,progressText = default;
        [SerializeField] private Image backGround = default,thumbNailImage = default,loadProgress = default;

        private void Awake()
        {
            SetUpProgress(false);
            
            this.GetPresenter()
                .OnStageSceneAsObservable()
                .Subscribe(async x =>
                {
                    SetUpProgress(true);

                    var stageSelect = SceneManager.GetActiveScene().name;

                    var load = await SceneManager
                        .LoadSceneAsync(x, LoadSceneMode.Additive)
                        .AsObservable()
                        .TakeWhile(y => y.progress >= 1);
                    
                    loadProgress.fillAmount = load.progress;
                    progressText.SetText($"{(Mathf.Round(load.progress) * 100).ToString()}%");
                    
                    await SceneManager.UnloadSceneAsync(stageSelect);
                });

            this.GetPresenter()
                .OnSendStageDataAsObservable()
                .Subscribe(SetTexts);

            this.GetPresenter()
                .OnThumbNailImageAsObservable()
                .Subscribe(x => thumbNailImage.SetImage(x));

            //TODO: なんか推移するアニメーションさせる
            this.GetPresenter()
                .OnBackGroundImageAsObservable()
                .Subscribe(x => backGround.SetImage(x));
        }

        private void SetTexts((string , string) data)
        {
            var (item1, item2) = data;
            title.SetText(item1);
            description.SetText(item2);
        }

        private void SetUpProgress(bool state)
        {
            progressText.SetActive(state);
            loadProgress.enabled = state;
        }
    }
}
