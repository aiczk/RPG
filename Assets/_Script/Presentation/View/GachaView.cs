﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Presentation.Presenter;
using Sirenix.Utilities;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.UI;

namespace Presentation.View
{
    public class GachaView : MonoBehaviour,IGachaView
    {
        [SerializeField] private Button singleGacha = default, multiGacha = default;
        [SerializeField] private Image gachaImagePrefab = default;
        [SerializeField] private Transform parent = default;
        [SerializeField] private Button completed = default;
        private ImagePool pool;
        private Image[] multiple = new Image[10];
        private Vector2 pos = new Vector2(500,0);

        private void Awake()
        {
            pool = new ImagePool(gachaImagePrefab,parent);

            pool.PreloadAsync(11, 11).Subscribe();
        }
        
        private void Start()
        {            
            this.OnDestroyAsObservable()
                .Subscribe(x => pool.Dispose());

            completed
                .OnClickAsObservable()
                .Subscribe(x => ResetImages(multiple));
             
            this.GetPresenter()
                .OnSingleGachaAsObservable()
                .Subscribe(x =>
                {
                    ResetImages(multiple);
                    multiple[0] = pool.Rent().SetImage(x.GetItemSprite()).SetPosition(Vector2.zero);
                });

            this.GetPresenter()
                .OnMultipleGachaAsObservable()
                .Subscribe(x =>
                {
                    ResetImages(multiple);
                    SetMultipleImages(pool, x);
                });
        }

        private void SetMultipleImages(ImagePool pool,IReadOnlyList<GachaItemData> gachaResult)
        {
            for (var i = 0; i < 10; i++)
                multiple[i] = pool
                    .Rent()
                    .SetImage(gachaResult[i].GetItemSprite())
                    .SetPosition(i <= 4 ? pos + new Vector2(100 * i - 700, 0) : new Vector2(100 * i - 700, 100));
        }

        private void ResetImages(Image[] images)
        {
            foreach (var sprite in images)
            {
                if (sprite == null)
                    continue;
                
                if(!sprite.gameObject.activeSelf)
                    continue;
                
                pool.Return(sprite);
            }
        }

        IObservable<Unit> IGachaView.OnSingleGachaClicked() => singleGacha.OnClickAsObservable();
        IObservable<Unit> IGachaView.OnMultiGachaClicked() => multiGacha.OnClickAsObservable();
    }
}
