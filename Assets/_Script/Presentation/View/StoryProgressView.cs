﻿using System.Collections;
using System.Collections.Generic;
using Presentation.Presenter;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Presentation.View
{
    public class StoryProgressView : MonoBehaviour,IStoryProgressView
    {
        [SerializeField] private Button decision = default;

        private void Start()
        {
            this.GetPresenter()
                .OnInteractiveAsObservable()
                .Subscribe(x => decision.interactable = x);
        }
    }
}
