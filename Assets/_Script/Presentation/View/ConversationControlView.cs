﻿using Presentation.Presenter;
using ReMotion;
using ReMotion.Extensions.Tweening;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Presentation.View
{
    public class ConversationControlView : MonoBehaviour,IConversationControlView
    {
        [SerializeField] private Image leftStandPicture = default;
        [SerializeField] private Image rightStandPicture = default;
        [SerializeField] private Image underBoard = default;
        [SerializeField] private TextMeshProUGUI ubt = default;
        
        private void Start()
        {
            this.GetPresenter()
                .OnConversationEventAsObservable()
                .Subscribe(x =>
                {
                    CharacterInAppearance(leftStandPicture,-500,x);
                    CharacterInAppearance(rightStandPicture,500,x);
                    UIMove(underBoard,-400,x);
                    AnimationTextAlpha(ubt, x);
                });
        }
        
        
        private void CharacterInAppearance(Graphic chara,float to,bool isSpeak,float duration = 2f)
        {
            if (isSpeak)
            {
                chara.rectTransform.TweenLocalPositionX(to, duration,EasingFunctions.EaseOutExpo);
                chara.TweenAlpha(1, 1);
            }
            else
            {
                chara.rectTransform.TweenLocalPositionX(to * 2, duration, EasingFunctions.EaseOutExpo);
                chara.TweenAlpha(0, 1);
            }
        }

        private void UIMove(Graphic ui, float to, bool isSpeak,float duration = 1.5f)
        {
            if (isSpeak)
            {
                ui.rectTransform.TweenLocalPositionY(to, duration,EasingFunctions.EaseOutExpo);
                ui.TweenAlpha(1, duration);
            }
            else
            {
                ui.rectTransform.TweenLocalPositionY(to * 2, duration,EasingFunctions.EaseOutExpo);
                ui.TweenAlpha(0, duration);
            }
        }

        private void AnimationTextAlpha(Graphic ui, bool isSpeak)
        {
            ui.TweenAlpha(isSpeak ? 1 : 0, 1);
        }
    }
}
