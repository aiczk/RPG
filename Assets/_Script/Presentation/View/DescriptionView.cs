﻿using System.Collections;
using System.Collections.Generic;
using Presentation.Presenter;
using Sirenix.OdinInspector;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

#pragma warning disable 0649
namespace Presentation.View
{
    public class DescriptionView : MonoBehaviour,IBringsView
    {
        [SerializeField, BoxGroup("Description")]
        private GameObject descriptionObj;
        
        [SerializeField, BoxGroup("Description")]
        private Button descClose;

        private void Start()
        {
            descriptionObj.SetActive(false);

            descClose
                .OnClickAsObservable()
                .Subscribe(x => descriptionObj.SetActive(false));
        }
    }
}
