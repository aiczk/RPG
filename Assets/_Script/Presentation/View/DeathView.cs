﻿using System.Collections;
using System.Collections.Generic;
using Presentation.Presenter;
using UniRx;
using UnityEngine;

namespace Presentation.View
{
    public class DeathView : MonoBehaviour,IDeathView
    {
        [SerializeField] private ParticleSystem ExplosionEffect = default;
        
        private void Start()
        {
            this.GetPresenter()
                .OnDeathAsObservable()
                .Subscribe(x =>
                {
                    ExplosionEffect.Emit(100);
                });
        }
    }
}
