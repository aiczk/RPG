﻿using System.Collections;
using System.Collections.Generic;
using Presentation.Presenter;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Presentation.View
{
    public class EventsView : MonoBehaviour,IEventView
    {
        [SerializeField] private Image speakMark = default;
        
        private void Start()
        {
            this.GetPresenter()
                .OnEventImagePositionAsObservable()
                .Subscribe(x => speakMark.rectTransform.position = x);

            this.GetPresenter()
                .OnEventImageVisibleAsObservable()
                .Subscribe(x => speakMark.enabled = x);
        }
    }
}
