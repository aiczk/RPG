﻿using System;
using Presentation.Presenter;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Presentation.View
{
    public class StoryView : MonoBehaviour, IStoryView
    {
        [SerializeField] private TextMeshProUGUI storyText = default, nameText = default;
        [SerializeField] private Image[] characters = default;

        private void Start()
        {
            this.GetPresenter()
                .OnSendStoryAsObservable()
                .SelectMany
                    (x => Observable
                                .Interval(TimeSpan.FromMilliseconds(100))
                                .TimeInterval()
                                .TakeUntil(this.GetPresenter().OnSendStoryAsObservable()),(data,time) => (data,time.Value)
                    )
                .Where(x => x.Value <= x.data.GetText().Length)
                .Subscribe(x =>
                {
                    var (data, time) = x;
                    DataOpener(data,(int)time);
                });

            this.GetPresenter()
                .OnInitializeTextAsObservable()
                .Subscribe(x =>
                {
                    storyText.SetText("");
                    nameText.SetText("");
                });
        }
        
        private void DataOpener(StoryDataTable data,int time)
        {
            var chara = data.GetCharacterData();
            var sprite = chara.GetExpressions(data.GetExpressionType());
            
            SetImage((int)chara.GetSpeaker(),sprite);
            
            storyText.SetText(data.GetText().Substring(0,time));
            nameText.SetText(chara.GetCharacterName());
        }

        private void SetImage(int index, Sprite sprite)
        {
            var image = characters[index];
            
            image.sprite = sprite;
        }
    }
}
