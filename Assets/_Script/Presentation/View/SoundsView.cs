﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using Presentation.Presenter;
using Sirenix.OdinInspector;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Presentation.View
{
    public class SoundsView : MonoBehaviour, ISoundsView
    {
        private enum SoundUtility
        {
            Zero,
            High
        }

        //[SerializeField] private GameObject parent = default;
        [SerializeField,BoxGroup("Text",centerLabel: true)] private TextMeshProUGUI BGMText = default, SEText = default;

        [SerializeField,BoxGroup("AudioSource",centerLabel: true)] private AudioSource BGM = default, SE = default;
        
        private ReactiveProperty<SoundUtility> BGMStateUtility = new ReactiveProperty<SoundUtility>();
        private ReactiveProperty<SoundUtility> SEStateUtility = new ReactiveProperty<SoundUtility>();

        private void Awake()
        {
            BGM.volume = PlayerPrefs.GetFloat("BGM_VOLUME");
            BGMText.text = string.Intern((PlayerPrefs.GetFloat("BGM_VOLUME") * 100).ToString(CultureInfo.InvariantCulture));
        }
        
        private void Start()
        {   
            this.GetPresenter()
                .OnBGMVolumeChangedAsObservable()
                .Subscribe(x => BGMStateUtility.Value = x <= 0 ? SoundUtility.Zero : SoundUtility.High);

            this.GetPresenter()
                .OnSEVolumeChangedAsObservable()
                .Subscribe(x => SEStateUtility.Value = x <= 0 ? SoundUtility.Zero : SoundUtility.High);
            
            
            this.GetPresenter()
                .OnBGMVolumeChangedAsObservable()
                .Select(x => (float)Math.Round(x,2,MidpointRounding.AwayFromZero))
                .Subscribe(x =>
                {
                    BGMText.SetText(string.Intern((x * 100).ToString(CultureInfo.InvariantCulture)));
                    BGM.volume = x;
                });

            this.GetPresenter()
                .OnSEVolumeChangedAsObservable()
                .Select(x => (float)Math.Round(x,2,MidpointRounding.AwayFromZero))
                .Subscribe(x =>
                {
                    SEText.SetText(string.Intern((x * 100).ToString(CultureInfo.InvariantCulture)));
                    SE.volume = x;
                });
        }
    }

}
