﻿using System;
using System.Collections;
using System.Collections.Generic;
using Presentation.Presenter;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using TMPro;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.UI;

namespace Presentation.View
{
    public class BringsView : MonoBehaviour,IBringsView
    {
        [SerializeField] private Transform parent = default;
        [SerializeField] private Item prefab = default;
        
        private Item[] items = new Item[30];
        private ItemPool pool;

        private void Awake()
        {
            pool = new ItemPool(prefab,parent);

            pool.PreloadAsync(20, 2).Subscribe();
        }
        
        private void Start()
        {   
            this.OnDestroyAsObservable()
                .Subscribe(x => pool.Dispose());
            
            this.GetPresenter()
                .OnPlayerBringsDataAsObservable()
                .Subscribe(Open);

            this.GetPresenter()
                .OnCurrentWareHousePageStateAsObservable()
                .Where(x => !x)
                .Subscribe(x => Close(items));
        }

        private void Open(GachaItemData[] data)
        {
            for (var i = 0; i < data.Length; i++)
            {
                var itemData = data[i];
                
                if (itemData == null)
                    return;

                var item = items[i] = pool.Rent();

                item.SetImage(itemData.GetItemSprite());
                item.SetData(itemData);
            }
        }

        private void Close(Item[] data)
        {
            for (var i = 0; i < data.Length; i++)
            {
                var itemData = data[i];
                
                if (itemData == null)
                    return;
                
                if(!itemData.gameObject.activeSelf)
                    return;
                
                pool.Return(items[i]);
            }
        }
    }
}