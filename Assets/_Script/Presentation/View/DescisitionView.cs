﻿using System.Collections;
using System.Collections.Generic;
using Presentation.Presenter;
using TMPro;
using UniRx;
using UnityEngine;

namespace Presentation.View
{
    public class DescisitionView : MonoBehaviour,IStoryProgressView
    {
        [SerializeField] private TextMeshProUGUI text = default;
        [SerializeField] private string play = default, notPlay = default;

        private void Start() =>
            this.GetPresenter()
                .OnInteractiveAsObservable()
                .Subscribe(x => text.SetText(x ? play : notPlay));
    }
}
