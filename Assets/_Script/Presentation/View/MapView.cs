﻿using System.Collections;
using System.Collections.Generic;
using Presentation.Presenter;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Presentation.View
{
    public class MapView : MonoBehaviour,IMapView
    {
        [SerializeField] private RawImage mapImage = default;
        [SerializeField] private MeshFilter stage = default;
        private float XBound, ZBound;
        
        private void Start()
        {
            var bounds = stage.mesh.bounds.size;
            
            XBound = bounds.x;
            ZBound = bounds.z;

            this.GetPresenter()
                .XYAxisMoveAsObservable()
                .Subscribe(pos =>
                {                    
                    mapImage.uvRect = new Rect
                    (
                        Mathf.Clamp((pos.x - 48) / XBound, -0.2f, 0.8f),
                        Mathf.Clamp((pos.z + 150) / ZBound, -0.09f, 0.9f),
                        0.4f,
                        0.2f
                    );
                });
        }
    }
}
