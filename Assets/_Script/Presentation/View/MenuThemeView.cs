﻿using System;
using System.Collections;
using System.Collections.Generic;
using Presentation.Presenter;
using UniRx;
using UniRx.Toolkit;
using UnityEngine;
using UnityEngine.UI;

namespace Presentation.View
{
    public class MenuThemeView : MonoBehaviour,IMenuThemeView
    {
        [SerializeField] private ThemeData theme = default,currentTheme = default;
        [SerializeField] private GameObject previewParent = default;
        [SerializeField] private Button decision = default, preview = default,right = default,left = default;
        private MenuTheme[] themes;
        private IThemeData themeData,currentData;

        private void Awake()
        {
            themeData = CastTo<IThemeData>.From(theme);
            currentData = CastTo<IThemeData>.From(currentTheme);
            previewParent.SetActive(false);
        }

        private void Start()
        {     
            var menuIndex = this.GetPresenter().InitializeMenu();
            currentData.SetImage(menuIndex.GetBackGroundTheme(), menuIndex.GetButtonTheme());
            
            this.GetPresenter()
                .OnPreviewThemeAsObservable()
                .DistinctUntilChanged(x => x)
                .Subscribe(x => themes = x);

            this.GetPresenter()
                .OnCurrentPageAsObservable()
                .Subscribe(x => themeData.SetData(themes[x.Repeat(0, themes.Length)]));

            this.GetPresenter()
                .OnApplyThemeAsObservable()
                .Subscribe(x => currentData.SetImage(x.GetBackGroundTheme(), x.GetButtonTheme()));
            
            preview
                .OnClickAsObservable()
                .Subscribe(x => previewParent.SetActive(!previewParent.activeSelf));
        }

        IObservable<Unit> IMenuThemeView.OnThemeColorDecisionAsObservable() => decision.OnClickAsObservable();
        IObservable<Unit> IMenuThemeView.OnRightArrowAsObservable() => right.OnClickAsObservable();
        IObservable<Unit> IMenuThemeView.OnLeftArrowAsObservable() => left.OnClickAsObservable();
    }
}
