﻿using System;
using System.Collections;
using System.Collections.Generic;
using Presentation.Presenter;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Presentation.View
{
    public class UIOpenView : MonoBehaviour,IUIOpenView
    {
        [SerializeField] private Canvas targetCanvas = default;
        [SerializeField] private Button open = default, close = default;

        private void Awake()
        {
            targetCanvas.enabled = false;
        }
        
        private void Start()
        {
            this.GetPresenter()
                .OnUIStateAsObservable()
                .Subscribe(x => targetCanvas.enabled = x);
        }

        IObservable<Unit> IUIOpenView.OnUIOpenAsObservable() => open.OnClickAsObservable();
        IObservable<Unit> IUIOpenView.OnUICloseAsObservable() => close.OnClickAsObservable();
    }
}
