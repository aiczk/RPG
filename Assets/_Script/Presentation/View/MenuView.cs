﻿using System;
using System.Collections;
using System.Collections.Generic;
using App.Utility;
using Presentation.Presenter;
using Sirenix.OdinInspector;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Presentation.View
{
    public class MenuView : MonoBehaviour,IMenuView
    {
        [SerializeField,BoxGroup( "BUTTON" )] private Button 
            setting = default,
            chara = default,
            gacha = default,
            store = default,
            notification = default;

        [SerializeField,BoxGroup( "PARENT" )] private GameObject 
            settingParent = default,
            wareHouse = default,
            gachaParent = default,
            storeParent = default,
            notificationParent = default;
        
        private void Start()
        {
            Activate(MenuIndex.Notification);
            
            this.GetPresenter()
                .OnMenuChoiceAsObservable()
                .Subscribe(Activate);

            gacha
                .OnClickAsObservable()
                .Subscribe(x => gachaParent.SetActive(!gachaParent.activeSelf));
        }

        private void Activate(MenuIndex index)
        {
            notificationParent.SetActive(index == MenuIndex.Notification);
            wareHouse.SetActive(index == MenuIndex.WareHouse);
            settingParent.SetActive(index == MenuIndex.Setting);
            gachaParent.SetActive(index == MenuIndex.Gacha);
            storeParent.SetActive(index == MenuIndex.Gacha || index == MenuIndex.Store);
        }

        IObservable<Unit> IMenuView.OnSettingButtonClickAsObservable() => setting.OnClickAsObservable();
        IObservable<Unit> IMenuView.OnWareHouseButtonClickAsObservable() => chara.OnClickAsObservable();
        IObservable<Unit> IMenuView.OnGachaButtonClickAsObservable() => gacha.OnClickAsObservable();
        IObservable<Unit> IMenuView.OnStoreButtonClickAsObservable() => store.OnClickAsObservable();
        IObservable<Unit> IMenuView.OnNotificationButtonClickAsObservable() => notification.OnClickAsObservable();
    }
}