﻿using System;

namespace App.Utility
{
    public enum QuestProgress
    {
        None,
        During,
        Completed,
    }

    public enum EnemyAttribute
    {
        Ground,
        Flying,
    }

    public enum StageID
    {
        stage1,
        stage2,
        stage3,
        stage4,
        stage5,
    }

    public enum MenuIndex
    {
        Notification,
        WareHouse,
        Gacha,
        Setting,
        Store,
    }
    
    public enum DistanceType
    {
        Short = 0,
        Medium = 1,
        Long = 2,
    }
    
    public enum NPCType
    {
        Store,
        Speak
    }
}
