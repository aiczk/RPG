using System;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class Item : MonoBehaviour
{
    [SerializeField]
    private Button open = default;

    private DescriptionData descData;
    private GachaItemData data;
    private GameObject descObj;
    
    private void Start()
    {
        descObj = SingletonUtility.GetDescriptionObject();
        descData = DescriptionData.Instance;
        
        open.OnClickAsObservable()
            .Subscribe(x =>
            {
                descObj.SetActive(true);
                descData.SetData(data.GetDescription(),data.GetItemSprite());
            });
    }

    public void SetImage(Sprite sprite) => open.image.SetImage(sprite);
    public void SetData(GachaItemData itemData) => data = itemData;
} 