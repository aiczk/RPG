
public interface Indexer<out TValue, in TKey>
{
    TValue this[TKey index] { get;}
}