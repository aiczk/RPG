using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DescriptionData : SingletonMonoBehaviour<DescriptionData>
{
    [SerializeField]
    private TextMeshProUGUI descText = default;

    [SerializeField]
    private Image descImage = default;
    
    public void SetData(string text, Sprite image)
    {
        descText.SetText(text);
        descImage.SetImage(image);
    }
}