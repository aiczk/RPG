﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : MonoBehaviour
{
    [SerializeField] private Vector3Int storyID = default;
    
    public Vector3Int GetStoryID() => storyID;
}
