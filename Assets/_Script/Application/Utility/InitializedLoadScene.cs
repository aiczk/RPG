﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InitializedLoadScene : MonoBehaviour
{
    [SerializeField] private SceneObject firstUI = default,secondUI = default;

    private void Awake()
    {
        SceneManager.LoadSceneAsync(firstUI, LoadSceneMode.Additive);
        SceneManager.LoadSceneAsync(secondUI, LoadSceneMode.Additive);
    }
}
