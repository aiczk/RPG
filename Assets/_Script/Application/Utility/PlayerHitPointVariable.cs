﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using Random = UnityEngine.Random;

public interface IHitPoint
{
    void ApplyDamage();
    int GetDamage();
}

public class PlayerHitPointVariable : MonoBehaviour,IHitPoint
{
    [SerializeField,Range(0,20)] private int BaseDamage = default;
    private int endurance = 100;
    private int damageTemp;

    private void Start()
    {
        this.ObserveEveryValueChanged(x => endurance)
            //.SkipWhile(x => x >= -1)
            .Subscribe(x =>
            {
                //TODO 親を殺す通知をする
                Debug.Log($"HP :{x}");
            });
    }

    void IHitPoint.ApplyDamage()
    {
        damageTemp = BaseDamage + Random.Range(0,5);
        
        endurance -= damageTemp;
    }
    
    int IHitPoint.GetDamage() => damageTemp;
}
