﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SingletonUtility
{
    public static GameObject GetPlayer() => SingletonUtilityHelper.Instance.GetPlayer();
    public static Transform GetPlayerTransform() => SingletonUtilityHelper.Instance.GetPlayer().transform;

    public static GameObject GetTouchManager() => SingletonUtilityHelper.Instance.GetManager();
    public static Camera GetMainCamera() => SingletonUtilityHelper.Instance.GetMainCamera();
    public static Camera GetSubCamera() => SingletonUtilityHelper.Instance.GetUICamera();
    public static GameObject GetDescriptionObject() => SingletonUtilityHelper.Instance.GetDescription();
}
