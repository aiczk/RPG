﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GUIUtility
{
    public static void GUISetActive(IEnumerable<GameObject> enumerator, bool value)
    {
        foreach (var go in enumerator)
        {
            go.SetActive(value);
        }
    }
    
    public static void GUISetActive(GameObject parent, bool value)
    {
        parent.SetActive(value);
    }
}
