﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using App.Utility;
using TMPro;
using UniRx;
using UniRx.Async;
using UniRx.Toolkit;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public static class Utility
{    
    public static float Distance3D(Vector3 a, Vector3 b) => (a - b).sqrMagnitude;
    public static float Distance2D(Vector2 a, Vector2 b) => (a - b).sqrMagnitude;
    
    public static Vector3 WorldToViewPort(this Vector3 target)
    {
        return SingletonUtility.GetMainCamera()
            .ViewportToWorldPoint(SingletonUtility.GetMainCamera().WorldToViewportPoint(target));
    }

    public static Image SetImage(this Image image, Sprite sprite)
    {
        if (!image.sprite.Equals(sprite))
            image.sprite = sprite;
        
        return image;
    }

    public static Image SetPosition(this Image image, Vector3 pos)
    {
        if (!(Distance2D(image.rectTransform.localPosition,pos) < 0.001f))
            image.rectTransform.localPosition = pos;

        return image;
    }
    
    public static bool IsArrayWithInRange<T>(this IEnumerable<T> self, int index ) => index < self.Count();
    public static int Repeat(this int self, int value, int max) => (self + value + max) % max;
    public static void SetActive(this TextMeshProUGUI text,bool active) => text.color = active ? Color.white : Color.clear;

    public static GameObject ClosestObject(Transform player,GameObject[] targetList)
    {
        var targetDist = float.MaxValue;
        GameObject target = default;

        foreach (var go in targetList)
        {
            if (go == null || !go.activeSelf)
                continue;
            
            var dist = Distance3D(player.position, go.transform.position);
            if (!(targetDist > dist))
                continue;
                
            target = go;
            targetDist = dist;
        }
        
        return target;
    }

    public static GameObject ClosestObject(Transform player,Transform parent)
    {
        var targetDist = float.MaxValue;
        GameObject target = default;

        var targetList = parent.GetComponentsInChildren<GameObject>();

        foreach (var go in targetList)
        {
            if (go == null || !go.activeSelf)
                continue;
            
            var dist = Distance3D(player.position, go.transform.position);
            if (!(targetDist > dist))
                continue;
                
            target = go;
            targetDist = dist;
        }
        
        return target;
    }
    
    public static string Rename(this GameObject go, string name) => go.name = name;
    
    public static T LoopElement<T>(this IList<T> list, int index)
    {
        if (list.Count == 0) throw new ArgumentException("要素数が0のためアクセスできません");

        index %= list.Count;

        if (index < 0)
        {
            index += list.Count;
        }

        var target = list[index];

        return target;
    }

    public static void Destroy(this GameObject obj) => Object.Destroy(obj);

    public static IObservable<T> SkipLatestValue<T>(this IReadOnlyReactiveProperty<T> source) => source.HasValue ? source.Skip(1) : source;
    
    //TODO: Get Progress
    public static IDisposable OnLoadScene(this IObservable<SceneObject> scene)
    {
        return
            scene
                .Subscribe(async x =>
                {
                    var currentSceneName = SceneManager.GetActiveScene().name;
                        
                    var load = await SceneManager
                        .LoadSceneAsync(x,LoadSceneMode.Additive)
                        .AsObservable()
                        .TakeWhile(y => y.progress >= 1);
                                            
                    await SceneManager.UnloadSceneAsync(currentSceneName);
                });
    }

    /// <summary>
    /// 直行座標・球体座標変換公式
    /// </summary>
    /// <param name="angle1">x</param>
    /// <param name="angle2">y</param>
    /// <param name="rad">半径</param>
    private static Vector3 GetPositionOnSphere(float angle1, float angle2, float rad)
    {
        Vector3 sphere = default;
        sphere.x = rad * Mathf.Sin(angle1) * Mathf.Cos(angle2);
        sphere.y = rad * Mathf.Cos(angle1);
        sphere.z = rad * Mathf.Sin(angle1) * Mathf.Sin(angle2);
        return sphere;
    }
}

public static class CastTo<T>
{
    private static class Cache<S>
    {
        static Cache()
        {
            var p = Expression.Parameter(typeof(S));
            var c = Expression.ConvertChecked(p, typeof(T));
            Caster = Expression.Lambda<Func<S,T>>(c, p).Compile();
        }
        internal static readonly Func<S,T> Caster;
    }

    public static T From<S>(S source)
    {
        return Cache<S>.Caster(source);
    }
}
