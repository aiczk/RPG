﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using Sirenix.Utilities;
using UniRx;
using UniRx.Triggers;

public class GeometryCameraController : MonoBehaviour 
{
    private bool enableInputCapture = true;
    private bool holdRightMouseCapture = false;
    private bool inputCaptured;
    private float vert,hori;
    private Vector3 def;
    
    private Transform player,mainCamTransform;

    private void Start()
    {                
        Cursor.visible = false;
        enabled = enableInputCapture;
        mainCamTransform = Camera.main.transform;
        player = SingletonUtility.GetPlayerTransform();
        
        var fixedUpdate = this.UpdateAsObservable().Share();
            
            fixedUpdate
            .Subscribe(x =>
            {
                if (!inputCaptured)
                {
                    if (!holdRightMouseCapture && Input.GetMouseButtonDown(0))
                        CaptureInput();
                    else if (holdRightMouseCapture && Input.GetMouseButtonDown(1))
                        CaptureInput();
                }

                if (!inputCaptured)
                    return;

                if (inputCaptured)
                {
                    if (!holdRightMouseCapture && Input.GetKeyDown(KeyCode.Escape))
                        ReleaseInput();
                    else if (holdRightMouseCapture && Input.GetMouseButtonUp(1))
                        ReleaseInput();
                }
            })
            .AddTo(this);
            
            fixedUpdate
                .Subscribe(x =>
                {
                    //Frustum.Instance.CalculateFrustumPlanes()
                    
                    if (Input.GetKey(KeyCode.LeftArrow))
                        hori -= 1;
                    if (Input.GetKey(KeyCode.RightArrow))
                        hori += 1;
                    if (Input.GetKey(KeyCode.UpArrow))
                        vert += 1;
                    if (Input.GetKey(KeyCode.DownArrow))
                        vert -= 1;
                    
                    mainCamTransform.LookAt(player);
                    mainCamTransform.localPosition = Vector3.Slerp(transform.localPosition,
                        MathfUtility.GetPositionOnSphere(vert / 30, hori / 30, 300) +
                        player.position, Time.deltaTime);
                });
    }

    private void OnValidate() 
    {
        if(Application.isPlaying)
            enabled = enableInputCapture;
    }
    
    private void OnApplicationFocus(bool focus) 
    {
        if(inputCaptured && !focus)
            ReleaseInput();
    }

    private void CaptureInput() 
    {
        Cursor.lockState = CursorLockMode.Locked;

        Cursor.visible = false;
        inputCaptured = true;

        //yaw = transform.eulerAngles.y;
        //pitch = transform.eulerAngles.x;
    }

    private void ReleaseInput() 
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        inputCaptured = false;
    }
}