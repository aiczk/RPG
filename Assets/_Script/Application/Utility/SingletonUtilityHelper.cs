﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public class SingletonUtilityHelper : SingletonMonoBehaviour<SingletonUtilityHelper>
{
    [SerializeField] private GameObject Player = default;
    [SerializeField] private GameObject ApplyManager = default;
    [SerializeField] private Camera sub = default;
    private Camera main = default;
    [SerializeField] private GameObject descriptionObj = default;

    private void Awake()
    {
        main = Camera.main;
    }
    
    public GameObject GetPlayer() => Player;
    public GameObject GetManager() => ApplyManager;
    public Camera GetMainCamera() => main;
    public Camera GetUICamera() => sub;
    public GameObject GetDescription() => descriptionObj;
}