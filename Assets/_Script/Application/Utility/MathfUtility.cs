﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MathfUtility 
{
    
    public static bool InsideCircle(Vector2 self,Vector2 target, float radius)
    {
        var diff = target - self;
            
        //x^2 + y^2 = r^2 
        //円の位置^2が半径^2以下か
        return Mathf.Pow(diff.x, 2) + Mathf.Pow(diff.y, 2) <= Mathf.Pow(radius, 2);
    }

    public static float Angle(Vector3 self,Vector3 target,bool positive = true)
    {
        var dif = target - self;

        var rad = Mathf.Atan2(dif.x, dif.z);
        var deg = rad * Mathf.Rad2Deg;

        if (!positive) 
            return deg;
        
        if (deg < 0)
            deg += 360;

        return deg;
    }
        
    public static Vector3 GetPositionOnSphere(float angle1, float angle2, float radius)
    {
        var rad = radius * Mathf.Deg2Rad;
            
        Vector3 sphere = default;
        sphere.x = rad * Mathf.Sin(angle1) * Mathf.Cos(angle2);
        sphere.y = rad * Mathf.Cos(angle1);
        sphere.z = rad * Mathf.Sin(angle1) * Mathf.Sin(angle2);

        return sphere;
    }
    
}
