﻿using System;
using System.Collections.Generic;
using Domain.UseCase;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public interface IIndicatorData
{
    void Initialize();
    void SetDrawingRangeInExists(bool value);
    bool GetDrawingRangeInExists();
    void SetImagePosition(Camera main, Camera Sub);
    void CheckRelativeDistance(Vector3 distance, float distanceLength = 20);
    bool GetRelativeDistanceValue();
    void SetRelativeDistanceValue(bool value);
    void SetGameObject(GameObject obj);
    TextMeshProUGUI GetCaption();
    Bounds GetTargetBound();
    void ImageEnable(bool active);
}

[Serializable]
public struct IndicatorData : IIndicatorData
{
#pragma warning disable 0649
    private bool IsDisplay,isDistance;
    private GameObject target;
    [SerializeField] private Image image;
    [SerializeField] private TextMeshProUGUI caption;

    void IIndicatorData.Initialize()
    {
        image.enabled = false;
        caption.color = Color.clear;
    }

    void IIndicatorData.SetDrawingRangeInExists(bool value) => IsDisplay = value;
    bool IIndicatorData.GetDrawingRangeInExists() => IsDisplay;

    void IIndicatorData.SetImagePosition(Camera main, Camera sub)
    {
        image.transform.position = Vector3.Lerp(
            image.rectTransform.position,
            sub.ViewportToWorldPoint(main.WorldToViewportPoint(target.transform.position)),
            1);
    }

    void IIndicatorData.CheckRelativeDistance(Vector3 player, float distanceLength) => 
        isDistance = Utility.Distance3D(player, target.transform.position) <= distanceLength;
    
    bool IIndicatorData.GetRelativeDistanceValue() => isDistance;
    void IIndicatorData.SetRelativeDistanceValue(bool value) => isDistance = value;

    void IIndicatorData.SetGameObject(GameObject obj) => target = obj;
    TextMeshProUGUI IIndicatorData.GetCaption() => caption;

    Bounds IIndicatorData.GetTargetBound() => target.GetComponent<Collider>().bounds;

    void IIndicatorData.ImageEnable(bool active)
    {
        if (active == image.enabled)
            return;
        
        image.enabled = active;
        caption.color = active ? Color.white : Color.clear;
    }
}