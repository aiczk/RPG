using UniRx.Toolkit;
using UnityEngine;

public class ItemPool : ObjectPool<Item>
{
    private Item prefab;
    private Transform parent;

    public ItemPool(Item prefab,Transform parent)
    {
        this.prefab = prefab;
        this.parent = parent;
    }

    protected override Item CreateInstance()
    {
        var e = Object.Instantiate(prefab);
        e.transform.SetParent(parent);
        return e;
    }
}