﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace Manager.Update
{
    public interface IUpdatable
    {
        void Update();
    }

    public interface IFixedUpdatable
    {
        void FixedUpdate();
    }

    public class UpdateManager : SingletonMonoBehaviour<UpdateManager>
    {
        private List<IUpdatable> updatables = new List<IUpdatable>();
        private List<IFixedUpdatable> fixedUpdatables = new List<IFixedUpdatable>();
        private IObservable<Unit> updateObservable,fixedObservable;
    
        private void Awake()
        {
            updateObservable = this.UpdateAsObservable();
            updateObservable
                .Subscribe(x =>
                {
                    for (var i = 0; i < updatables.Count; i++)
                    {
                        var update = updatables[i];
                        update.Update();
                    }
                });
            
            fixedObservable = this.FixedUpdateAsObservable();
            fixedObservable
                .Subscribe(x =>
                {
                    for (var i = 0; i < fixedUpdatables.Count; i++)
                    {
                        var fixedUpdate = fixedUpdatables[i];
                        fixedUpdate.FixedUpdate();
                    }
                });
        }

        public void AddUpdateList(IUpdatable updatable) => updatables.Add(updatable);
        public void AddFixedUpdateList(IFixedUpdatable fixedUpdatable) => fixedUpdatables.Add(fixedUpdatable);
        public IObservable<Unit> Update() => updateObservable.Share();
        public IObservable<Unit> FixedUpdate() => fixedObservable.Share();
    }
}
