using UnityEngine;

public struct Spherical 
{
    private float radius;
    private float phi;
    private float theta;
    
    public Spherical(float radius = 1f, float phi = 0f, float theta = 0f)
    {
        this.radius = radius;
        this.phi = phi;
        this.theta = theta;
    }

    public Spherical SetFromVector3(Vector3 vector) 
    {
        radius = vector.magnitude;

        if (radius == 0f) 
        {
            theta = 0f;
            phi = 0f;
        } 
        else
        {
            theta = Mathf.Atan2(vector.x, vector.z);
            phi = Mathf.Acos(Mathf.Clamp(vector.y / radius, -1, 1));
        }

        return this;
    }

    public Vector3 SetVector3( ref Vector3 target )
    {
        var sinPhiRadius = Mathf.Sin(phi) * radius;

        target.x = sinPhiRadius * Mathf.Sin(theta);
        target.y = Mathf.Cos(phi) * radius;
        target.z = sinPhiRadius * Mathf.Cos(theta);

        return target;
    }

    public static Spherical FromVector3(Vector3 vector) => new Spherical().SetFromVector3(vector);
}