using System;
using System.Reflection;
using UniRx;
using UnityEngine;

public class Frustum : SingletonMonoBehaviour<Frustum>
{
    [SerializeField] private Camera cma = default;
    public delegate void FrustmCallback(Plane[] planes, Matrix4x4 worldToProjectMatrix);
    public FrustmCallback CalculateFrustumPlanes = null;
    private Plane[] planes = new Plane[6];
    private Camera main;
    
    private void Awake()
    {
        var info = typeof(GeometryUtility).GetMethod("Internal_ExtractPlanes",
            BindingFlags.Static | BindingFlags.NonPublic,
            null,
            new[]
            {
                typeof(Plane[]),
                typeof(Matrix4x4)
            }
            , null);
        
        CalculateFrustumPlanes = Delegate.CreateDelegate(typeof(FrustmCallback), info ?? throw new Exception("NULL")) as FrustmCallback;
        
        main = Camera.main;
    }

    private void Start()
    {
        main.ObserveEveryValueChanged(x => Vector3Int.CeilToInt(x.transform.position))
            .Subscribe(x =>
            {
                CalculateFrustumPlanes(planes, cma.projectionMatrix * cma.worldToCameraMatrix);
                var bound = new Bounds(main.transform.position, Vector3.one);

                if (!GeometryUtility.TestPlanesAABB(planes, bound))
                    Debug.Log("Out of Camera");
            });
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere (transform.position, 5f);
    }
}