﻿using System.Collections;
using System.Collections.Generic;
using UniRx.Toolkit;
using UnityEngine;
using UnityEngine.UI;

public class ImagePool : ObjectPool<Image>
{
    private Image prefab;
    private Transform parent;

    public ImagePool(Image prefab, Transform parent)
    {
        this.prefab = prefab;
        this.parent = parent;
    }
    
    protected override Image CreateInstance()
    {
        var e = Object.Instantiate(prefab);

        e.transform.SetParent(parent);

        return e;
    }
}
