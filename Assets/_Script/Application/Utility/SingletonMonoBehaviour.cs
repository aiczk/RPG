﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

public class SingletonMonoBehaviour<T> : MonoBehaviour where T : MonoBehaviour 
{
    private static volatile T instance;
    private static bool applicationIsQuitting;   
    private static object syncObj = new object (); 

    public static T Instance 
    {
        get 
        {
            if(applicationIsQuitting)
                return null;

            if (instance != null) 
                return instance;
            
            instance = FindObjectOfType<T>();

            if ( FindObjectsOfType<T>().Length > 1)
                return instance;

            if (instance != null) 
                return instance;
            
            lock (syncObj)
            {
                var singleton = new GameObject
                {
                    name = $"{typeof(T)} (singleton)"
                };
                
                instance = singleton.AddComponent<T>();
                DontDestroyOnLoad(singleton);
            }
            return instance;
        }
        
        private set => instance = value;
    }

    private void OnApplicationQuit() => applicationIsQuitting = true;
    private void OnDestroy () => Instance = null;

    protected SingletonMonoBehaviour () {}
}

public abstract class SingletonSerializedScriptableObject<T> : SerializedScriptableObject where T : ScriptableObject 
{
    static T _instance = null;
    public static T Instance
    {
        get
        {
            if (!_instance)
                _instance = Resources.FindObjectsOfTypeAll<T>().FirstOrDefault();
            return _instance;
        }
    }
}

public abstract class SingletonScriptableObject<T> : ScriptableObject where T : ScriptableObject 
{
    static T _instance = null;
    public static T Instance
    {
        get
        {
            if (!_instance)
                _instance = Resources.FindObjectsOfTypeAll<T>().FirstOrDefault();
            return _instance;
        }
    }
}