﻿using System;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

public class Bridge : SingletonMonoBehaviour<Bridge>
{
    private Transform player = default;

    private void Awake()
    {
        player = SingletonUtility.GetPlayerTransform();
    }
    
    public IObservable<Collider> OnTriggerStay() => player.OnTriggerStayAsObservable();
    public IObservable<Collider> OnTriggerExit() => player.OnTriggerExitAsObservable();
    public IObservable<Collider> OnTriggerEnter() => player.OnTriggerEnterAsObservable();
}
