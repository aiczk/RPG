﻿using System;
using System.Collections;
using System.Collections.Generic;
using Data.Repository;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace Data.DataStore
{
    //DataStore
    public class TemporaryCollider : MonoBehaviour,IDamagePositionDataStore
    {        
        private void Start()
        {
            this.OnCollisionEnterAsObservable()
                .Where(x => x.gameObject.GetComponent<IHitPoint>() != null)
                .Subscribe(x =>
                {
                    var hitPoint = x.gameObject.GetComponent<IHitPoint>();
                    
                    hitPoint.ApplyDamage();
                    this.GetRepository().SetValue(x.GetContact(0).point, hitPoint.GetDamage());
                });
        }
    }
}