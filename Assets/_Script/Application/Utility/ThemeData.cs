﻿using System.Collections;
using System.Collections.Generic;
using UniRx.Toolkit;
using UnityEngine;
using UnityEngine.UI;

public interface IThemeData
{
    void SetData(MenuTheme data);
    void SetImage(Sprite bg, Sprite btn);
}

public class ThemeData : MonoBehaviour,IThemeData
{
    [SerializeField] private Image backGround = default, button = default;
    
    void IThemeData.SetData(MenuTheme data)
    {
        backGround.SetImage(data.GetBackGroundTheme());
        button.SetImage(data.GetButtonTheme());
    }

    void IThemeData.SetImage(Sprite bg, Sprite btn)
    {
        backGround.SetImage(bg);
        button.SetImage(btn);
    }
}