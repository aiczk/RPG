using System.Collections.Generic;
using App.Utility;
using Sirenix.OdinInspector;
using UnityEngine;

public interface IOffensivePattern
{
    IPatternComposition GetOffensiveVariation(DistanceType distance);
}

[CreateAssetMenu( menuName = "Util/Create OffencePattern", fileName = "Pattern" )]
public class OffensivePattern : SerializedScriptableObject,IOffensivePattern
{
    [SerializeField] private IReadOnlyDictionary<DistanceType,PatternComposition> patterns = new Dictionary<DistanceType, PatternComposition>();

    IPatternComposition IOffensivePattern.GetOffensiveVariation(DistanceType distance) => patterns[distance];
}