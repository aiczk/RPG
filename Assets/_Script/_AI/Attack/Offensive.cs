using System;
using System.Collections.Generic;
using App.Utility;
using Presentation.Presenter;
using Presentation.View;
using UniRx;
using UniRx.Diagnostics;
using UniRx.Triggers;
using UnityEngine;
using Random = UnityEngine.Random;

public interface IIndividual
{
    IObservable<int> OnHitStopAsObservable();
    IObservable<Offensive> OnDestroyOffensiveAsObservable();
    IObservable<DistanceType> OnEffectAsObservable();
    int GetIndex();
}

public class Offensive : MonoBehaviour,IIndividual,IObsEffectUtility
{
    private Subject<int> attack = new Subject<int>();
    private Subject<DistanceType> type = new Subject<DistanceType>();
    private IHealth health;
    private IOffensivePattern offensive;
    //private IEffect effect;
    private int index,wait;
    private Transform self,player;

    private void Start()
    {
        player = SingletonUtility.GetPlayerTransform();
        
        self = transform;
        wait = Random.Range(500, 2500);
        
        Observable
            .Interval(TimeSpan.FromSeconds(3))
            .SelectMany(x => Observable.Return(wait).Delay(TimeSpan.FromMilliseconds(wait)))
            .Subscribe(x =>
            {
                var types = ChangeDistance(Utility.Distance3D(self.position,player.position));
                wait = offensive.GetOffensiveVariation(types).GetRandomTechnique().GetHitStopTime();
                
                type.OnNext(types);
                //effect.require.OnNext(types);
                attack.OnNext(wait);
            })
            .AddTo(this);
        
//        this.GetPresenter()
//            .OnEffectRentAsObservable()
//            .Select(x => x.transform)
//            .Subscribe(x =>
//            {
//                x.rotation = self.rotation;
//                x.position = self.position;
//            })
//            .AddTo(this);
    }

    private static DistanceType ChangeDistance(float value)
    {
        if (value > 0 || value < 100)
            return DistanceType.Short;
        if (value > 100 || value < 200)
            return DistanceType.Medium;
        if (value < 300)
            return DistanceType.Long;
                
        throw new Exception("Out Of Range");
    }

    public void SetPattern(IOffensivePattern pattern) => offensive = pattern;
    public void SetHealth(IHealth hlh) => health = hlh;
    //public void SetEffect(IEffect eft) => effect = eft;
    public void SetIndex(int i) => index = i;
    
    IObservable<int> IIndividual.OnHitStopAsObservable() => attack.AsObservable();
    IObservable<Offensive> IIndividual.OnDestroyOffensiveAsObservable() => 
        health.OnDestroyOffensiveAsObservable().Select(x => this);

    IObservable<DistanceType> IIndividual.OnEffectAsObservable() => type.AsObservable();
    int IIndividual.GetIndex() => index;
}