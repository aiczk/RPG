using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

public interface IPatternComposition
{
    IOffensiveVariation GetRandomTechnique();
    IOffensiveVariation GetIndexTechnique(int index);
}

[Serializable,InlineProperty]
public class PatternComposition : IPatternComposition
{
    [SerializeField] private List<OffensiveVariation> technique = new List<OffensiveVariation>();

    IOffensiveVariation IPatternComposition.GetRandomTechnique() => technique[Random.Range(0, technique.Count)];
    IOffensiveVariation IPatternComposition.GetIndexTechnique(int index) => technique[index];
}