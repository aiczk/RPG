﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Playables;

public interface IOffensiveVariation
{    
    int GetDamage();
    int GetHitStopTime();
    AnimationClip GetAnimation();
}

[CreateAssetMenu( menuName = "Util/Create OffensiveVariation", fileName = "Variation" )]
public class OffensiveVariation : ScriptableObject,IOffensiveVariation
{
    //エフェクトファイルなども入れる。
    [SerializeField, Range(1, 25)] private int damage = 1;
    //Animationの長さ
    [SerializeField, Range(0, 30)] private int hitStopTime = 0;
    [SerializeField] private AnimationClip motion = default;

    int IOffensiveVariation.GetDamage() => damage;
    int IOffensiveVariation.GetHitStopTime() => hitStopTime * 100;

    AnimationClip IOffensiveVariation.GetAnimation() => motion;
}
