﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public interface IHostileIdentifier
{
    ID GetID();
    Offensive GetOffensivePrefab();
}

public interface IHostileUtility
{
    EnemyType GetEnemyType();
}

[CreateAssetMenu( menuName = "Util/Create Identifier", fileName = "Identifier" )]
public class HostileIdentifier : ScriptableObject,IHostileIdentifier,IHostileUtility
{
    [SerializeField, EnumToggleButtons] private EnemyType type = EnemyType.AdvanceGuard;
    [SerializeField] private Offensive prefab = default;
    [SerializeField] private ID ID = default;

    ID IHostileIdentifier.GetID() => ID;
    Offensive IHostileIdentifier.GetOffensivePrefab() => prefab;
    EnemyType IHostileUtility.GetEnemyType() => type;
}

public enum EnemyType
{
    AdvanceGuard,
    RearGuard,
}