﻿using System.Collections;
using System.Collections.Generic;
using Domain.UseCase;
using UnityEngine;

namespace Data.Repository
{
    public interface IOffensiveDataStore
    {
        IOffensivePattern GetOffensivePattern(ID id);
    }
    
    public class OffensiveRepository : MonoBehaviour,IOffensiveRepository
    {
        private IOffensiveDataStore dataStore;

        private void Awake()
        {
            dataStore = GetComponent<IOffensiveDataStore>();
        }

        IOffensivePattern IOffensiveRepository.GetOffensivePattern(ID id) => dataStore.GetOffensivePattern(id);
    }
}
