using System;
using UnityEngine;

namespace Domain.UseCase
{
    [Serializable]
    public struct HostileData
    {
        [SerializeField] private HostileIdentifier identifier;
        [SerializeField] private Transform parent;
        [SerializeField,Range(0,10),Tooltip("Amount of Enemy")] private int quantity;

        public HostileData(HostileIdentifier identifier, int quantity,Transform parent)
        {
            this.identifier = identifier;
            this.quantity = quantity;
            this.parent = parent;
        }

        public IHostileIdentifier GetIdentifier() => identifier;
        public IHostileUtility GetIdentifierUtility() => identifier;
        public int GetQuantity() => quantity;
        public Transform GetParent() => parent;
    }
}