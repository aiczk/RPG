using System;
using Presentation.View;
using UniRx.Toolkit;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public class OffensivePool : ObjectPool<Offensive>
{
    private ISoldierSettings setting;
    private readonly Offensive prefab;
    private readonly IOffensivePattern pattern;
    private readonly EnemyType type;
    private readonly int index;
    private readonly Transform parent;
    private static readonly int rearHealth = 150, advanceHealth = 250;

    public OffensivePool(Offensive prefab, Transform parent,IOffensivePattern pattern,EnemyType type,int index)
    {
        this.prefab = prefab;
        this.pattern = pattern;
        this.type = type;
        this.index = index;
        this.parent = parent ?? new GameObject("TEMP PARENT").transform;
    }
    
    protected override Offensive CreateInstance()
    {
        var obj = Object.Instantiate(prefab);
        var transform = obj.transform;
        var pos = obj.transform.position;
        var gameObject = obj.gameObject;
        
        transform.SetParent(parent);
        transform.position = new Vector3(pos.x + Random.Range(-10,10), pos.y + Random.Range(0,10), pos.z+ Random.Range(-10,10));
                
        var rig = gameObject.GetComponent<Rigidbody>() ?? gameObject.AddComponent<Rigidbody>();
        
        rig.angularDrag = 10;
        rig.drag = 2;
        rig.useGravity = false;

        var health = gameObject.AddComponent<Health>();
        //var effect = gameObject.AddComponent<Effect>();
        
        switch (type)
        {
            case EnemyType.RearGuard:
                setting = gameObject.AddComponent<Soldier_Rear>();
                health.SetHitPoint(rearHealth);
                break;
            
            case EnemyType.AdvanceGuard:
                setting = gameObject.AddComponent<Soldier_Advance>();
                health.SetHitPoint(advanceHealth);
                break;
        }
        
        setting.SetOffensive(obj);
                                
        obj.SetPattern(pattern);
        obj.SetIndex(index);
        obj.SetHealth(health);
        //obj.SetEffect(effect);
                
        return obj;
    }
}