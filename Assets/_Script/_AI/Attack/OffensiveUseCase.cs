﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UniRx;
using UnityEngine;

namespace Domain.UseCase
{
    public interface IOffensiveUseCase
    {
        IObservable<IEnumerable<Offensive>> OnOffensivesChangedAsObservable();
    }

    public interface IOffensivePresenter
    {
        IObservable<Offensive> OnDiedAsObservable();
    }

    public interface IOffensiveRepository
    {
        IOffensivePattern GetOffensivePattern(ID id);
    }
    
    internal class OffensiveUseCase : SerializedMonoBehaviour,IOffensiveUseCase
    {
        [ListDrawerSettings(DraggableItems = false)]
        [SerializeField] private IReadOnlyList<HostileData> identifiers = new List<HostileData>();
        
        private IOffensiveRepository repository;
        private IOffensivePresenter presenter;
        private OffensivePool[] pools;
        private List<Offensive> offensives = new List<Offensive>();
        private Subject<IEnumerable<Offensive>> offensiveChanged = new Subject<IEnumerable<Offensive>>();
        
        private void Start()
        {
            repository = GetComponent<IOffensiveRepository>();
            presenter = GetComponent<IOffensivePresenter>();
            
            pools = new OffensivePool[identifiers.Count];

            for (var i = 0; i < identifiers.Count; i++)
            {
                var data = identifiers[i];
                if (data.GetQuantity() < 0)
                    continue;
                
                var id = data.GetIdentifier().GetID();
                var prefab = data.GetIdentifier().GetOffensivePrefab();
                var parent = data.GetParent();
                var type = data.GetIdentifierUtility().GetEnemyType();
                var loadCounts = data.GetQuantity();
                var pattern = repository.GetOffensivePattern(id);

                pools[i] = new OffensivePool(prefab,parent,pattern,type,i);
                                
                for (var v = 0; v < loadCounts; v++)
                    offensives.Add(pools[i].Rent());
            }
            
            presenter
                .OnDiedAsObservable()
                .Subscribe(x =>
                {
                    pools[((IIndividual) x).GetIndex()].Return(x);
                    offensives.Remove(x);
                });

            offensiveChanged.OnNext(offensives);
        }

        IObservable<IEnumerable<Offensive>> IOffensiveUseCase.OnOffensivesChangedAsObservable() =>
            offensiveChanged.AsObservable().Share();
    }
}