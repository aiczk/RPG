﻿using System;
using System.Collections;
using System.Collections.Generic;
using Data.Repository;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Data.DataStore
{
    internal class OffensiveDataStore : SerializedMonoBehaviour,IOffensiveDataStore
    {
        [SerializeField] private IReadOnlyDictionary<ID,OffensivePattern> patterns = new Dictionary<ID, OffensivePattern>();
        IOffensivePattern IOffensiveDataStore.GetOffensivePattern(ID id) => patterns[id];
    }

    [Serializable]
    public struct OffensiveData
    {
        [SerializeField] private OffensivePattern pattern;
        [SerializeField] private EffectsTable table;

        public OffensiveData(OffensivePattern pattern, EffectsTable table)
        {
            this.pattern = pattern;
            this.table = table;
        }

        public OffensivePattern GetPattern() => pattern;
        public EffectsTable GetTable() => table;
    }
}