﻿using System;
using System.Collections;
using System.Collections.Generic;
using Domain.UseCase;
using UnityEngine;

namespace Presentation.Presenter
{
    public interface IOffensiveView
    {
        IObservable<Offensive> OnDiedAsObservable();
    }
    
    public class OffensivePresenter : SingletonMonoBehaviour<OffensivePresenter>,IOffensivePresenter
    {
        private IOffensiveUseCase useCase;
        private IOffensiveView view;

        private void Awake()
        {
            useCase = GetComponent<IOffensiveUseCase>();
            view = GetComponent<IOffensiveView>();
        }

        public IObservable<IEnumerable<Offensive>> OnOffensivesChangedAsObservable() =>
            useCase.OnOffensivesChangedAsObservable();

        IObservable<Offensive> IOffensivePresenter.OnDiedAsObservable() => view.OnDiedAsObservable();
    }

    public static class OP
    {
        public static OffensivePresenter GetPresenter(this IOffensiveView view) => OffensivePresenter.Instance;
    }
}
