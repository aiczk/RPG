﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Presentation.Presenter;
using UniRx;
using UnityEngine;

namespace Presentation.View
{
    public class OffensiveView : MonoBehaviour,IOffensiveView
    {
        private Subject<Offensive> offensiveDestroy = new Subject<Offensive>();

        private void Awake()
        {
            this.GetPresenter()
                .OnOffensivesChangedAsObservable()
                .Select(x => x.Select(y => (IIndividual)y).ToList())
                .Subscribe(offensiveList =>
                {
                    foreach (var individualData in offensiveList)
                    {
                        individualData
                            .OnDestroyOffensiveAsObservable()
                            .Subscribe(x => offensiveDestroy.OnNext(x))
                            .AddTo(this);
                    }
                });
        }

        IObservable<Offensive> IOffensiveView.OnDiedAsObservable() => offensiveDestroy.AsObservable();

    }
}