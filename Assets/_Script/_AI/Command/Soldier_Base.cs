﻿using System;
using System.Collections;
using System.Collections.Generic;
using Presentation.Presenter;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Presentation.View
{
    //前衛 後衛 を作る。
    //前衛 -> プレイヤーについていく&プレイヤー/自身の攻撃範囲内で攻撃する。
    //後衛 -> プレイヤーにはついていかないが、プレイヤーが自身の攻撃範囲外に出ると範囲内に入るように動く。&プレイヤーの攻撃範囲外から攻撃する。自身はプレイヤーの攻撃範囲内に入ったら逃げる。

    public interface ISoldierSettings
    {
        void SetOffensive(IIndividual individual);
    }
        
    public class Soldier_Base : MonoBehaviour,ICommandView
    {
        private Rigidbody rig;
        private Vector3 torque;
        private protected static readonly int minRadius = 3;
        private protected static readonly int maxRadius = 25;
                
        private static readonly float ratio = 100;

        private protected IObservable<Vector3> chaseObservable;
        
        private void Main()
        {
            rig = GetComponent<Rigidbody>();
            
            chaseObservable =
                this.GetPresenter()
                    .ChaseObservable()
                    .WithLatestFrom(this.GetPresenter().OnSendPlayerPosition(), (unit, pos) => pos)
                    .Share();
            
            chaseObservable
                .Subscribe(x =>
                {
                    var diff = x - transform.position;
                    var target_rot = Quaternion.LookRotation(diff);
                    var q = target_rot * Quaternion.Inverse(transform.rotation);
                    
                    torque = Quaternion2Vec3(q);
                    
                    var pow = torque * ratio;
                    rig.AddTorque(pow);
                })
                .AddTo(this);
            
        }

        private Vector3 Quaternion2Vec3(Quaternion q)
        {
            Vector3 temp = default;
            
            temp.x = q.x;
            temp.y = q.y;
            temp.z = q.z;

            return temp;
        }

        private protected static bool InsideCircle(Vector2 self, Vector2 target, float radius) =>
            MathfUtility.InsideCircle(self, target, radius);

        private protected static Vector3 Randomize(int rad) => Random.insideUnitSphere * rad;

        //selfから逆方向に三角形を展開させ、その範囲内
    }

}