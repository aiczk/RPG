﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Presentation.Presenter;
using UniRx;
using UniRx.Triggers;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Presentation.View
{    
    internal class Soldier_Advance : Soldier_Base,ISoldierSettings
    {
        private bool attack;
        private IIndividual individual;
        private Transform self;

        private void Start()
        {
            self = transform;
            
            var attacked = individual
                .OnHitStopAsObservable()
                .Share();
            
            attacked
                .Subscribe(x => attack = true);

            attacked
                .SelectMany(x => Observable.Return(x).Delay(TimeSpan.FromMilliseconds(x)))
                .Subscribe(x => attack = false);

            
            chaseObservable
                .Where(x => !attack)
                .Where(x => !InsideCircle(self.position, x, minRadius))
                .Subscribe(x => self.position = Vector3.Slerp(self.position,x, Time.deltaTime))
                .AddTo(gameObject);
        }

        void ISoldierSettings.SetOffensive(IIndividual individual) => this.individual = individual;
    }
    
}
