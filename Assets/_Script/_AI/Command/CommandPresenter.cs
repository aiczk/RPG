﻿using System;
using System.Collections;
using System.Collections.Generic;
using Domain.UseCase;
using UniRx;
using UnityEngine;

namespace Presentation.Presenter
{
    public interface ICommandView
    {
        
    }
    
    public class CommandPresenter : SingletonMonoBehaviour<CommandPresenter>,ICommandPresenter
    {
        private ICommandUseCase useCase;

        private void Awake()
        {
            useCase = GetComponent<ICommandUseCase>();
        }

        public IObservable<Vector3> OnSendPlayerPosition() => useCase.OnSendPlayerPosition();

        public IObservable<IEnumerable<Transform>> OnAlliesTransformsAsObservable() =>
            useCase.OnAlliesTransformsAsObservable();

        public Vector3 GetPlayerCurrentPosition() => useCase.GetPlayerCurrentPosition();
        public IObservable<Unit> ChaseObservable() => useCase.ChaseObservable();
    }

    public static class CP
    {
        public static CommandPresenter GetPresenter(this ICommandView view) => CommandPresenter.Instance;
    }
}
