﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace Domain.UseCase
{
    public interface ICommandUseCase
    {
        IObservable<Vector3> OnSendPlayerPosition();
        IObservable<IEnumerable<Transform>> OnAlliesTransformsAsObservable();
        Vector3 GetPlayerCurrentPosition();
        IObservable<Unit> ChaseObservable();
    }

    public interface ICommandPresenter
    {
        
    }

    public interface ICommandRepository
    {
        
    }
    
    public class CommandUseCase : MonoBehaviour,ICommandUseCase
    {
        [SerializeField,Range(200,350)] private int enqueueInterval = default,dequeueInterval = default;
        [SerializeField,Range(100,150)] private int moveInterval = default;
        [SerializeField] private Transform player = default;
        
        private Queue<Vector3> inputQueue = new Queue<Vector3>();
        private Vector3 currentPosition,tempPosition;
        private ReactiveProperty<Vector3> pos = new ReactiveProperty<Vector3>();
        private IOffensiveUseCase offensiveUseCase;
        private IObservable<Unit> chaseObservable;

        private void Awake()
        {
            offensiveUseCase = GameObject.FindWithTag("Offensive").GetComponent<IOffensiveUseCase>();
            
            player
                .ObserveEveryValueChanged(x => x.position)
                .ThrottleFirst(TimeSpan.FromMilliseconds(enqueueInterval))
                .Subscribe(x => inputQueue.Enqueue(x));

            Observable
                .Interval(TimeSpan.FromMilliseconds(moveInterval))
                .Select(x => player.position)
                .Where(x => !inputQueue.Count.Equals(0))
                .Subscribe(x =>
                {
                    currentPosition = x;

                    if (currentPosition.Equals(tempPosition))
                    {
                        inputQueue.Clear();
                        Debug.Log("Queue Clear");
                        return;
                    }

                    currentPosition = tempPosition = x;
                })
                .AddTo(this);

            Observable
                .Interval(TimeSpan.FromMilliseconds(dequeueInterval))
                .Where(x => inputQueue.Count > 0)
                .Subscribe(x => pos.Value = inputQueue.Dequeue())
                .AddTo(this);

            chaseObservable = this.FixedUpdateAsObservable().Share();
        }
        
        Vector3 ICommandUseCase.GetPlayerCurrentPosition() => player.position;
        IObservable<Unit> ICommandUseCase.ChaseObservable() => chaseObservable;
        IObservable<Vector3> ICommandUseCase.OnSendPlayerPosition() => pos.AsObservable();
        IObservable<IEnumerable<Transform>> ICommandUseCase.OnAlliesTransformsAsObservable() =>
            offensiveUseCase
                .OnOffensivesChangedAsObservable()
                .Select(x => x.Select(y => y.transform));
    }
}
