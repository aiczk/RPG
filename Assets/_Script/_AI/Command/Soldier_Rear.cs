﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using Presentation.Presenter;
using Sirenix.Utilities;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace Presentation.View
{
    internal class Soldier_Rear : Soldier_Base,ISoldierSettings
    {
        private static readonly int radius = 250;
        private bool attack;
        private IIndividual individual;
        private Transform self;
        
        private void Start()
        {
            self = transform;
            
            var attacked = individual
                .OnHitStopAsObservable()
                .Share();

            attacked
                .Subscribe(x => attack = true);

            attacked
                .SelectMany(x => Observable.Return(x).Delay(TimeSpan.FromMilliseconds(x)))
                .Subscribe(x => attack = false);
            
            var hitStopObservable = chaseObservable
                .Where(x => !attack)
                .Share();

            hitStopObservable
                .Where(x => InsideCircle(self.position, x, minRadius))
                .Subscribe(x =>
                {
                    var randomPos = Randomize(radius).Clamp(self.position, Vector3.positiveInfinity) + x;
                    self.position = Vector3.Slerp(self.position, randomPos, Time.deltaTime);
                })
                .AddTo(gameObject);
            
            hitStopObservable
                .Where(x => InsideCircle(self.position,x,maxRadius))
                .Subscribe(x =>
                {
                    //Debug.Log("射程圏内");
                    //Debug.Log(Angle(self.position,x).ToString());
                })
                .AddTo(gameObject);
            
            hitStopObservable
                .Where(x => !InsideCircle(self.position, x, maxRadius))
                .Subscribe(x =>
                {
                    self.position = Vector3.Slerp(self.position, x, Time.deltaTime);
                    //Debug.Log("攻撃範囲外");
                })
                .AddTo(gameObject);
        }
        
        void ISoldierSettings.SetOffensive(IIndividual individual) => this.individual = individual;
    }
}