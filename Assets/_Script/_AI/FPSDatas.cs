﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class FPSDatas : MonoBehaviour
{
    [SerializeField] private Text text = default;
    [SerializeField, Range(24, 120)] private int fps = default;
    
    void Start()
    {
        Application.targetFrameRate = fps;
        
        FPSCounter
            .Current
            .ThrottleFirst(TimeSpan.FromMilliseconds(100))
            .Subscribe(x => text.text = x.ToString());
    }
}

public static class FPSCounter
{
    const int BufferSize = 5; 
    public static IReadOnlyReactiveProperty<float> Current { get; }

    static FPSCounter()
    {
        Current = Observable.EveryUpdate()
            .Select(_ => Time.deltaTime)
            .Buffer(BufferSize, 1)
            .Select(x => 1.0f / x.Average())
            .ToReadOnlyReactiveProperty();
    }
}