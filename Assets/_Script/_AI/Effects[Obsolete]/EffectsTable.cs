﻿using System;
using System.Collections;
using System.Collections.Generic;
using App.Utility;
using Sirenix.OdinInspector;
using UnityEngine;

public interface IEffectsTable
{
    IReadOnlyList<EffectData> GetList(DistanceType distanceType);
}

[CreateAssetMenu( menuName = "Util/Create EffectsTable", fileName = "Table" )]
public class EffectsTable : SerializedScriptableObject,IEffectsTable
{
    [SerializeField]
    private IReadOnlyDictionary<DistanceType, List<EffectData>> effects = new Dictionary<DistanceType, List<EffectData>>();

    IReadOnlyList<EffectData> IEffectsTable.GetList(DistanceType distanceType) => effects[distanceType];
}

[Serializable]
public struct EffectData
{
    [SerializeField,Range(1,7)] private int loadSize;
    [SerializeField,Range(1,10)] private int shrinkRatio;
    [SerializeField] private RFX4_EffectSettings prefab;

    public EffectData(int loadSize, RFX4_EffectSettings prefab,int shrinkRatio)
    {
        this.loadSize = loadSize;
        this.prefab = prefab;
        this.shrinkRatio = shrinkRatio;
    }

    public int GetLoadSize() => loadSize;
    public RFX4_EffectSettings GetPrefab() => prefab;
    public float GetShrinkRatio() => shrinkRatio / 10;
}