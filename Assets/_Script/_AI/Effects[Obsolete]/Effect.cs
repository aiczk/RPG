using System;
using System.Collections.Generic;
using App.Utility;
using Presentation.Presenter;
using UniRx;
using UnityEngine;

public interface IObsEffect
{
    IObservable<RFX4_EffectSettings> OnEffectRentAsObservable();
}

public class ObsEffect : MonoBehaviour,IObsEffect,IObsEffectUtility
{
    //private IObservable<RFX4_EffectSettings> rentObservable;
    
    //仕方ない
    IObservable<RFX4_EffectSettings> IObsEffect.OnEffectRentAsObservable() =>
        this.GetPresenter().OnEffectRentAsObservable();

//    IObservable<RFX4_EffectSettings> IEffect.OnEffectRentAsObservable() => rentObservable;
//    
//    public void SetRentObservable(IObservable<RFX4_EffectSettings> observable) => rentObservable = observable;
}