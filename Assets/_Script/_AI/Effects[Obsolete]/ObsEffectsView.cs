﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using App.Utility;
using Presentation.Presenter;
using UniRx;
using UnityEngine;

namespace Presentation.View
{
    public class ObsEffectsView : MonoBehaviour,IObsEffectsView
    {
        [SerializeField,Range(1,5)] private int waitTime = 0;
        private Subject<RFX4_EffectSettings> destroyed = new Subject<RFX4_EffectSettings>();
        private Subject<DistanceType> rent4eft = new Subject<DistanceType>();
        
        private void Start()
        {            
            this.GetPresenter()
                .OnEffectRentAsObservable()
                .SelectMany(x => Observable.Timer(TimeSpan.FromSeconds(waitTime)),(rfx,time) => rfx)
                .Subscribe(x => destroyed.OnNext(x));

            this.GetPresenter()
                .OnEffectsAsObservable()
                .Select(x => x.ToList())
                .Subscribe(individuals =>
                {
                    foreach (var data in individuals)
                    {
                        //((Effect)effect).SetRentObservable(effectRent);
                        data
                            .OnEffectAsObservable()
                            .Subscribe(x => rent4eft.OnNext(x));
                    }
                });
        }

        IObservable<RFX4_EffectSettings> IObsEffectsView.OnEffectDestroyedAsObservable() => destroyed.AsObservable();
        IObservable<DistanceType> IObsEffectsView.Rent4EffectAsObservable() => rent4eft.AsObservable();
    }
}