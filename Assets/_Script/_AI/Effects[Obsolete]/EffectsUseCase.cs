﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using App.Utility;
using UniRx;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Domain.UseCase
{
    public interface IEffectsUseCase
    {
        IObservable<RFX4_EffectSettings> OnEffectRentAsObservable();
        IObservable<IEnumerable<IIndividual>> OnEffectsAsObservable();
    }

    public interface IEffectsPresenter
    {
        IObservable<RFX4_EffectSettings> OnEffectDestroyedAsObservable();
        IObservable<DistanceType> Rent4EffectAsObservable();
    }
    
    public class EffectsUseCase : MonoBehaviour,IEffectsUseCase
    {
        [SerializeField] private EffectsTable effectsData = default;

        private Subject<RFX4_EffectSettings> eftSub = new Subject<RFX4_EffectSettings>();
        private IEffectsPresenter presenter;
        private IEffectsTable table;
        private IOffensiveUseCase offensiveUseCase;
        private EffectsPool[] effectsPool;
        private Transform parent;
        private GameObject[] parents = new GameObject[3];

        private void Start()
        {
            parents = new []        
            {
                new GameObject("Effect Short"),
                new GameObject("Effect Mid"),
                new GameObject("Effect Long"), 
            };
            
            offensiveUseCase = GameObject.FindWithTag("Offensive").GetComponent<IOffensiveUseCase>();
            presenter = GetComponent<IEffectsPresenter>();
            table = effectsData;
            
            for (var types = 0; types < 3; types++)
            {
                var distanceType = CastTo<DistanceType>.From(types);
                var list = table.GetList(distanceType);

                parent = parents[types].transform;
                
                effectsPool = new EffectsPool[list.Count];
                
                for (var i = 0; i < list.Count; i++)
                {
                    var effect = list[i];
                    var prefab = effect.GetPrefab();
                    var load = effect.GetLoadSize();
                    var shrinkRatio = effect.GetShrinkRatio();

                    effectsPool[i] = new EffectsPool(prefab,parent,i);
                    effectsPool[i].PreloadAsync(load, load).Subscribe();
                    effectsPool[i].StartShrinkTimer(TimeSpan.FromMilliseconds(500), shrinkRatio, load);
                }
            }

            presenter
                .OnEffectDestroyedAsObservable()
                .Subscribe(x => effectsPool[x.GetIndex()].Return(x));

            presenter
                .Rent4EffectAsObservable()
                .Select(x => (int)x)
                .Subscribe(x => eftSub.OnNext(effectsPool[x].Rent()));
        }


        IObservable<RFX4_EffectSettings> IEffectsUseCase.OnEffectRentAsObservable() => eftSub.AsObservable().Share();

        IObservable<IEnumerable<IIndividual>> IEffectsUseCase.OnEffectsAsObservable() => 
            offensiveUseCase.OnOffensivesChangedAsObservable();
    }
}