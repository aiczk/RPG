﻿using System;
using System.Collections;
using System.Collections.Generic;
using App.Utility;
using Domain.UseCase;
using UniRx;
using UnityEngine;

namespace Presentation.Presenter
{
    public interface IObsEffectsView
    {
        IObservable<RFX4_EffectSettings> OnEffectDestroyedAsObservable();
        IObservable<DistanceType> Rent4EffectAsObservable();
    }

    public interface IObsEffectUtility
    {
        
    }

    public class EffectsPresenter : SingletonMonoBehaviour<EffectsPresenter>,IEffectsPresenter
    {
        private IEffectsUseCase useCase;
        private IObsEffectsView view;
        
        private void Awake()
        {
            useCase = GetComponent<IEffectsUseCase>();
            view = GetComponent<IObsEffectsView>();
        }

        IObservable<RFX4_EffectSettings> IEffectsPresenter.OnEffectDestroyedAsObservable() =>
            view.OnEffectDestroyedAsObservable();

        IObservable<DistanceType> IEffectsPresenter.Rent4EffectAsObservable() => view.Rent4EffectAsObservable();
        public IObservable<IEnumerable<IIndividual>> OnEffectsAsObservable() => useCase.OnEffectsAsObservable();
        public IObservable<RFX4_EffectSettings> OnEffectRentAsObservable() => useCase.OnEffectRentAsObservable().Share();
    }

    public static class EP
    {
        public static EffectsPresenter GetPresenter(this IObsEffectsView view) => EffectsPresenter.Instance;
        public static EffectsPresenter GetPresenter(this IObsEffectUtility util) => EffectsPresenter.Instance;
    }
}
