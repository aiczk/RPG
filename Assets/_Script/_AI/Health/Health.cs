﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UniRx.Diagnostics;
using UniRx.Triggers;
using UnityEngine;

public interface IHealth
{
    IObservable<Unit> OnDestroyOffensiveAsObservable();
}

public class Health : MonoBehaviour,IHealth
{
    private Subject<Unit> death = new Subject<Unit>();
    private readonly string tagName = "Bullet";
    private int health;

    private void Start()
    {        
        this.OnCollisionEnterAsObservable()
            .Where(x => x.gameObject.CompareTag(tagName))
            .Select(x => x.gameObject)
            .Subscribe(x =>
            {
                //スクリプト.InterfaceMethod.GetDamage
            });
    }

    public void SetHitPoint(int value) => health = value;
    IObservable<Unit> IHealth.OnDestroyOffensiveAsObservable() => death.AsObservable();
}
