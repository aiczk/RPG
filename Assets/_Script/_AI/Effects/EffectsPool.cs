using UniRx.Toolkit;
using UnityEngine;

public class EffectsPool : ObjectPool<RFX4_EffectSettings>
{
    private readonly RFX4_EffectSettings prefab;
    private readonly Transform parent;
    private readonly int index;

    public EffectsPool(RFX4_EffectSettings prefab, Transform parent,int index)
    {
        this.prefab = prefab;
        this.parent = parent;
        this.index = index;
    }
    
    protected override RFX4_EffectSettings CreateInstance()
    {
        var obj = Object.Instantiate(prefab);
        var transform = obj.transform;
        
        transform.SetParent(parent);
        
        obj.SetIndex(index);
        
        return obj;
    }
}